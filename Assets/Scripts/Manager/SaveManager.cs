﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Config;
using UI;
using UnityEngine.UI;
/// <summary>
/// 存档管理器
/// </summary>
public class SaveManager : Common.MonoSingleton<SaveManager>
{
    private string sceneName = "level";
    /// <summary>
    /// 场景名称
    /// </summary>
    public string SceneName { get => PlayerPrefs.GetString(sceneName); }
    public override void Init()
    {
        base.Init();
        ////读取值
        //Load("Set", Set.Instance);
        //float arg0 = Set.Instance.soundValue;
        ////设置音量
        //SoundManager.Instance.masterMixer.SetFloat(Const.Sound.MixerVolume, arg0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            SavePlayerData();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadPlayerData();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneController.Instance.transitionToMain();
        }
    }
    /// <summary>
    /// 保存玩家数据
    /// </summary>
    public void SavePlayerData()
    {
        if (GameController.Instance.player)
        {
            Save(GameController.Instance.player.stateData.name, GameController.Instance.player.stateData);
        }
    }
    /// <summary>
    /// 读取玩家数据
    /// </summary>
    public void LoadPlayerData()
    {
        if (GameController.Instance.player)
            Load(GameController.Instance.player.stateData.name, GameController.Instance.player.stateData);
    }
    /// <summary>
    /// 保存数据
    /// </summary>
    /// <param name="key">数据名称</param>
    /// <param name="data">数据内容</param>
    public void Save(string key, Object data)
    {
        string jsonData = JsonUtility.ToJson(data);
        //玩家信息
        PlayerPrefs.SetString(key, jsonData);
        //场景信息
        PlayerPrefs.SetString(sceneName, SceneManager.GetActiveScene().name);
        PlayerPrefs.Save();
    }
    /// <summary>
    /// 读取数据
    /// </summary>
    /// <param name="key">数据名称</param>
    /// <param name="data">数据内容</param>
    public void Load(string key, Object data)
    {
        if (PlayerPrefs.HasKey(key))
        {
            //获取处理的数据覆盖到原数据
            JsonUtility.FromJsonOverwrite(PlayerPrefs.GetString(key), data);
        }
    }
}
