using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
/// <summary>
/// 声音管理器
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class SoundManager : Common.MonoSingleton<SoundManager>
{
    /// <summary>
    /// 固定声源
    /// </summary>
    public AudioSource audioSource;
    public AudioSource bgmSource;
    public AudioMixer masterMixer;
    [SerializeField]
    private AudioClip jumpAudio, hurtAudio, moveAudio, bgmAudio;

    private void Start()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        audioSource = audioSources[0];
        bgmSource = audioSources[1];
        bgmSource.clip = bgmAudio;
        bgmSource.Play();
    }

    public void JumpAudio()
    {
        audioSource.clip = jumpAudio;
        audioSource.Play();
    }
    public void HurtAudio()
    {
        audioSource.clip = hurtAudio;
        audioSource.Play();
    }
    public void MoveAudio()
    {
        //audioSource.clip = moveAudio;
        //audioSource.Play();
    }
    /// <summary>
    /// 播放空间声源
    /// </summary>
    /// <param name="audioSource">播放的声源位置</param>
    /// <param name="audioClip">声源文件</param>
    public void PlaySpaceAudio(AudioSource audioSource, AudioClip audioClip)
    {
        audioSource.clip = audioClip;
        audioSource.Play();
    }

    /// <summary>
    /// 播放空间声源
    /// </summary>
    /// <param name="audioSource">播放的声源位置</param>
    /// <param name="audioClip">声源文件</param>
    public void PlaySpaceAudio(AudioSource audioSource, string audioClipName)
    {
        audioSource.clip = Resources.Load<AudioClip>(audioClipName); ;
        audioSource.Play();
    }
}
