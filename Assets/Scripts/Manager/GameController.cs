﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FF;

public class GameController : Common.MonoSingleton<GameController>
{
    private void Start() {
        
    }
    /// <summary>
    /// 玩家
    /// </summary>
    [HideInInspector]
    public PlayerState player;
    /// <summary>
    /// 玩家预制件
    /// </summary>
    public GameObject playerPrefab;
    /// <summary>
    /// 游戏结束观察者们
    /// </summary>
    /// <typeparam name="IEndGameObserver"></typeparam>
    /// <returns></returns>
    private List<IEndGameObserver> endGameObservers = new List<IEndGameObserver>();
    /// <summary>
    /// 注册玩家
    /// </summary>
    /// <param name="playerState">玩家状态</param>
    public void RigisterPlayer(PlayerState playerState)
    {
        player = playerState;
    }
    /// <summary>
    /// 添加观察者
    /// </summary>
    /// <param name="observer"></param>
    public void AddObserver(IEndGameObserver observer)
    {
        endGameObservers.Add(observer);
    }
    /// <summary>
    /// 移除观察者
    /// </summary>
    /// <param name="observer"></param>
    public void RemoveObserver(IEndGameObserver observer)
    {
        endGameObservers.Remove(observer);
    }
    /// <summary>
    /// 通知所有观察者
    /// </summary>
    public void NotifyObservers()
    {
        foreach (IEndGameObserver observer in endGameObservers)
        {
            observer.EndGameNotify();
        }
    }

}
