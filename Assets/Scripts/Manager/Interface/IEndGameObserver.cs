﻿/// <summary>
/// 游戏结束观察者
/// </summary>
public interface IEndGameObserver
{
    /// <summary>
    /// 游戏结束通知
    /// </summary>
    public void EndGameNotify();
}
