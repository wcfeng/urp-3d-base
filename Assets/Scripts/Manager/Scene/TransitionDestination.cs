﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 传送的目的地
/// </summary>
public class TransitionDestination : MonoBehaviour
{
    /// <summary>
    /// 目的地标签
    /// </summary>
    public EM.DestinationTag destinationTag;
}
