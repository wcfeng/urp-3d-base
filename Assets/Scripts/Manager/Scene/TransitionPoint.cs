﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 场景转换点(传送起点)
/// </summary>
public class TransitionPoint : MonoBehaviour
{
    [Header("Transition Info")]
    /// <summary>
    /// 需要传送的场景名称
    /// </summary>
    public string sceneName;
    /// <summary>
    /// 传送类型
    /// </summary>
    public EM.TransitionType transitionType;
    /// <summary>
    /// 目的地标记
    /// </summary>
    public EM.DestinationTag destinationTag;
    /// <summary>
    /// 是否可以传送
    /// </summary>
    private bool canTrans;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && canTrans)
        {
            SceneController.Instance.TransitionToDestination(this);
        }
    }
    /// <summary>
    /// 玩家进入传送范围
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(EM.Tag.Player.ToString()))
        {
            canTrans = true;
        }
    }
    /// <summary>
    /// 玩家离开传送范围
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(EM.Tag.Player.ToString()))
        {
            canTrans = false;
        }
    }
}
