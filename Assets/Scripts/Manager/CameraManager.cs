using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using FF;
using Common;

public class CameraManager : MonoSingleton<CameraManager>
{
    /// <summary>
    /// 虚拟相机
    /// </summary>
    [HideInInspector]
    public CinemachineVirtualCamera followCamera;

    /// <summary>
    /// 相机关注玩家
    /// </summary>
    /// <param name="player"></param>
    public void RigisterPlayer(PlayerState player)
    {
        followCamera = FindObjectOfType<CinemachineVirtualCamera>();
        if (followCamera != null)
        {
            followCamera.Follow = player.transform.FindChildByName(Const.Player.CameraRoot);
            followCamera.LookAt = player.transform.FindChildByName(Const.Player.CameraRoot);
        }
    }
}
