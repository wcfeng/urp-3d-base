﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
/// <summary>
/// 场景管理
/// </summary>
public class SceneController : Common.MonoSingleton<SceneController>, IEndGameObserver
{
    /// <summary>
    /// 玩家物体
    /// </summary>
    private GameObject player;
    /// <summary>
    /// 玩家预制件
    /// </summary>
    private GameObject playerPrefab;
    /// <summary>
    /// 淡入淡出预制件
    /// </summary>
    public SceneFader sceneFaderPrefab;
    private void Start()
    {
        playerPrefab = GameController.Instance.playerPrefab;
        //注册观察者
        GameController.Instance.AddObserver(this);
    }
    /// <summary>
    /// 同场景传送
    /// </summary>
    /// <param name="transitionPoint">传送点</param>
    public void TransitionToDestination(TransitionPoint transitionPoint)
    {
        switch (transitionPoint.transitionType)
        {
            case EM.TransitionType.SameScene:
                //获取当前场景名称
                string activeScene = SceneManager.GetActiveScene().name;
                StartCoroutine(Transition(activeScene, transitionPoint.destinationTag));
                break;
            case EM.TransitionType.DifferentScene:
                StartCoroutine(Transition(transitionPoint.sceneName, transitionPoint.destinationTag));
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// 跨场景传送
    /// </summary>
    /// <param name="sceneName">场景名称</param>
    /// <param name="destinationTag">目的地标签</param>
    /// <returns></returns>
    IEnumerator Transition(string sceneName, EM.DestinationTag destinationTag)
    {
        //保存玩家数据
        SaveManager.Instance.SavePlayerData();

        if (SceneManager.GetActiveScene().name == sceneName)
        {
            player = GameController.Instance.player.gameObject;
            // NavMeshAgent playerAgent = player.GetComponent<NavMeshAgent>();
            // playerAgent.enabled = false;
            //获取目标位置
            TransitionDestination transPoint = getDestination(destinationTag);
            //设置玩家到目标位置
            player.transform.SetPositionAndRotation(transPoint.transform.position, transPoint.transform.rotation);
            // playerAgent.enabled = true;
            yield return null;
        }
        else
        {
            //异步读取场景
            yield return SceneManager.LoadSceneAsync(sceneName);
            TransitionDestination transPoint = getDestination(destinationTag);
            //生成玩家预制件
            yield return Instantiate(playerPrefab, transPoint.transform.position, transPoint.transform.rotation);
            //读取玩家数据
            SaveManager.Instance.LoadPlayerData();
            yield break;
        }

    }

    /// <summary>
    /// 根据目标标签 获取目标位置
    /// </summary>
    /// <param name="destinationTag">目标标签</param>
    /// <returns></returns>
    private TransitionDestination getDestination(EM.DestinationTag destinationTag)
    {
        TransitionDestination[] entrances = FindObjectsOfType<TransitionDestination>();
        for (int i = 0; i < entrances.Length; i++)
        {
            if (entrances[i].destinationTag == destinationTag)
            {
                return entrances[i];
            }
        }

        return null;
    }
    /// <summary>
    /// 加载主场景
    /// </summary>
    public void transitionToMain()
    {
        StartCoroutine(LoadMain());
    }
    /// <summary>
    /// 加载场景
    /// </summary>
    /// <param name="sceneName"></param>
    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadLevel(sceneName));
    }
    /// <summary>
    /// 加载保存的场景
    /// </summary>
    public void LoadToSaveScene()
    {
        StartCoroutine(LoadLevel(SaveManager.Instance.SceneName));
    }

    /// <summary>
    /// 根据名称加载场景
    /// </summary>
    /// <param name="scene">场景名称</param>
    /// <returns></returns>
    IEnumerator LoadLevel(string scene)
    {
        if (scene != "")
        {
            //淡出
            SceneFader fade = Instantiate(sceneFaderPrefab);
            yield return StartCoroutine(fade.FadeOut(Const.Common.FadeTime));
            //加载场景
            yield return SceneManager.LoadSceneAsync(scene);
            //获取起点位置
            Transform startPoint = GetEntrance();
            print(startPoint);
            //生成玩家
            yield return player = Instantiate(playerPrefab, startPoint.position, startPoint.rotation);
            //保存数据
            SaveManager.Instance.SavePlayerData();
            //淡入
            yield return StartCoroutine(fade.FadeIn(Const.Common.FadeTime));
            yield break;
        }
    }
    /// <summary>
    /// 加载到主菜单
    /// </summary>
    /// <returns></returns>
    IEnumerator LoadMain()
    {
        SceneFader fade = Instantiate(sceneFaderPrefab);
        yield return StartCoroutine(fade.FadeOut(Const.Common.FadeTime));
        yield return SceneManager.LoadSceneAsync(Const.Scene.Main);
        yield return StartCoroutine(fade.FadeIn(Const.Common.FadeTime));
        yield break;
    }
    /// <summary>
    /// 获取场景起点
    /// </summary>
    /// <returns>起点位置</returns>
    public Transform GetEntrance()
    {
        foreach (TransitionDestination item in FindObjectsOfType<TransitionDestination>())
        {
            if (item.destinationTag == EM.DestinationTag.Start)
                return item.transform;
        }
        return null;
    }

    public void EndGameNotify()
    {
        print("adsc");
        StartCoroutine(LoadMain());
    }
}
