﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Common;

public class MouseManager : Common.MonoSingleton<MouseManager>
{


    private RaycastHit hitInfo;
    public Texture2D point, doorway, attack, target, arrow;

    public event Action<Vector3> OnMouseClicked;
    public event Action<GameObject> OnEnemyClicked;

    // cinemachine
    /// <summary>
    /// 缩放速度
    /// </summary>
    [Tooltip("缩放速度")]
    public float zoomSpeed = 5f;

    [Tooltip("How far in degrees can you move the camera up")]
    public float TopClamp = 70.0f;

    [Tooltip("How far in degrees can you move the camera down")]
    public float BottomClamp = -30.0f;
    private float _rotateY;
    private float _rotateX;
    /// <summary>
    /// 是否初始化角色信息
    /// </summary>
    private bool isInitPlayer;
    /// <summary>
    /// 相机观察点
    /// </summary>
    private Transform playerCameraRoot;
    /// <summary>
    /// 原位置
    /// </summary>
    private Transform playerCameraRootTransformInit;
    /// <summary>
    /// 原缩放长度
    /// </summary>
    private float playerCameraRootFieldOfViewInit;

    private void Update()
    {
        // MouseControl();
        // SetCursorTexture();
    }

    private void LateUpdate()
    {
        if (GameController.Instance.player)
        {
            if (!isInitPlayer)
            {
                InitPlayer();
            }
            CameraRotation();
            CamaraZoom();
        }else
        {
            isInitPlayer = false;
        }
    }

    /// <summary>
    /// 初始化角色信息
    /// </summary>
    private void InitPlayer()
    {
        playerCameraRoot = GameController.Instance.player.playerCameraRoot;
        playerCameraRootTransformInit = playerCameraRoot;
        playerCameraRootFieldOfViewInit = CameraManager.Instance.followCamera.m_Lens.FieldOfView;
        isInitPlayer = true;
    }

    /// <summary>
    /// 鼠标旋转 控制摄像头旋转
    /// </summary>
    private void CameraRotation()
    {
        // Cinemachine will follow this target
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
            
        if (mouseX != 0 || mouseY != 0)
        {
            //y轴旋转
            _rotateY += mouseX;
            _rotateX += -mouseY;
        }
        _rotateX = ClampAngle(_rotateX, BottomClamp, TopClamp);
        playerCameraRoot.rotation = Quaternion.Euler(_rotateX, _rotateY, 0);
    }

    /// <summary>
    /// 鼠标缩放 控制焦距
    /// </summary>
    private void CamaraZoom()
    {
        float speed = Input.GetAxis("Mouse ScrollWheel");
        if (speed != 0)
        {
            CameraManager.Instance.followCamera.m_Lens.FieldOfView += speed * zoomSpeed;
        }
    }

    /// <summary>
    /// 鼠标点击层控制
    /// </summary>
    private void MouseControl()
    {
        if (Input.GetMouseButtonDown(0) && hitInfo.collider != null)
        {
            if (hitInfo.collider.gameObject.CompareTag("Ground"))
            {
                OnMouseClicked?.Invoke(hitInfo.point);
            }

            if (hitInfo.collider.gameObject.CompareTag("Enemy"))
            {
                OnEnemyClicked?.Invoke(hitInfo.collider.gameObject);
            }
            if (hitInfo.collider.gameObject.CompareTag("Attackable"))
            {
                OnEnemyClicked?.Invoke(hitInfo.collider.gameObject);
            }
            if (hitInfo.collider.gameObject.CompareTag(EM.Tag.Portal.ToString()))
            {
                OnMouseClicked?.Invoke(hitInfo.point);
            }
        }
    }

    /// <summary>
    /// 设置鼠标样式
    /// </summary>
    private void SetCursorTexture()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hitInfo))
        {
            switch (hitInfo.collider.gameObject.tag)
            {
                case "Ground":
                    Cursor.SetCursor(target, new Vector2(16, 16), CursorMode.Auto);
                    break;
                case "Enemy":
                    Cursor.SetCursor(attack, new Vector2(16, 16), CursorMode.Auto);
                    break;
                case "Portal":
                    Cursor.SetCursor(doorway, new Vector2(16, 16), CursorMode.Auto);
                    break;
                default:
                    Cursor.SetCursor(arrow, new Vector2(16, 16), CursorMode.Auto);
                    break;
            }
        }
    }

    /// <summary>
    /// 限制值在360之内
    /// </summary>
    /// <param name="lfAngle"></param>
    /// <param name="lfMin"></param>
    /// <param name="lfMax"></param>
    /// <returns></returns>
    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }

}
