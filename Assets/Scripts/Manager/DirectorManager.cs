using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using FF;
using Common;
using UnityEngine.Playables;
/// <summary>
/// timeLine
/// </summary>
public class DirectorManager : MonoSingleton<DirectorManager>
{
    private PlayableDirector[] directors;

    /// <summary>
    /// ����
    /// </summary>
    /// <param name="directorName">����</param>
    /// <returns></returns>
    public PlayableDirector Find(string directorName)
    {
        directors = FindObjectsOfType<PlayableDirector>();
        return directors.Find(d => d.name == directorName);
    }
}
