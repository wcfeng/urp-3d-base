﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FF.Skill;

[CreateAssetMenu(fileName = "New Data", menuName = "ScriptableObject/Skill/Skill Data")]
/// <summary>
/// 技能数据
/// </summary>
public class SkillData_SO : ScriptableObject
{
    [Header("技能基础信息")]
    /// <summary>
    /// 技能id
    /// </summary>
    [Tooltip("技能id")]
    public int id;
    /// <summary>
    /// 技能等级
    /// </summary>
    [Tooltip("技能等级")]
    public int level = 1;
    /// <summary>
    /// 技能名称
    /// </summary>
    [Tooltip("技能名称")]
    public string skillName = "BaseAttack";
    /// <summary>
    /// 技能描述
    /// </summary>
    [Tooltip("技能描述")]
    public string description;
    /// <summary>
    /// 冷却时间
    /// </summary>
    [Tooltip("冷却时间")]
    public float coolTime = 1;
    /// <summary>
    /// 冷却剩余
    /// </summary>
    [HideInInspector]
    public float coolRemain;
    /// <summary>
    /// 魔法消耗
    /// </summary>
    [Tooltip("魔法消耗")]
    public int costMP;
    /// <summary>
    /// 攻击距离
    /// </summary>
    [Tooltip("攻击距离")]
    public float attackDistance = 1;
    /// <summary>
    /// 攻击角度
    /// </summary>
    [Tooltip("攻击角度")]
    public float attackAngle = 90;
    /// <summary>
    /// 技能生成位置名称（不填默认当前人物原点）
    /// </summary>
    [Tooltip("技能生成位置名称,通常填写为模型的手部（不填默认当前人物原点）")]
    public string createPos;
    /// <summary>
    /// 是否使用对象池 可变的技能不可设置对象池 例如：仍出去的石头要保存在现场 并且可以变成其他攻击形态
    /// </summary>
    [Tooltip("是否使用对象池 可变的技能不可设置对象池 例如：仍出去的石头要保存在现场 并且可以变成其他攻击形态")]
    public bool useObjectPool = true;
    [Header("技能影响信息")]
    /// <summary>
    /// 攻击目标标签
    /// </summary>
    /// <value></value>
    [Tooltip("攻击目标标签")]
    public EM.Tag[] attackTargetTags = {
        EM.Tag.Enemy
    };
    /// <summary>
    /// 攻击目标
    /// </summary>
    [HideInInspector]
    public Transform[] attackTargets;
    /// <summary>
    /// 技能影响类型
    /// </summary>
    /// <value></value>
    [Tooltip("技能影响类型")]
    public ImpactType[] impactType = {
        ImpactType.CostMP,
        ImpactType.Damage,
    };
    // public string[] impactType = {"CostMP","Damage"};
    /// <summary>
    /// 连击id
    /// </summary>
    [Tooltip("下一个技能编号")]
    public int nextBatterId;
    /// <summary>
    /// 伤害比率
    /// </summary>
    [Tooltip("伤害比率")]
    public float atkRatio = 1;
    /// <summary>
    /// 持续时间
    /// </summary>
    [Tooltip("持续时间")]
    public float durationTime;
    /// <summary>
    /// 伤害间隔
    /// </summary>
    [Tooltip("伤害间隔")]
    public float atkInterval;
    /// <summary>
    /// 技能拥有者
    /// </summary>
    [HideInInspector]
    public GameObject owner;
    /// <summary>
    /// 技能预制件名称
    /// </summary>
    [Tooltip("技能预制件名称")]
    public string prefabName = "BaseMeleeAttackSkill";
    /// <summary>
    /// 技能预制件
    /// </summary>
    [HideInInspector]
    public GameObject skillPrefab;
    /// <summary>
    /// 动画名称
    /// </summary>
    [Tooltip("动画名称")]
    public string animationName;
    /// <summary>
    /// 受击特效名称
    /// </summary>
    [Tooltip("受击特效名称")]
    public string hitFxName;
    /// <summary>
    /// 受击特效预制件
    /// </summary>
    [Tooltip("受击特效预制件")]
    public GameObject hitFxPrefab;
    /// <summary>
    /// 攻击类型
    /// </summary>
    [Tooltip("攻击类型 单攻 群攻")]
    public SkillAttackType attackType;
    /// <summary>
    /// 选择类型
    /// </summary>
    [Tooltip("选择类型 扇形 圆形等")]
    public SelectorType selectorType;

    [Header("技能的力 对击退效果有效")]
    public float force;
}
