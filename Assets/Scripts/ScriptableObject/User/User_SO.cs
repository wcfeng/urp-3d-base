﻿using UnityEngine;
//首先定义一个类（序列化System.Serializable）
namespace Data
{
    [System.Serializable]
    public class User_SO : ScriptableObject
    {
        //下面都是需要json配置的数据(json文件里面的数据名要和类里面的名字相同，不然读取不到)
        /// <summary>
        /// 用户名
        /// </summary>
        public string username;
        /// <summary>
        /// 密码
        /// </summary>
        public string password;
        /// <summary>
        /// 密钥
        /// </summary>
        public string token;
        public int code;
        public string message;
        public string action;
    }
}
