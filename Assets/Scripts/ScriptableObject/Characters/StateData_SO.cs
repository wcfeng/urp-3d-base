﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Data", menuName = "ScriptableObject/Character/State Data")]
/// <summary>
/// 状态数据
/// </summary>
public class StateData_SO : ScriptableObject
{
    [Header("State Info")]
    /// <summary>
    /// 最大血量
    /// </summary>
    public int maxHP = 20;
    /// <summary>
    /// 当前血量
    /// </summary>
    public int HP = 20;
    /// <summary>
    /// 最大魔法
    /// </summary>
    public int maxMP = 20;
    /// <summary>
    /// 当前魔法
    /// </summary>
    public int MP = 20;
    /// <summary>
    /// 当前防御
    /// </summary>
    public int DEF = 3;
    /// <summary>
    /// 移动速度
    /// </summary>
    public int moveSpeed = 5;
    /// <summary>
    /// 质量 - 有击退效果的技能时 力量大于质量时,会被击退
    /// </summary>
    public float mass = 50;
    /// <summary>
    /// 模型半径 -大体型的物体需要设置才能被范围攻击判定到
    /// </summary>
    public float modelRadius = 0;
    [Header("Kill")]
    /// <summary>
    /// 提供经验
    /// </summary>
    [Tooltip("提供经验")]
    public int offerExp = 20;
    /// <summary>
    /// 死亡后延迟销毁时间
    /// </summary>
    public int destroyTime = 10;
    [Header("Level")]
    /// <summary>
    /// 当前等级
    /// </summary>
    public int currentLevel = 1;
    /// <summary>
    /// 最大等级
    /// </summary>
    public int maxLevel = 20;
    /// <summary>
    /// 升级所需经验值
    /// </summary>
    public int levelUpExp = 100;
    /// <summary>
    /// 当前经验
    /// </summary>
    public int currentExp;
    /// <summary>
    /// 等级倍增率（每增加1级，经验获取难度增加多少）
    /// 0-不增加 1-增加一倍
    /// </summary>
    public float levelBuff = 0.2f;
    /// <summary>
    /// 经验倍数
    /// </summary>
    /// <value></value>
    public float LevelMultiplier
    {
        get
        {
            return 1 + (currentLevel - 1) * levelBuff;
        }
    }
    [Header("Attack")]
    /// <summary>
    /// 基础攻击
    /// </summary>
    public int attack = 5;
    /// <summary>
    /// 攻击距离
    /// </summary>
    public float attackDistance = 1;
    /// <summary>
    /// 视野距离
    /// </summary>
    public float sightDistance = 10;
    /// <summary>
    /// 视野角度
    /// </summary>
    public float sightAngle = 90;
    /// <summary>
    /// 暴击伤害
    /// </summary>
    public float criticalMultiplier = 2;
    /// <summary>
    /// 暴击率
    /// </summary>
    public float criticalChance = 0.2f;

}
