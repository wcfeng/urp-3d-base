using System.IO; 
using System.Text; 
using UnityEditor; 
using UnityEditorInternal; 
using UnityEngine; 
/// <summary>
/// 枚举多选类型
/// </summary>
[CustomPropertyDrawer(typeof(EnumFlags))]
public class EnumFlagsAttributeDrawer : PropertyDrawer
{
 
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        property.intValue = EditorGUI.MaskField(position, label, property.intValue, property.enumNames);

        //Debug.Log("图层的值：" + property.intValue);
    }
}
