using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FF.Weapon
{
    /// <summary>
    /// 枚举多选 案例
    /// </summary>
    public abstract class EnumMultiSelectDemo : MonoBehaviour
    {
        /// <summary>
        /// 枪类型
        /// </summary>
        [EnumFlags]
        public EM.GunType gunType;

        private void Start() {
            StartCoroutine(RepeatDamage());
        }

        private IEnumerator RepeatDamage()
        {
            //总伤害时间
            while (true)
            {
                print(gunType);
                if ((EM.GunType.Pistol & gunType) == EM.GunType.Pistol)     //&判断type是否存在one这个枚举，与one做判断，
                {
                    Debug.Log("我进入Pistol环节");
                }
                yield return new WaitForSeconds(1);
            }
        }
    }
}
