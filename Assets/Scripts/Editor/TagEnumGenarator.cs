using System.IO; 
using System.Text; 
using UnityEditor; 
using UnityEditorInternal; 
using UnityEngine; 
public class TagEnumGenarator : MonoBehaviour
{
    /// <summary>
    /// 生成Tags 枚举字段
    /// </summary>
    [MenuItem("Tools/Tags/GenTagEnum")]
    public static void GenTagEnum()
    {
        var tags = InternalEditorUtility.tags; 
        var arg = ""; 
        foreach (var tag in tags)
        {
            arg += "\t\t" + tag + ",\n";
        }
        var res = "namespace EM\n{\n";
        res += "\tpublic enum Tag\n\t{\n" + arg + "}\n\t";
        res += "}\n";
        var path = Application.dataPath + "/Scripts/Enum/Tag.cs";

        File.WriteAllText(path, res, Encoding.UTF8);

        AssetDatabase.SaveAssets();

        AssetDatabase.Refresh();

    }

}
