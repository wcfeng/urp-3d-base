using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace FF
{
    /*
     * Application.persistentDataPath 读写目录
    */
    /// <summary>
    /// 读取 Assets/Resources/ 下的预制件资源
    /// 生成预制件资源配置文件 Assets/SteamingAssets/Config/PrefabMap.txt
    /// </summary>
    public class GenerateResConfig : Editor
    {
        [MenuItem("Tools/Resources/PrefabMap/Generate")]
        public static void Generate()
        {
            string resourcePath = "Assets/Resources";
            string streamingAssetsPath = "Assets/StreamingAssets";
            string prefabMapFilename = Const.Common.PrefabMapFile;
            //读取所有预制件信息
            string[] resFiles = AssetDatabase.FindAssets("t:prefab", new string[] { resourcePath });
            for (int i = 0; i < resFiles.Length; i++)
            {
                resFiles[i] = AssetDatabase.GUIDToAssetPath(resFiles[i]);
                string filename = Path.GetFileNameWithoutExtension(resFiles[i]);
                string filepath = resFiles[i].Replace(resourcePath+"/", string.Empty).Replace(".prefab",string.Empty);
                resFiles[i] = filename + "=" + filepath;
            }
            //如果文件不存在
            if (!Directory.Exists(streamingAssetsPath))
                //创建目录
                Directory.CreateDirectory(streamingAssetsPath);
            //写入文件
            File.WriteAllLines(streamingAssetsPath+"/"+prefabMapFilename, resFiles);
            //刷新资产
            AssetDatabase.Refresh();
        }
    }
}
