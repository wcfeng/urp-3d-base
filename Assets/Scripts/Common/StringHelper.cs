using System;
using System.Collections;
using System.Collections.Generic;

namespace Common
{
    /// <summary>
    /// 字符串助手
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// 字符串转枚举
        /// </summary>
        /// <param name="str">要转换的字符串</param>
        /// <typeparam name="T">转换的类型</typeparam>
        /// <returns>枚举值</returns>
        public static T ToEnum<T>(this String str)
        {
            return (T)Enum.Parse(typeof(T), str);
        }
    }
}
