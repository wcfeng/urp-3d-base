using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using FF;

namespace Common
{
    /// <summary>
    /// Socket
    /// </summary>
    public class TestNet2 : MonoSingleton<TestNet2>
    {
        //人物模型预设
        public GameObject humanPrefab;
        //人物列表
        public CharacterState myHuman;
        public StateData_SO syncStateData;
        public Dictionary<string, SyncState> otherHumans;
        public bool isStart = false;

        public override void Init()
        {
            if (!isStart) return;
            //网络模块
            NetManager.AddListener("Enter", OnEnter);
            NetManager.AddListener("List", OnList);
            NetManager.AddListener("Move", OnMove);
            NetManager.AddListener("Leave", OnLeave);
            NetManager.AddListener("Attack", OnAttack);
            NetManager.AddListener("Hit", OnHit);
            NetManager.AddListener("Die", OnDie);
            NetManager.Connect("127.0.0.1", 9501);

            otherHumans = new Dictionary<string, SyncState>();
        }
        private void Start()
        {
            if (!isStart) return;
            //设置角色
            PlayerState obj = GameController.Instance.player;
            float x = Random.Range(-5, 5)+48;
            float z = Random.Range(-5, 5)+5;
            obj.transform.position = new Vector3(x, 0, z);
            myHuman = obj.GetComponent<CharacterState>();
            string desc = NetManager.GetDesc();
            //发送协议 进入游戏
            Vector3 pos = myHuman.transform.position;
            Vector3 eul = myHuman.transform.eulerAngles;
            string sendStr = "Enter|";
            sendStr += NetManager.GetDesc() + ",";
            sendStr += pos.x + ",";
            sendStr += pos.y + ",";
            sendStr += pos.z + ",";
            sendStr += eul.y + ",";
            sendStr += "50" + ",";
            NetManager.Send(sendStr);
            //获取列表
            NetManager.Send("List|");

        }

        private void Update()
        {
            NetManager.Update();
        }

        public void SendMove(CharacterState characterState)
        {
            Vector3 pos = characterState.transform.position;
            Vector3 eul = characterState.transform.eulerAngles;
            float moveSpeed = characterState.GetComponentInChildren<Animator>().GetFloat(Const.Animation.Speed);
            string sendStr = "Move|";
            sendStr += NetManager.GetDesc() + ",";
            sendStr += pos.x + ",";
            sendStr += pos.y + ",";
            sendStr += pos.z + ",";
            sendStr += eul.y + ",";
            sendStr += moveSpeed + ",";
            NetManager.Send(sendStr);
        }

        public void SendAttack(CharacterState characterState)
        {
            Vector3 pos = characterState.transform.position;
            Vector3 eul = characterState.transform.eulerAngles;
            float moveSpeed = characterState.GetComponentInChildren<Animator>().GetFloat(Const.Animation.Speed);
            string sendStr = "Attack|";
            sendStr += NetManager.GetDesc() + ",";
            sendStr += eul.y + ",";
            NetManager.Send(sendStr);
        }

        public void SendHit(CharacterState characterState,float damage)
        {
            string sendStr = "Hit|";
            sendStr += characterState.desc + ",";
            sendStr += damage + ",";
            NetManager.Send(sendStr);
        }

        public void SendDie(CharacterState characterState)
        {
            string sendStr = "Die|";
            sendStr += characterState.desc + ",";
            NetManager.Send(sendStr);
        }

        void OnEnter(string msg)
        {
            Debug.Log("OnEnter" + msg);
            string[] split = msg.Split(',');
            string desc = split[0];
            float x = float.Parse(split[1]);
            float y = float.Parse(split[2]);
            float z = float.Parse(split[3]);
            float eulY = float.Parse(split[4]);
            //是自己
            if (desc == NetManager.GetDesc()) return;
            //已添加
            if (otherHumans.ContainsKey(desc)) return;
            //添加一个角色
            GameObject obj = (GameObject)Instantiate(humanPrefab);
            obj.transform.position = new Vector3(x, y, z);
            obj.transform.eulerAngles = new Vector3(0, eulY, 0);
            obj.SetActive(false);
            SyncState h = obj.AddComponent<SyncState>();
            h.stateDataTemplate = syncStateData;
            obj.SetActive(true);
            h.desc = desc;
            
            otherHumans.Add(desc, h);
        }

        void OnList(string msgArgs)
        {
            Debug.Log("OnList" + msgArgs); 
            //解析参数
            string[] split = msgArgs.Split(',');
            int count = (split.Length - 1) / 6;
            for (int i = 0; i < count; i++)
            {
                string desc = split[i * 6 + 0];
                float x = float.Parse(split[i * 6 + 1]);
                float y = float.Parse(split[i * 6 + 2]);
                float z = float.Parse(split[i * 6 + 3]);
                float eulY = float.Parse(split[i * 6 + 4]);
                int hp = int.Parse(split[i * 6 + 5]);
                //是自己
                if (desc == NetManager.GetDesc()) continue;
                //已添加
                if (otherHumans.ContainsKey(desc)) continue;
                //添加一个角色
                GameObject obj = (GameObject)Instantiate(humanPrefab);
                obj.transform.position = new Vector3(x, y, z);
                obj.transform.eulerAngles = new Vector3(0, eulY, 0);
                //先禁用 赋值后再启用 可后执行awake
                obj.SetActive(false);
                SyncState h = obj.AddComponent<SyncState>();
                h.stateDataTemplate = syncStateData;
                obj.SetActive(true);
                h.desc = desc;
                
                otherHumans.Add(desc, h);
            }
        }

        void OnMove(string msgArgs)
        {
            Debug.Log("OnMove" + msgArgs); 
            //解析参数
            string[] split = msgArgs.Split(',');
            string desc = split[0];
            float x = float.Parse(split[1]);
            float y = float.Parse(split[2]);
            float z = float.Parse(split[3]);
            float eulY = float.Parse(split[4]);
            float moveSpeed = float.Parse(split[5]);
            //移动
            if (!otherHumans.ContainsKey(desc)) return;
            SyncState h = otherHumans[desc];
            Vector3 targetPos = new Vector3(x, y, z);
            h.Move(targetPos, moveSpeed);
        }

        void OnLeave(string msgArgs)
        {
            Debug.Log("OnLeave" + msgArgs); 
            //解析参数
            string[] split = msgArgs.Split(',');
            string desc = split[0]; 
            //删除
            if (!otherHumans.ContainsKey(desc)) return;
            SyncState h = otherHumans[desc];
            Destroy(h.gameObject);
            otherHumans.Remove(desc);
        }

        void OnAttack(string msgArgs)
        {
            Debug.Log("OnAttack" + msgArgs); 
            //解析参数
            string[] split = msgArgs.Split(',');
            string desc = split[0];
            float eulY = float.Parse(split[1]);
            //攻击动作
            if (!otherHumans.ContainsKey(desc)) return;
            SyncState h = otherHumans[desc];
            h.Attack();
        }

        void OnHit(string msgArgs)
        {
            Debug.Log("OnHit" + msgArgs);
            //解析参数
            string[] split = msgArgs.Split(',');
            string desc = split[0];
            float damage = float.Parse(split[1]);
            Debug.Log(NetManager.GetDesc());
            Debug.Log(desc);
            //受伤
            //if (NetManager.GetDesc() == desc)
            //{
            //    myHuman.Damage(damage);
            //}
            
        }

        void OnDie(string msgArgs)
        {
            Debug.Log("OnHit" + msgArgs);
            //解析参数
            string[] split = msgArgs.Split(',');
            string desc = split[0];
            //死亡
            if (NetManager.GetDesc() == desc)
            {
                Debug.Log("死亡通知");
            }
        }
    }
}
