using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    /// <summary>
    /// 变换组件助手
    /// </summary>
    public static class TransformHelper
    {
        /// <summary>
        /// 获取子变换组件
        /// </summary>
        /// <param name="currentTF">当前变换组件</param>
        /// <param name="childName">子变换组件名称</param>
        /// <returns></returns>
        public static Transform FindChildByName(this Transform currentTF, string childName)
        {
            //获取当前子变换组件
            Transform childTF = currentTF.Find(childName);
            //如果找到 返回
            if (childTF != null) return childTF;
            //没找到 当前子组件
            for (int i = 0; i < currentTF.childCount; i++)
            {
                //子组件寻找下级组件
                childTF = FindChildByName(currentTF.GetChild(i), childName);
                if (childTF != null) return childTF;
            }
            return null;
        }
    }
}
