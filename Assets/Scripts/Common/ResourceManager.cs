using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace Common
{
    /// <summary>
    /// 资源管理
    /// </summary>
    public class ResourceManager
    {
        /// <summary>
        /// 预制件配置字典
        /// </summary>
        private static Dictionary<string, string> configMap;

        public object StringRador { get; private set; }
        /// <summary>
        /// 初始化
        /// </summary>
        static ResourceManager()
        {
            configMap = new Dictionary<string, string>();
            string fileContent = ConfigurationReader.GetConfigFile(Const.Common.PrefabMapFile);
            //读取配置文件
            ConfigurationReader.Reader(fileContent, BuildMap);
        }
        /// <summary>
        /// 读取配置到字典
        /// </summary>
        /// <param name="line"></param>
        private static void BuildMap(string line)
        {
            string[] keyValue = line.Split('=');
            configMap.Add(keyValue[0], keyValue[1]);
        }
        /// <summary>
        /// 根据预制件名称加载资源
        /// </summary>
        /// <param name="prefabName">预制件名称</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Load<T>(string prefabName) where T : Object
        {
            string prefabPath = configMap[prefabName];
            return Resources.Load<T>(prefabPath);
        }
    }
}
