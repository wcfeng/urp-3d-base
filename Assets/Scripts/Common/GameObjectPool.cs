using UnityEngine;
using System.Collections.Generic;
using System.Collections;
namespace Common
{
    /// <summary>
    /// 可重置
    /// </summary>
    public interface IResetable
    {
        void OnReset();
    }
    /// <summary>
    /// 游戏对象池：
    /// </summary>
    public class GameObjectPool : MonoSingleton<GameObjectPool>
    {
        /// <summary>
        /// 缓存字典
        /// </summary>
        private Dictionary<string, List<GameObject>> cache;
        public override void Init()
        {
            base.Init();
            cache = new Dictionary<string, List<GameObject>>();
        }
        //1 创建池：

        //2 创建使用池的元素【游戏对象】一个对象并使用对象:
        //池中有从池中返回;池中没有加载，放入池中再返回   
        /// <summary>
        /// 创建对象
        /// </summary>
        /// <param name="key">预制件名称</param>
        /// <param name="prefab">预制件</param>
        /// <param name="positon">位置</param>
        /// <param name="rotation">旋转</param>
        /// <returns></returns>
        public GameObject CreateObject(string key, GameObject prefab, Vector3 positon, Quaternion rotation)
        {
            //1)查找池中有无可用游戏对象
            GameObject temoGo = FindUsable(key);
            //2)池中有从池中返回;
            if (temoGo != null)
            {
                temoGo.transform.position = positon;
                temoGo.transform.rotation = rotation;
                temoGo.SetActive(true);//表示当前这个游戏对象在使用中
            }
            else//3)池中没有加载，放入池中再返回
            {
                temoGo = Instantiate(prefab, positon, rotation) as GameObject;
                //放入池中
                Add(key, temoGo);
            }
            //作为池物体的子物体
            temoGo.transform.parent = this.transform;
            //temoGo.GetComponent<IResetable>().OnReset();
            //遍历执行物体中所有需要重置的逻辑
            foreach (IResetable item in temoGo.GetComponents<IResetable>())
            {
                item.OnReset();
            }
            return temoGo;
        }
        /// <summary>
        /// 查找有无可用的游戏物体
        /// </summary>
        /// <param name="key">预制件名称</param>
        /// <returns></returns>
        private GameObject FindUsable(string key)
        {
            if (cache.ContainsKey(key))
            {
                //找列表 中找出 未激活【未活动】的游戏物体 
                //return cache[key].Find(g => !g.activeSelf);
                return cache[key].Find(g => !g.activeInHierarchy);
            }
            return null;
        }
        /// <summary>
        /// 添加游戏物体到对象池
        /// </summary>
        /// <param name="key">预制件名称</param>
        /// <param name="go">游戏对象</param>
        private void Add(string key, GameObject go)
        {
            //先检查池中是否 有需要的key，没有，需要向创建key和对应的列表。
            if (!cache.ContainsKey(key))
            {
                cache.Add(key, new List<GameObject>());
            }
            //把游戏对象添加到 池中
            cache[key].Add(go);
        }

        /// <summary>
        /// 释放资源：从池中删除对象
        /// </summary>
        /// <param name="key">预制件名称</param>
        public void Clear(string key)
        {
            if (cache.ContainsKey(key))
            {
                //释放场景中 的游戏物体
                for (int i = 0; i < cache[key].Count; i++)
                {
                    Destroy(cache[key][i]);
                }
                //移除了对象的地址
                cache.Remove(key);
            }
        }
        /// <summary>
        /// 释放全部资源
        /// </summary>
        public void ClearAll()
        {
            //string[] keys = cache.Keys;
            //List<string> keys = new List<string>(cache.Keys);
            //for (int i = 0; i < keys.Count; i++)
            //{
            //  Clear(keys[i]);
            //}
            foreach (var key in cache.Keys)
            {
                Clear(key);
            }
            //异常时执行下面 foreach不能删除自己
            //foreach (var key in new List<string>(cache.Keys))
            //{
            //    Clear(key);
            //}
        }
        /// <summary>
        /// 回收对象：使用完对象返回池中【从画面中消失】
        /// </summary>
        /// <param name="go">游戏对象</param>
        public void CollectObject(GameObject go)
        {
            go.SetActive(false);//本质：画面消失
        }
        /// <summary>
        /// 延时回收对象 等待一定的时间 协程
        /// </summary>
        /// <param name="go">游戏对象</param>
        /// <param name="delay">延迟时间</param>
        public void CollectObject(GameObject go, float delay = 0)
        {
            StartCoroutine(CollectDelay(go, delay));
        }
        private IEnumerator CollectDelay(GameObject go, float delay)
        {
            yield return new WaitForSeconds(delay);
            CollectObject(go);
        }
    }
}

