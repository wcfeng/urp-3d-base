using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Common
{
    /// <summary>
    /// Socket
    /// </summary>
    public class TestSocket : MonoSingleton<TestSocket> 
    {
        Socket socket;
        //接收缓冲区
        byte[] readBuff = new byte[1024];
        string recvStr = "";

        public void Connect()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //同步
            //socket.Connect("127.0.0.1", 9501);
            //异步
            socket.BeginConnect("127.0.0.1", 9501, ConnectCallback, socket);
        }

        /// <summary>
        /// Connect 回调
        /// </summary>
        /// <param name="ar"></param>
        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                //连接成功
                Socket socket = (Socket)ar.AsyncState;
                //socket.EndConnect(ar);
                int count = socket.EndReceive(ar);
                recvStr = System.Text.Encoding.Default.GetString(readBuff, 0, count);
                socket.BeginReceive(readBuff, 0, 1024, 0, ReceiveCallback, socket);
                Debug.Log("Socket Connect Succ");
            }
            catch (SocketException ex)
            {
                //连接失败
                Debug.Log("Socket Connect fail" + ex.ToString());
            }
        }

        //Receive回调
        public void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                Socket socket = (Socket)ar.AsyncState;
                int count = socket.EndReceive(ar);
                recvStr = System.Text.Encoding.Default.GetString(readBuff, 0, count);
                socket.BeginReceive(readBuff, 0, 1024, 0, ReceiveCallback, socket);
            }
            catch (SocketException ex)
            {
                Debug.Log("Socket Receive fail" + ex.ToString());
            }
        }

        //发送数据
        public void Send()
        {
            string input = "a";
            byte[] sendBytes = System.Text.Encoding.Default.GetBytes(input);
            //同步
            //socket.Send(sendBytes);
            //异步
            socket.BeginSend(sendBytes, 0, sendBytes.Length, 0, SendCallback, socket);
            //socket.Close();
        }
        /// <summary>
        /// 发送回调
        /// </summary>
        /// <param name="ar"></param>
        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket socket = (Socket)ar.AsyncState;
                int count = socket.EndSend(ar);
                Debug.Log("Socket Send succ" + count);
            }
            catch (SocketException ex)
            {
                Debug.Log("Socket Send fail" + ex.ToString());
            }
        }

        //public void Update()
        //{
        //    byte[] readBuff = new byte[1024];
        //    int count = socket.Receive(readBuff);
        //    string recvStr = System.Text.Encoding.Default.GetString(readBuff, 0, count);
        //    if (recvStr != null && recvStr != "")
        //    {
        //        print(recvStr);
        //    }
        //}

    }
}
