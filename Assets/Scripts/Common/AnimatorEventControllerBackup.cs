using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
namespace Common
{
    /// <summary>
    /// 动画事件管理 备份文件
    /// </summary>
    public class AnimatorEventControllerBackup : MonoBehaviour
    {
        private Animator anim;
        /// <summary>
        /// 攻击事件
        /// </summary>
        public event Action attackHandler;
        private void Start()
        {
            anim = GetComponent<Animator>();
            //自动添加动画事件
            AddAnimationEvent();
        }

        private void AddAnimationEvent()
        {
            //得到所有动画
            AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;

            for (int i = 0; i < clips.Length; i++)
            {
                //根据动画名字  找到你要添加的动画
                //print(clips[i].name);
                if (string.Equals(clips[i].name, "Attack1"))
                {
                    //添加动画事件开始
                    AnimationEvent events = new AnimationEvent();
                    events.functionName = "OnClickAtk01";
                    events.stringParameter = "Attack1";
                    events.time = 0.1f;
                    clips[i].AddEvent(events);

                    //AnimationEvent eventsCancel = new AnimationEvent();
                    ////添加动画事件结束
                    //eventsCancel.functionName = "OnCancelAnim";
                    //eventsCancel.stringParameter = "param_idletohit01";
                    //eventsCancel.time = clips[i].length * 0.9f;
                    //print(clips[i].length);
                    //print(eventsCancel.time);
                    //clips[i].AddEvent(eventsCancel);

                    AnimationEvent eventsEnd = new AnimationEvent();
                    //添加动画事件结束
                    eventsEnd.functionName = "OnEndAnim";
                    eventsEnd.stringParameter = "Attack1";
                    eventsEnd.time = clips[i].length;
                    clips[i].AddEvent(eventsEnd);
                }
                if (string.Equals(clips[i].name, "jump"))
                {
                    AnimationEvent eventsEnd = new AnimationEvent();
                    //添加动画事件结束
                    eventsEnd.functionName = "OnEndAnim";
                    eventsEnd.stringParameter = "param_idletojump";
                    eventsEnd.time = clips[i].length * 0.9f;
                    clips[i].AddEvent(eventsEnd);
                }
            }

            anim.Rebind();
        }


        private void OnClickAtk01(string animParam)
        {
            if (attackHandler != null)
            {
                attackHandler();
            }
        }

        private void OnCancelAnim(string animParam)
        {

            anim.SetBool(animParam, false);
        }

        private void OnEndAnim(string animParam)
        {
            anim.SetBool(animParam, false);
        }


        private void AddAnimationEvent2()
        {
            //得到所有动画
            AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;


            for (int i = 0; i < clips.Length; i++)
            {
                //根据动画名字  找到你要添加的动画

                if (string.Equals(clips[i].name, "atk01"))
                {
                    //添加动画事件
                    AnimationEvent events = new AnimationEvent();
                    events.functionName = "OnClickAtk01";
                    events.time = 0.1f;
                    clips[i].AddEvent(events);

                }

                if (string.Equals(clips[i].name, "atk02"))
                {
                    //添加动画事件
                    AnimationEvent events = new AnimationEvent();
                    events.functionName = "OnClickAtk02";
                    events.time = 0.1f;
                    clips[i].AddEvent(events);

                }
                if (string.Equals(clips[i].name, "atk03"))
                {
                    //添加动画事件
                    AnimationEvent events = new AnimationEvent();
                    events.functionName = "OnClickAtk03";
                    events.time = 1.0f;
                    clips[i].AddEvent(events);

                }

                if (string.Equals(clips[i].name, "atk05"))
                {
                    //添加动画事件
                    AnimationEvent events = new AnimationEvent();
                    events.functionName = "OnClickAtk05";
                    events.time = 0.14f;
                    clips[i].AddEvent(events);

                }

                if (string.Equals(clips[i].name, "atk08"))
                {
                    //添加动画事件
                    AnimationEvent events = new AnimationEvent();
                    events.functionName = "OnClickAtk08";
                    events.time = 0.25f;
                    clips[i].AddEvent(events);

                }
            }

            anim.Rebind();
        }


    }
}