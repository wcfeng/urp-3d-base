using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Common
{
    /// <summary>
    /// web请求
    /// </summary>
    public class WebRequest : MonoSingleton<WebRequest> 
    {
        public int code;
        public string message;
        public UnityWebRequest request;

        public IEnumerator GetRequest()
        {
            //string url = "https://www.baidu.com/";
            string url = "http://localhost:22222/test/test/a4";
            using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
            {

                //设置header
                webRequest.SetRequestHeader("Content-Type", "application/json");
                webRequest.SetRequestHeader("authKey", "leohui");

                yield return webRequest.SendWebRequest();

                if (webRequest.result == UnityWebRequest.Result.ProtocolError)
                {
                    Debug.LogError(webRequest.error);
                }
                else
                {
                    Debug.Log(webRequest.downloadHandler.text);
                }
            }
        }

        /// <summary>
        /// post请求
        /// </summary>
        /// <param name="data">请求数据</param>
        /// <param name="getResponse">相应方法</param>
        /// <returns></returns>
        public IEnumerator PostRequest(UnityEngine.Object data,Action<string> getResponse)
        {
            string url = "http://localhost:22222/api/visitor/login";
            using (request = new UnityWebRequest(url,"POST"))
            {
                string userJons = JsonUtility.ToJson(data);

                //string userJons = "{\"username\":\"zhangsan\",\"password\":\"123456\"}";

                byte[] bodyRaw = Encoding.UTF8.GetBytes(userJons);
                request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
                request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();

                //设置header
                request.SetRequestHeader("Content-Type", "application/json");
                request.SetRequestHeader("authKey", "leohui");

                yield return request.SendWebRequest();

                if (request.result == UnityWebRequest.Result.ProtocolError)
                {
                    getResponse(request.error);
                }
                else
                {
                    getResponse(request.downloadHandler.text);
                }
            }
        }
    }
}
