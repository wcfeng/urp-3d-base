using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    /// <summary>
    /// 物体生成器
    /// </summary>
    public class ObjectGenerator : MonoBehaviour
    {
        /// <summary>
        /// 该出生点生成的物体
        /// </summary>
        [Tooltip("该出生点生成的物体")]
        public GameObject target;
        /// <summary>
        /// 生成物体的总数量
        /// </summary>
        [Tooltip("生成物体的总数量")]
        public int totalNum = 10;
        /// <summary>
        /// 生成物体的时间间隔
        /// </summary>
        [Tooltip("生成物体的时间间隔")]
        public float intervalTime = 3;
        /// <summary>
        /// 最多同时存在多少个
        /// </summary>
        [Tooltip("最多同时存在多少个")]
        public int maxNum = 1;
        /// <summary>
        /// 半径范围
        /// </summary>
        [Tooltip("半径范围")]
        public float radius = 1;
        /// <summary>
        /// 生成物体的计数器
        /// </summary>
        private int counter;
        /// <summary>
        /// 生成的物体列表
        /// </summary>
        [HideInInspector]
        public List<GameObject> objs;

        void Start()
        {
            //初始时，物体计数为0；
            counter = 0;
            //重复生成物体
            InvokeRepeating("Create", 0.5F, intervalTime);
        }

        //方法，生成物体
        private void Create()
        {
            //最多同时存在多少个
            if (maxNum <= objs.Count) return;
            //生成一只物体
            Vector2 position = Random.insideUnitCircle* radius;
            GameObject gameObject = Instantiate(target, transform.position + new Vector3(position.x,0,position.y), Quaternion.identity);
            gameObject.AddComponent<IAttribute>();
            gameObject.GetComponent<IAttribute>().objectGenerator = this;
            objs.Add(gameObject);
            counter++;
            //如果计数达到最大值
            if (counter == totalNum)
            {
                //停止刷新
                CancelInvoke();
            }
        }
    }
}
