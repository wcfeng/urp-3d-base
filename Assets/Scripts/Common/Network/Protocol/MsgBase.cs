using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MsgBase
{
    /// <summary>
    /// Э����
    /// </summary>
    public string protocolName;
    public string controller;
    public string action;
    public string token;
    public int code;
    public string message;
    /// <summary>
    /// ����
    /// </summary>
    /// <param name="msgBase"></param>
    /// <returns></returns>
    public static byte[] Encode(MsgBase msgBase)
    {
        string s = JsonUtility.ToJson(msgBase);
        return System.Text.Encoding.UTF8.GetBytes(s);
    }
    public static MsgBase Decode(string protocolName,string data)
    {
        //string s = System.Text.Encoding.UTF8.GetString(bytes);
        MsgBase msgBase = (MsgBase)JsonUtility.FromJson(data, Type.GetType(protocolName));
        return msgBase;
    }
}
