using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//玩家相关协议
/// <summary>
/// 获取信息
/// </summary>
public class MsgGetInfo : MsgBase
{
    public MsgGetInfo()
    {
        protocolName = "MsgGetInfo";
        controller = "player";
        action = "GetInfo";
    }

    public int coin;
    public int exp;
}
/// <summary>
/// 保存信息
/// </summary>
public class MsgSaveInfo : MsgBase
{
    public MsgSaveInfo()
    {
        protocolName = "MsgSaveInfo";
        controller = "player";
        action = "SaveInfo";
    }

    public int coin;
    public int exp;
}
/// <summary>
/// 保存信息
/// </summary>
public class MsgCreatePlayer : MsgBase
{
    public MsgCreatePlayer()
    {
        protocolName = "MsgCreatePlayer";
        controller = "player";
        action = "CreatePlayer";
    }
    public string nickname;
}
