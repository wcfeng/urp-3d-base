﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 固定时间给服务端发送ping协议
/// </summary>
public class MsgPing : MsgBase
{
    public MsgPing()
    {
        protocolName = "MsgPing";
    }
}
/// <summary>
/// 固定时间接收pong协议
/// </summary>
public class MsgPong : MsgBase
{
    public MsgPong()
    {
        protocolName = "MsgPong";
    }
}
