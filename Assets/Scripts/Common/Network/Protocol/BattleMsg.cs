using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MsgMove : MsgBase
{
    public MsgMove()
    {
        protocolName = "MsgMove";
    }

    public float x = 0;
    public float y = 0;
    public float z = 0;
}

public class MsgAttack : MsgBase
{
    public MsgAttack()
    {
        protocolName = "Attack";
    }

    public string desc = "127.0.0.1:6222";
}
