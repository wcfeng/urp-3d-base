using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    /// <summary>
    /// 公共属性
    /// </summary>
    public class IAttribute : MonoBehaviour
    {
        /// <summary>
        /// 生成器
        /// </summary>
        public ObjectGenerator objectGenerator;

        public void OnDisable()
        {
            //从生成器移除
            objectGenerator?.objs.Remove(gameObject);
        }
    }
}
