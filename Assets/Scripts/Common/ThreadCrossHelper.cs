using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    /// <summary>
    /// 线程交叉访问助手类（需要手动挂在到场景中）
    /// </summary>
    public class ThreadCrossHelper : MonoSingleton<ThreadCrossHelper>
    {
        /// <summary>
        /// 延迟项目
        /// </summary>
        class DelayedItem
        {
            public Action CurrentAction { get; set; }
            public DateTime Time { get; set; }
        }
        /// <summary>
        /// 线程列表
        /// </summary>
        private List<DelayedItem> actionList;

        public override void Init()
        {
            base.Init();
            actionList = new List<DelayedItem>();
        }
        private void Update()
        {
            lock (actionList)
            {
                for (int i = actionList.Count - 1; i >= 0; i--)
                {
                    //如果到达执行时间
                    if (actionList[i].Time <= DateTime.Now)
                    {
                        //执行
                        actionList[i].CurrentAction();
                        //移除
                        actionList.RemoveAt(i);
                    }
                }
            }
        }
        /// <summary>
        /// 执行线程
        /// </summary>
        /// <param name="action">委托行为</param>
        /// <param name="delay">延迟时间</param>
        public void ExecuteOnMainThread(Action action, float delay = 0)
        {
            lock (actionList)
            {
                var item = new DelayedItem()
                {
                    CurrentAction = action,
                    Time = DateTime.Now.AddSeconds(delay)
                };
                actionList.Add(item);
            }
        }

        //示例
        //UnityEngine.UI.Text txtTime;
        //float second = 120;
        //public void Test()
        //{
        //    ThreadCrossHelper.Instance.ExecuteOnMainThread(() =>
        //    {
        //        txtTime.text = string.Format("{0}:{1}", second / 60, second % 60);
        //    });
        //    System.Threading.Thread.Sleep(1000);
        //}

    }
}
