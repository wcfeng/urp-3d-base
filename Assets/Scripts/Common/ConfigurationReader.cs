using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using myint=System.Int32; 
using ConfigDic = System.Collections.Generic.Dictionary
<string, System.Collections.Generic.Dictionary<string, string>>;
namespace Common
{
    /// <summary>
    /// StreamingAssets文件读取
    /// </summary>
    public static class ConfigurationReader
    {
        /// <summary>
        /// 获取配置文件
        /// </summary>
        /// <param name="filename">StreamingAssets下文件名称</param>
        /// <returns>文件内容</returns>
        public static string GetConfigFile(string filename)
        {
            //if(Application.platform == RuntimePlatform.Android)
            string url;
#if UNITY_EDITOR || UNITY_STANDALONE
            url = "file://" + Application.dataPath + "/StreamingAssets/" + filename;
#elif UNITY_IPHONE
            url = "file://" + Application.dataPath + "/Raw/"+ filename;
#elif UNITY_ANDROID
            url = "jar:file://" + Application.dataPath + "!/assets/"+ filename;
#elif UNITY_WEBGL
            url = System.IO.Path.Combine(Application.streamingAssetsPath, filename);
#endif
            //WWW www = new WWW(url);
            //while (true)
            //{
            //    if (www.isDone)
            //        return www.text;
            //}

            // 可换成以下
            UnityWebRequest www = UnityWebRequest.Get(url);
            www.SendWebRequest();
            while (true)
            {
                if (www.downloadHandler.isDone)
                    return www.downloadHandler.text;
            }
        }
        /// <summary>
        /// 读取配置文件
        /// </summary>
        /// <param name="fileContent">文件内容</param>
        /// <param name="handler">每行读取方法</param>
        public static void Reader(string fileContent,Action<string> handler)
        {
            using (StringReader reader = new StringReader(fileContent))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    //string[] keyValue = line.Split('=');
                    //configMap.Add(keyValue[0], keyValue[1]);
                    handler(line);
                }
                //string line = reader.ReadLine();
                //while(line != null)
                //{
                //    string[] keyValue = line.Split('=');
                //    configMap.Add(keyValue[0], keyValue[1]);
                //    line = reader.ReadLine();
                //}
            }
        }
        
        /// 处理所有数据   有了委托 不需要了
        private static ConfigDic BuildDic(string lines)
        {
            ConfigDic dic = new ConfigDic();
            string mainKey = null; //主键
            string subKey = null;  //子键
            string subValue = null;//值
            StringReader reader = new StringReader(lines);
            string line = null;
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Trim();//去除空白行
                if (!string.IsNullOrEmpty(line))
                {   //取主键 如 [Idle] 》》Idle
                    if (line.StartsWith("["))
                    {
                        mainKey = line.Substring(1, line.IndexOf("]") - 1);
                        dic.Add(mainKey, new Dictionary<string, string>());
                    }//取子键以及值
                    else
                    {
                        var configValue = line.Split('>');
                        subKey = configValue[0].Trim();
                        subValue = configValue[1].Trim();
                        dic[mainKey].Add(subKey, subValue);
                    }
                }
            }
            return dic;
        }
    }
}


