using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
using UnityWebSocket;
using System;

public static class WebSocketTool
{
    static WebSocket socket;
    //委托类型
    public delegate void MsgListener(MsgBase msgBase);
    //监听列表
    public static Dictionary<string, MsgListener> listeners = new Dictionary<string, MsgListener>();
    //消息列表
    static List<MsgBase> msgList = new List<MsgBase>();
    //消息列表长度
    static int msgCount = 0;
    //每一次update处理的消息量
    readonly static int MAX_MESSAGE_FIRE = 10;

    //心跳相关
    //是否启用心跳
    public static bool isUsePing = true; 
    //心跳间隔时间
    public static int pinginterval = 30;
    //上一次发送PING的时间
    static float lastPingTime = 0;
    //上一次收到PONG的时间
    public static float lastPongTime = 0; 

    /// <summary>
    /// 心跳检测
    /// </summary>
    private static void PingUpdate()
    {
        //是否启用
        if (!isUsePing) return;
        //每隔一段时间 发送ping
        if(Time.time - lastPingTime > pinginterval)
        {
            MsgPing msgPing = new MsgPing();
            Send(msgPing);
            lastPingTime = Time.time;
        }
        //每隔一段时间 检测pong时
        if(Time.time - lastPongTime > pinginterval * 4)
        {
            //关闭连接
            socket.CloseAsync();
        }

    }

    //添加监听
    public static void AddListener(string msgName, MsgListener listener)
    {
        if (listeners.ContainsKey(msgName))
        {
            //添加
            listeners[msgName] += listener;
        }
        else
        {
            //新增
            listeners[msgName] = listener;
        }
    }
    //添加监听
    public static void RemoveListener(string msgName, MsgListener listener)
    {
        if (listeners.ContainsKey(msgName))
        {
            listeners[msgName] -= listener;
            //删除
            if(listeners[msgName] == null)
            {
                listeners.Remove(msgName);
            }
        }
    }

    private static void InitState()
    {
        msgList = new List<MsgBase>();
        msgCount = 0;

        //上一次发送PING的时间
        lastPingTime = Time.time;
        //上一次收到PONG的时间
        lastPongTime = Time.time;
    }
    //获取描述
    public static string GetDesc()
    {
        if (socket == null) return "";
        Debug.Log(socket);
        return "desc test";
    }

    public static void Connect(string ip, int port)
    {
        // 创建实例
        //string address = "ws://127.0.0.1:9501";
        string address = string.Format("ws://{0}:{1}", ip,port);
        socket = new WebSocket(address);

        // 注册回调
        socket.OnOpen += OnOpen;
        socket.OnClose += OnClose;
        socket.OnMessage += OnMessage;
        socket.OnError += OnError;

        // 连接
        socket.ConnectAsync();
        
        // 关闭连接
        //socket.CloseAsync();
    }

    /// <summary>
    /// 发送数据
    /// </summary>
    /// <param name="sendStr">字符串数据</param>
    public static void Send(string sendStr)
    {
        if (socket == null) return;
        if (socket.ReadyState != WebSocketState.Open) return;

        // 发送 string 类型数据
        socket.SendAsync(sendStr);
    }

    public static void Send(MsgBase msgBase)
    {
        if (socket == null) return;
        if (socket.ReadyState != WebSocketState.Open) return;

        byte[] bytes = MsgBase.Encode(msgBase);

        socket.SendAsync(bytes);
    }

    /// <summary>
    /// 发送数据
    /// </summary>
    /// <param name="data">对象数据</param>
    public static void Send(UnityEngine.Object data)
    {
        if (socket == null) return;
        if (socket.ReadyState != WebSocketState.Open) return;
        string sendStr = JsonUtility.ToJson(data);

        Debug.Log(sendStr);
        //异步
        var bytes = System.Text.Encoding.UTF8.GetBytes(sendStr);
        // 或者 发送 byte[] 类型数据（建议使用）
        socket.SendAsync(bytes);
    }

    //Update 
    public static void Update()
    {
        msgUpdate();
        PingUpdate();
        //if (msgList.Count <= 0)
        //    return;
        //string msgStr = msgList[0];
        //Debug.Log(msgStr);
        //msgList.RemoveAt(0);
        //if (msgStr == "") return;
        //string[] split = msgStr.Split('|');
        //string msgName = split[0];
        //string msgArgs = split[1];
        //监听回调；
        //if (listeners.ContainsKey(msgName))
        //{
        //    listeners[msgName](msgArgs);
        //}
    }

    /// <summary>
    /// 处理消息
    /// </summary>
    private static void msgUpdate()
    {
        if (msgCount == 0) return;
        for (int i = 0; i < MAX_MESSAGE_FIRE; i++)
        {
            MsgBase msgBase = null;
            lock (msgList)
            {
                if(msgCount > 0)
                {
                    msgBase = msgList[0];
                    msgList.RemoveAt(0);
                    msgCount--;
                }
            }
            //分发消息
            if(msgBase != null)
            {
                FireMsg(msgBase.protocolName, msgBase);
            }
            else
            {
                //没有消息  跳出循环
                break;
            }
        }
    }

    /// <summary>
    /// 分发消息
    /// </summary>
    /// <param name="msgName"></param>
    /// <param name="msgBase"></param>
    private static void FireMsg(string msgName,MsgBase msgBase)
    {
        if (listeners.ContainsKey(msgName))
        {
            listeners[msgName](msgBase);
        }
    }

    private static void OnError(object sender, ErrorEventArgs e)
    {
        Debug.Log(string.Format("Error: {0}", e.Message));
    }

    private static void OnMessage(object sender, MessageEventArgs e)
    {
        //接收到数据  解析数据
        try
        {
            //正确格式的数据
            //从父协议中获取到子协议  再次解析协议
            MsgBase mb = MsgBase.Decode("MsgBase", e.Data);
            MsgBase msgData = MsgBase.Decode(mb.protocolName, e.Data);
            //添加到消息队列
            lock (msgList)
            {
                msgList.Add(msgData);
            }
            msgCount ++;
        }
        catch (Exception ex)
        {
            //错误格式的数据
            Debug.Log(ex.Message);
            return;
        }
    }

    private static void OnClose(object sender, CloseEventArgs e)
    {
        Debug.Log(string.Format("Closed: StatusCode: {0}, Reason: {1}", e.StatusCode, e.Reason));
    }

    private static void OnOpen(object sender, OpenEventArgs e)
    {
        Debug.Log(string.Format("Connected"));
    }
}
