using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Common
{
    /// <summary>
    /// NetManager
    /// </summary>
    public static class NetManager
    {
        //定义套接字
        static Socket socket;
        //接收缓冲区
        static byte[] readBuff = new byte[1024];
        //委托类型
        public delegate void MsgListener(String str);
        //监听列表
        private static Dictionary<string, MsgListener> listeners =
        new Dictionary<string, MsgListener>();
        //消息列表
        static List<String> msgList = new List<string>();
        //添加监听
        public static void AddListener(string msgName, MsgListener listener)
        {
            listeners[msgName] = listener;
        }
        //获取描述
        public static string GetDesc()
        {
            if (socket == null) return "";
            if (!socket.Connected) return "";
            return socket.LocalEndPoint.ToString();
        }
        //连接
        public static void Connect(string ip, int port)
        {
            //Socket
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.BeginConnect(ip, port, ConnectCallback, socket);
        }

        /// <summary>
        /// Connect 回调
        /// </summary>
        /// <param name="ar"></param>
        private static void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                //连接成功
                Socket socket = (Socket)ar.AsyncState;
                //socket.EndConnect(ar);
                int count = socket.EndReceive(ar);
                string recvStr = System.Text.Encoding.Default.GetString(readBuff, 0, count);
                socket.BeginReceive(readBuff, 0, 1024, 0, ReceiveCallback, socket);
                Debug.Log("Socket Connect Succ");
            }
            catch (SocketException ex)
            {
                //连接失败
                Debug.Log("Socket Connect fail" + ex.ToString());
            }
        }

        /// <summary>
        /// Receive 回调
        /// </summary>
        /// <param name="ar"></param>
        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                Socket socket = (Socket)ar.AsyncState;
                int count = socket.EndReceive(ar);
                string recvStr = System.Text.Encoding.Default.GetString(readBuff, 0, count);
                msgList.Add(recvStr);
                socket.BeginReceive(readBuff, 0, 1024, 0, ReceiveCallback, socket);
            }
            catch (SocketException ex)
            {
                Debug.Log("Socket Receive fail" + ex.ToString());
            }
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="sendStr">字符串数据</param>
        public static void Send(string sendStr)
        {
            if (socket == null) return;
            if (!socket.Connected) return;

            //直接发送
            //byte[] sendBytes = System.Text.Encoding.Default.GetBytes(sendStr);
            //粘包解决  长度信息法 前面加数量
            byte[] sendBytes = SendBefore(sendStr);
            //异步
            socket.BeginSend(sendBytes, 0, sendBytes.Length, 0, SendCallback, socket);
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="data">对象数据</param>
        public static void Send(UnityEngine.Object data)
        {
            if (socket == null) return;
            if (!socket.Connected) return;
            string userJons = JsonUtility.ToJson(data);
            //直接发送 
            //byte[] sendBytes = Encoding.UTF8.GetBytes(userJons);
            //粘包解决  长度信息法 前面加数量
            byte[] sendBytes = SendBefore(userJons);

            Debug.Log(userJons);
            //异步
            socket.BeginSend(sendBytes, 0, sendBytes.Length, 0, SendCallback, socket);
        }
        /// <summary>
        /// 发送回调
        /// </summary>
        /// <param name="ar"></param>
        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket socket = (Socket)ar.AsyncState;
                int count = socket.EndSend(ar);
                Debug.Log("Socket Send succ" + count);
            }
            catch (SocketException ex)
            {
                Debug.Log("Socket Send fail" + ex.ToString());
            }
        }
        /// <summary>
        /// 长度信息法(解决粘包)
        /// </summary>
        /// <param name="sendStr"></param>
        /// <returns></returns>
        private static byte[] SendBefore(string sendStr)
        {
            byte[] bodyBytes = Encoding.Default.GetBytes(sendStr);
            int len = (int)bodyBytes.Length;
            byte[] lenBytes = BitConverter.GetBytes(len);
            byte[] sendBytes = lenBytes.Concat(bodyBytes).ToArray();
            return sendBytes;
        }
        //Update 
        public static void Update()
        {
            if (msgList.Count <= 0)
                return;
            string msgStr = msgList[0];
            Debug.Log(msgStr);
            msgList.RemoveAt(0);
            if (msgStr == "") return;
            string[] split = msgStr.Split('|');
            string msgName = split[0];
            string msgArgs = split[1];
            //监听回调；
            if (listeners.ContainsKey(msgName))
            {
                listeners[msgName](msgArgs);
            }
        }
    }
}
