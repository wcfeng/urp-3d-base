using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Common
{
    /// <summary>
    /// 动画事件
    /// </summary>
    public class AnimationEventBehaviour : MonoBehaviour
    {
        /// <summary>
        /// 动画控制
        /// </summary>
        private Animator anim;
        /// <summary>
        /// 攻击事件
        /// </summary>
        public event Action attackHandler;
        private void Start()
        {
            anim = GetComponent<Animator>();
        }

        private void OnCancelAnim(string animParam)
        {

            anim.SetBool(animParam, false);
        }
        private void CancelAnim(string animParam)
        {

            anim.SetBool(animParam, false);
        }
        private void OnMeleeAttack(string animParam)
        {

            if (attackHandler != null)
            {
                attackHandler();
            }
        }

        private void OnAttack()
        {
            if(attackHandler != null)
            {
                attackHandler();
            }
        }
    }
}
