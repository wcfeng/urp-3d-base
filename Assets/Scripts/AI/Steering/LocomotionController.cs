﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace AI.Steering
{
    /// <summary>
    /// 运动控制
    /// </summary>
    public class LocomotionController : Vehicle
    {
        /// <summary>
        /// 转向 :当前操控方向
        /// </summary>
        public void Rotation()
        {
            if (currentForce != Vector3.zero)
            {
                var dir = Quaternion.LookRotation(currentForce);
                transform.rotation = Quaternion.Lerp(transform.rotation,
                    dir, rotationSpeed * Time.deltaTime);
            }
        }
        /// <summary>
        /// 移动
        /// </summary>
        public void Movement()
        {
            //1计算出当前的操控方向和速度
            currentForce += finalForce * Time.deltaTime;
            currentForce = Vector3.ClampMagnitude(currentForce, maxSpeed);
            //2移动
            transform.position += currentForce * Time.deltaTime;
        }
        /// <summary>
        /// 播放动画
        /// </summary>
        public void PlayAnimation() { }
        ///以上方法，需要反复调用
        private void Update()
        {
            Rotation();
            Movement();
            PlayAnimation();
        }
    }
}
