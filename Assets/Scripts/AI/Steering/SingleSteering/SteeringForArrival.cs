﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace AI.Steering
{
    /// <summary>
    /// 3 到达
    /// </summary>
    public class SteeringForArrival:Steering
    {
        /// <summary>
        /// 减速区半径
        /// </summary>
        public float slowdownDistance = 5;
        /// <summary>
        /// 到达区半径
        /// </summary>
        public float arrivalDistance = 2;
        public override Vector3 GetForce()
        {
            float distance = Vector3.Distance(target.position, transform.position) - arrivalDistance;
            //减速区外：靠近算法 保持在最高速
            float realSpeed = speed;
            //到达区内 ：速度为0，
            if(distance<=0)
            return Vector3.zero;
            //减速区内 速度递减
            if (distance < slowdownDistance)
            {
                realSpeed = distance / (slowdownDistance - arrivalDistance) * speed;
                realSpeed = realSpeed < 1 ? 1 : realSpeed; // ? :
            }
            //print(realSpeed);
            expectForce = (target.position - transform.position).normalized * realSpeed;
            return (expectForce - vehicle.currentForce) * weight;
        }
    }
}
