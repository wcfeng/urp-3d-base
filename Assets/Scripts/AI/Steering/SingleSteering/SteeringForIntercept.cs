﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace AI.Steering
{
    /// <summary>
    /// 4 拦截 此方法存在一定问题
    /// </summary>
    class SteeringForIntercept : Steering
    {
        //画拦截点
        private Vector2 tempPiont;
        public void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(tempPiont, 1);
        }
        public override Vector3 GetForce()
        {
            //拦截 注意在 自己前方一定的角度内拦截，在侧边不拦截
            //1目标和运动体的距离
            var toTarget = target.position - transform.position;
            var angle = Vector3.Angle(target.forward, toTarget);
            if (angle > 20 && angle < 160)
            {
                //2时间
                var targetSpeed = target.GetComponent<Vehicle>().currentForce.magnitude;
                var time = toTarget.magnitude / (targetSpeed + vehicle.currentForce.magnitude);
                //3推断时间内走的距离
                var runDistance = targetSpeed * time;
                //4拦截点位置
                var interceptPoint = target.position + target.forward * runDistance;
                tempPiont = interceptPoint;
                //5期望（操控力）
                expectForce = (interceptPoint - transform.position).normalized * speed;     
            }
            else
            {
                expectForce = toTarget.normalized * speed;//靠近算法
            }
            //返回
            //6 实际=（期望-当前）*权重
            return (expectForce - vehicle.currentForce) * weight;
        }
    }
}
