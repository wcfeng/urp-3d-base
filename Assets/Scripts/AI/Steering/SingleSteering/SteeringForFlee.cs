﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace AI.Steering
{
    /// <summary>
    ///2 远离
    /// </summary>
    public class SteeringForFlee : Steering
    {
        /// <summary>
        /// 离开的最大距离
        /// </summary>
        public float safeDistance = 10;
        public override Vector3 GetForce()
        {
            if (target == null) return Vector3.zero;
            var dir=transform.position-target.position;
            if(dir.magnitude<safeDistance)
            {
                 //当前
                //vehicle.currentForce
                //期望 =(自身位置-？？)
                expectForce = dir.normalized * speed;
                //实际=（期望-当前）*权重
                var realForce = (expectForce - vehicle.currentForce) * weight;
                 return realForce;
            }
            else
            {
                return Vector3.zero;
            }           
        }
    }
}
