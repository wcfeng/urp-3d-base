﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace AI.Steering
{
    /// <summary>
    ///1 靠近
    /// </summary>
    public class SteeringForSeek:Steering
    {
        public override Vector3 GetForce()
        {
            //没有找到目标
            if (target == null) return Vector3.zero;
            //期望 = (目标位置-？？)
            //获取指向目标位置的向量 * 速度
            expectForce = (target.position - transform.position).normalized * speed;
            //实际=（期望-当前）*权重
            Vector3 realForce = (expectForce - vehicle.currentForce) * weight;
            return realForce;
        }
    }
}
