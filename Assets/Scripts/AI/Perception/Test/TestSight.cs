﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AI.Perception;

public class TestSight : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // SensorTriggerSystem.Instance.AddSensor(GetComponent<SightSensor>());
        //注册视觉感应器事件
        GetComponent<SightSensor>().OnPerception += TestSight_OnPerception;
        GetComponent<SightSensor>().OnNonPerception += TestSight_OnNonPerception;
    }

    private void TestSight_OnNonPerception()
    {
        print("看不见");
    }

    private void TestSight_OnPerception(List<AbstractTrigger> triggerList)
    {
        triggerList.ForEach(t => print("看见了:" + t.name));
    }
}
