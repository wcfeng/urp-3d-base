﻿using System;
using System.Collections.Generic;
using UnityEngine;
namespace AI.Perception
{
    /// <summary>
    /// 感知触发系统
    /// </summary>
    public class SensorTriggerSystem : Common.MonoSingleton<SensorTriggerSystem>
    {
        /// <summary>
        /// 检查时间间隔
        /// </summary>
        public float checkInterval = 0.2f;
        /// <summary>
        /// 感应器列表
        /// </summary>
        private List<AbstractSensor> sensorList = new List<AbstractSensor>();
        /// <summary>
        /// 触发器列表
        /// </summary>
        private List<AbstractTrigger> triggerList = new List<AbstractTrigger>();
        /// <summary>
        /// 添加感应器
        /// </summary>
        public void AddSensor(AbstractSensor sensor)
        {
            sensorList.Add(sensor);
        }
        /// <summary>
        /// 添加触发器
        /// </summary>
        public void AddTrigger(AbstractTrigger trigger)
        {
            triggerList.Add(trigger);
        }
        /// <summary>
        /// 检查触发条件:每个感应器检查 对于触发器
        /// </summary>
        private void CheckTrigger()
        {
            for (int i = 0; i < sensorList.Count; i++)
            {
                if (sensorList[i].enabled)
                {
                    //感应器检查所有触发器
                    sensorList[i].OnTestTrigger(triggerList);
                }
            }
        }
        /// <summary>
        /// 更新系统
        /// </summary>
        private void UpdateSystem()
        {
            sensorList.RemoveAll(s => !s.isEnable);
            triggerList.RemoveAll(t => !t.isEnable);
        }

        private void Oncheck()
        {
            UpdateSystem();
            CheckTrigger();
        }

        private void OnEnable()
        {
            InvokeRepeating("Oncheck", 0, checkInterval);
        }
        private void OnDisable()
        {
            CancelInvoke("Oncheck");
        }



    }
}
