﻿namespace AI.Perception
{
    /// <summary>
    /// 触发器类型
    /// </summary>
    public enum TriggerType
    {
		/// <summary>
        /// 视觉
        /// </summary>
        Sight,
		/// <summary>
        /// 听觉
        /// </summary>
        Sound,
    }
}
