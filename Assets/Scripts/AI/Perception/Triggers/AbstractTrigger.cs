﻿using UnityEngine;
namespace AI.Perception
{
    /// <summary>
    /// 抽象触发器
    /// </summary>
    public abstract class AbstractTrigger:MonoBehaviour
    {
        /// <summary>
        /// 是否启用
        /// </summary>
		public bool isEnable = true;
        /// <summary>
        /// 触发器类型
        /// </summary>
        public TriggerType triggerType;
        private void Start() {
            Init();
            //把当前触发器 加入到 感应触发系统
            SensorTriggerSystem sys = SensorTriggerSystem.Instance;
            sys.AddTrigger(this);
        }

        public abstract void Init();

        private void OnDestroy() {
            isEnable = false;
        }

    }
}
