﻿using UnityEngine;
namespace AI.Perception
{
    /// <summary>
    /// 视觉触发器
    /// </summary>
    public class SightTrigger:AbstractTrigger
    {
        /// <summary>
        /// 接收感知的位置 默认当前物体
        /// </summary>
        [Tooltip("接收感知的位置 默认值为当前物体")]
        public Transform recievePos;
        public override void Init(){
            if(!recievePos){
                recievePos = transform;
            }
            triggerType = TriggerType.Sight;
        }
    }
}
