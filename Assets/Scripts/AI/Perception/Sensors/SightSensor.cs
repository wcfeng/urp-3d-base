﻿using UnityEngine;
using System;
using System.Collections.Generic;
namespace AI.Perception
{
    /// <summary>
    /// 视觉感应器
    /// </summary>
    public class SightSensor : AbstractSensor
    {
        /// <summary>
        /// 视距
        /// </summary>
        public float sightDistance = 10;
        /// <summary>
        /// 启用视角检测
        /// </summary>
        public bool enabledAngle;
        /// <summary>
        /// 视角
        /// </summary>
        public float sightAngle = 90;
        /// <summary>
        /// 启用遮挡检测
        /// </summary>
        public bool enabledRay;
        /// <summary>
        /// 忽略的层
        /// </summary>
        [Tooltip("忽略的层")]
        public LayerMask layerMask;
        /// <summary>
        /// 发送感知的位置
        /// </summary>
        [Tooltip("发送感知的位置 默认值为当前物体")]
        public Transform sendPos;
        [Header("视野范围显示")]
        /// <summary>
        /// 视野显示模式
        /// </summary>
        [Tooltip("视野显示模式")]
        public FieldOfViewShow fieldOfViewShow;
        /// <summary>
        /// 发送感知的子物体名称
        /// </summary>
        [Tooltip("发送感知的子物体名称")]
        public string viewPos = "FieldOfViewShow";
        private MeshFilter viewMeshFilter;
        /// <summary>
        /// 视野范围显示
        /// </summary>
        [Tooltip("视野范围显示")]
        public enum FieldOfViewShow
        {
            /// <summary>
            /// 不显示
            /// </summary>
            None,
            /// <summary>
            /// scene显示
            /// </summary>
            Debug,
            /// <summary>
            /// game显示
            /// </summary>
            Show,
        }
        /// <summary>
        /// 射线数量，越多则越密集
        /// </summary>
        [Tooltip("视野范围显示")]
        public int viewLines = 30;
        /// <summary>
        /// 顶点列表
        /// </summary>
        private List<Vector3> viewVerts;
        /// <summary>
        /// 顶点序号列表
        /// </summary>
        private List<int> viewIndices;
        public override void Init()
        {
            if (!sendPos)
            {
                sendPos = transform;
            }

            Transform viewTF = transform.Find(viewPos);
            switch (fieldOfViewShow)
            {
                case FieldOfViewShow.Debug:
                    viewMeshFilter = viewTF.GetComponent<MeshFilter>();
                    break;
                case FieldOfViewShow.Show:
                    viewMeshFilter = viewTF.GetComponent<MeshFilter>();
                    viewVerts = new List<Vector3>();
                    viewIndices = new List<int>();
                    break;
                default:
                    break;
            }
            
        }
        /// <summary>
        /// 检测是否存在感知对象
        /// </summary>
        /// <param name="trigger">触发器对象</param>
        /// <returns></returns>
        protected override bool TestTrigger(AbstractTrigger trigger)
        {
            //视觉触发过滤
            if (trigger.triggerType != TriggerType.Sight) return false;
            SightTrigger trigger1 = trigger as SightTrigger;
            //检测是否在视野范围内
            Vector3 dir = trigger1.recievePos.position - sendPos.position;
            bool b1 = dir.magnitude < sightDistance;
            bool b2 = true;
            bool b3 = true;
            //判断视角
            if (enabledAngle)
            {
                b2 = Vector3.Angle(transform.forward, dir) < sightAngle / 2;
            }
            //判断层
            if (enabledRay)
            {
                RaycastHit hit;
                b3 = Physics.Raycast(sendPos.position, dir, out hit, sightDistance,~layerMask) && hit.collider.gameObject == trigger.gameObject;
            }
            return b1 && b2 && b3;
        }

        private void Update()
        {
            switch (fieldOfViewShow)
            {
                case FieldOfViewShow.Debug:
                    DebugDraw();
                    break;
                case FieldOfViewShow.Show:
                    FieldOfView();
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 显示视野
        /// </summary>
        void FieldOfView()
        {
            // 清空顶点列表
            viewVerts.Clear();
            viewVerts.Add(Vector3.zero); // 加入起点坐标，局部坐标系
                                         // 获得最左边那条射线的向量，相对正前方，角度是 -45°
            Vector3 forward_left = Quaternion.Euler(0, -45, 0) * transform.forward * sightDistance;
            // 依次处理每一条射线
            for (int i = 0; i <= viewLines; i++)
            {
                // 每条射线都在 forward_left 的基础上偏转一点，最后一个正好偏转 90°到视线最右侧
                Vector3 v = Quaternion.Euler(0, (90.0f / viewLines) * i, 0) * forward_left;
                // 角色位置加 v，就是射线终点 pos
                Vector3 pos = transform.position + v;
                // 实际发射射线。注意 RayCast 的参数，重载很多容易出错
                RaycastHit hitInfo;
                if (Physics.Raycast(transform.position, v, out hitInfo, sightDistance))
                {
                    // 碰到物体，终点改为碰到的点
                    pos = hitInfo.point;
                }
                // 将每个点的位置加入列表，注意转为局部坐标系
                Vector3 p = transform.InverseTransformPoint(pos);
                viewVerts.Add(p);
            }
            // 根据顶点位置绘制模型
            RefreshView();
        }
        /// <summary>
        /// 根据顶点位置绘制模型
        /// </summary>
        void RefreshView()
        {
            viewIndices.Clear();
            // 逐个加入三角面，每个三角面都以起点开始
            for (int i = 1; i < viewVerts.Count - 1; i++)
            {
                viewIndices.Add(0); // 起点，角色位置
                viewIndices.Add(i);
                viewIndices.Add(i + 1);
            }
            // 填写 Mesh 信息
            Mesh mesh = new Mesh();
            mesh.vertices = viewVerts.ToArray();
            mesh.triangles = viewIndices.ToArray();
            viewMeshFilter.mesh = mesh;
        }

        /// <summary>
        /// 画虚线 仅在scene窗口有效
        /// </summary>
        void DebugDraw()
        {
            List<Vector3> points = new List<Vector3>();
            // 绘制视线范围开始
            Func<bool> _DrawRange = () =>
            {
                List<int> tris = new List<int>();
                for (int i = 2; i < points.Count; i++)
                {
                    tris.Add(0);
                    tris.Add(i - 1);
                    tris.Add(i);
                }
                Mesh mesh = new Mesh();
                mesh.vertices = points.ToArray();
                mesh.triangles = tris.ToArray();
                mesh.RecalculateBounds();
                mesh.RecalculateNormals();
                mesh.RecalculateTangents();

                viewMeshFilter.mesh = mesh;
                return true;
            };
            Vector3 offset = new Vector3(0, 1, 0); // 每个顶点都抬高一点
            points.Add(offset);
            for (int d = -60; d < 60; d += 4)
            {
                Vector3 v = Quaternion.Euler(0, d, 0) * transform.forward;
                Ray ray = new Ray(transform.position + offset, v);
                RaycastHit hitInfo;
                if (!Physics.Raycast(ray, out hitInfo, sightDistance))
                {
                    Vector3 localv = transform.InverseTransformVector(v);
                    points.Add(offset + localv * sightDistance);
                    Debug.DrawLine(transform.position, transform.position + v * sightDistance, Color.red);
                }
                else
                {
                    Vector3 local = transform.InverseTransformPoint(hitInfo.point);
                    points.Add(offset + local);
                    Debug.DrawLine(transform.position, hitInfo.point, Color.red);
                }
            }
            _DrawRange();
            // 绘制视线范围结束
        }
    }
}
