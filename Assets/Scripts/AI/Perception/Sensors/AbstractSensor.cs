﻿using UnityEngine;
using System;
using System.Collections.Generic;
namespace AI.Perception
{
    /// <summary>
    /// 抽象感应器
    /// </summary>
    public abstract class AbstractSensor : MonoBehaviour
    {
        /// <summary>
        /// 是否启用
        /// </summary>
		public bool isEnable = true;
        /// <summary>
        /// 没有感知事件
        /// </summary>
        public event Action OnNonPerception;
        /// <summary>
        /// 感知事件
        /// </summary>
        public event Action<List<AbstractTrigger>> OnPerception;
        private void Start()
        {
            Init();
            //把当前感应器 放到 感知系统中
            SensorTriggerSystem sys = SensorTriggerSystem.Instance;
            sys.AddSensor(this);
        }

        public abstract void Init();

        private void OnDestroy()
        {
            isEnable = false;
        }

        /// <summary>
        /// 检测触发器 检查所有的触发器【触发 条件】
        /// </summary>
        /// <param name="triggerList">触发器列表</param>
        public void OnTestTrigger(List<AbstractTrigger> triggerList)
        {
            //找到启用的、忽略当前物体、检测到感知的 所有触发器
            triggerList = triggerList.FindAll(t => t.isEnable && t.gameObject != this.gameObject && TestTrigger(t));
            //触发感知事件
            if (triggerList.Count > 0)
            {
                if (OnPerception != null)
                    OnPerception(triggerList);
            }
            else
            {
                if (OnNonPerception != null)
                    OnNonPerception();
            }
        }
        /// <summary>
        /// 检测触发器是否被感知
        /// </summary>
        protected abstract bool TestTrigger(AbstractTrigger trigger);

    }
}
