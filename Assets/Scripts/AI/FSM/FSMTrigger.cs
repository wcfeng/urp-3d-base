using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.FSM
{
    /// <summary>
    /// 条件类
    /// </summary>
    public abstract class FSMTrigger
    {
        /// <summary>
        /// 条件id
        /// </summary>
        public FSMTriggerID triggerId;
        public FSMTrigger()
        {
            Init();
        }
        /// <summary>
        /// 初始化
        /// </summary>
        public abstract void Init();
        /// <summary>
        /// 检测是否达成条件
        /// </summary>
        /// <param name="fsm">状态机</param>
        /// <returns></returns>
        public abstract bool HandleTrigger(FSMBase fsm);       
    }
}
