using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace AI.FSM
{
    /// <summary>
    /// 攻击状态
    /// </summary>
    class AttackState:FSMState
    {
        public override void Init()
        {
            stateId = FSMStateID.Attack;
        }
        public override void ActionState(FSMBase fsm)
        {
            base.ActionState(fsm);
            //朝向目标
            fsm.transform.LookAt(fsm.targetTF);
            //使用随机技能
            fsm.AutoUseSkill();
        }
        public override void EnterState(FSMBase fsm)
        {
            fsm.StopMove();
            //fsm.PlayAnimation(fsm.animParams.Idle);//!!!!
        }
        public override void ExitState(FSMBase fsm){}
    }
}
