using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.FSM
{
    /// <summary>
    /// 死亡状态
    /// </summary>
    public class DeadState : FSMState
    {
        public override void Init()
        {
            stateId = FSMStateID.Dead;
        }
        public override void EnterState(FSMBase fsm)
        {
            fsm.enabled = false;//死亡状态 不再转入其它状态
        }
    }
}
