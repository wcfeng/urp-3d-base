using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.FSM
{
    /// <summary>
    /// 追逐状态
    /// </summary>
    public class PursuitState:FSMState
    {
        public override void Init()
        {
            stateId = FSMStateID.Pursuit;
        }        
        public override void ActionState(FSMBase fsm)
        {
            base.ActionState(fsm);
            //1：条件需要有追逐的目标 
            if (fsm.targetTF == null) return;
            //2:  主要控制追的 速度，靠近的距离=攻击距离
            fsm.MoveToTarget(fsm.targetTF.position,
                fsm.moveSpeed,
                fsm.chState.stateData.attackDistance);//attackDistance不要为零            
        }
        public override void ExitState(FSMBase fsm)
        {   //4:停下来-转换状态【追逐>攻击] 
            fsm.StopMove();
            base.ExitState(fsm);
            fsm.anim.SetFloat(Const.Animation.Speed, 1);
        }

        public override void EnterState(FSMBase fsm)
        {
            base.EnterState(fsm);
            fsm.anim.SetFloat(Const.Animation.Speed, 1);
        }
    }
}
