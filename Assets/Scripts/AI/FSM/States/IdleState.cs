using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.FSM
{
    /// <summary>
    /// 待机状态
    /// </summary>
    public class IdleState:FSMState
    {
        public override void Init()
        {
            //设置状态编号为 待机状态的编号
            stateId = FSMStateID.Idle;
        }        
        public override void EnterState(FSMBase fsm)
        {
            //播放待机动画
            base.EnterState(fsm);
            if(fsm.anim) fsm.anim.SetFloat(Const.Animation.Speed,0);
            
        }
        public override void ExitState(FSMBase fsm)
        {
            //播放待机动画
            base.ExitState(fsm);
            if(fsm.anim) fsm.anim.SetFloat(Const.Animation.Speed, 0);
        }
    }
}
