using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace AI.FSM
{
    /// <summary>
    /// 巡逻状态
    /// </summary>
    public  class PatrolState:FSMState
    {
        public override void Init()
        {
            stateId = FSMStateID.Patrol;
        }
        /// <summary>
        /// 路点索引
        /// </summary>
        private int index;
        public override void ActionState(FSMBase fsm)
        {
            //1是否到达当前路点
            if(Vector3.Distance(fsm.transform.position,fsm.wayPoints[index].position) < fsm.patrolArrivalDistance)
            {
                //2是否是最后一个路点
                if (index == fsm.wayPoints.Length - 1)
                {
                    //根据巡逻的方式，决定 结束，再次开始【循环，来回】
                    switch (fsm.patrolMode)
                    { 
                        case PatrolMode.Once:
                            fsm.IsPatrolComplete = true;
                            return;
                        case PatrolMode.PingPong:
                            //翻转数组
                            Array.Reverse(fsm.wayPoints);//[0,1,2]
                            index += 1;
                            break;
                    }
                }
                //下一路点
                index = (index + 1) % fsm.wayPoints.Length; 
            }
            //移动
            fsm.MoveToTarget(fsm.wayPoints[index].position,fsm.patrolSpeed, fsm.patrolArrivalDistance);
        }
        public override void EnterState(FSMBase fsm)
        {
            base.EnterState(fsm);
            fsm.IsPatrolComplete = false;
            //播放动画
            fsm.anim.SetFloat(Const.Animation.Speed, 0.5f);
        }
        public override void ExitState(FSMBase fsm)
        {
            base.ExitState(fsm);
            fsm.StopMove();
            fsm.anim.SetFloat(Const.Animation.Speed, 0f);
        }
    }
}
