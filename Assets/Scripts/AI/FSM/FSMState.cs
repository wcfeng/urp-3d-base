using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace AI.FSM
{
    /// <summary>
    /// 状态抽象类
    /// </summary>
    abstract public class FSMState
    {
        /// <summary>
        /// 状态id
        /// </summary>
        public FSMStateID stateId;
        /// <summary>
        /// 条件列表 要达到状态的条件
        /// </summary>
        private List<FSMTrigger> triggers;
        /// <summary>
        /// 转换映射表
        /// </summary>
        private Dictionary<FSMTriggerID, FSMStateID> map;

        public FSMState()
        {
            map = new Dictionary<FSMTriggerID, FSMStateID>();
            triggers = new List<FSMTrigger>();
            Init();
        }
        /// <summary>
        /// 初始化
        /// </summary>
        abstract public void Init();
        /// <summary>
        /// 添加条件和状态的映射
        /// </summary>
        /// <param name="triggerid">条件id</param>
        /// <param name="stateid">状态id</param>
        public void AddMap(FSMTriggerID triggerid, FSMStateID stateid)
        {
            if (map.ContainsKey(triggerid))
            {
                map[triggerid] = stateid;
            }
            else
            {
                map.Add(triggerid, stateid);
                CreateTrigger(triggerid);
            }
        }
        /// <summary>
        /// 反射添加条件对象
        /// </summary>
        /// <param name="triggerId">条件id</param>
        private void CreateTrigger(FSMTriggerID triggerId)
        {
            Type type = Type.GetType("AI.FSM." + triggerId + "Trigger");
            if (type != null)
            {
                var triggerObj = Activator.CreateInstance(type) as FSMTrigger;
                triggers.Add(triggerObj);
            }
        }
        /// <summary>
        /// 删除条件
        /// </summary>
        /// <param name="triggerId"></param>
        public void RemoveTrigger(FSMTriggerID triggerId)
        {
            if (map.ContainsKey(triggerId))
            {
                map.Remove(triggerId);
                RemoveTriggerObject(triggerId);
            }
        }
        /// <summary>
        /// 删除条件对象
        /// </summary>
        /// <param name="triggerId">条件id</param>
        private void RemoveTriggerObject(FSMTriggerID triggerId)
        {
            triggers.RemoveAll(t => t.triggerId == triggerId);
        }
        /// <summary>
        /// 根据条件查找状态
        /// </summary>
        /// <param name="triggerId">条件id</param>
        /// <returns></returns>
        public FSMStateID GetOutputState(FSMTriggerID triggerId)
        {
            if (map.ContainsKey(triggerId))
            {
                return map[triggerId];
            }
            return FSMStateID.None;
        }
        /// <summary>
        /// 条件检测
        /// </summary>
        /// <param name="fsm">状态机</param>
        virtual public void Reason(FSMBase fsm)
        {
            for (int i = 0; i < triggers.Count; i++)
            {
                if (triggers[i].HandleTrigger(fsm))
                {
                    //下面这句应该没用
                    FSMStateID stateID = map[triggers[i].triggerId];
                    fsm.ChangeActiveState(triggers[i].triggerId);
                    return;
                }
            }
        }
        /// <summary>
        /// 离开状态
        /// </summary>
        /// <param name="fsm"></param>
        public virtual void ExitState(FSMBase fsm)
        { }
        /// <summary>
        /// 状态中
        /// </summary>
        /// <param name="fsm"></param>
        public virtual void ActionState(FSMBase fsm)
        { }
        /// <summary>
        /// 进入状态
        /// </summary>
        /// <param name="fsm"></param>
        public virtual void EnterState(FSMBase fsm)
        { }
    }
}
