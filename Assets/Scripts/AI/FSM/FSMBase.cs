using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using FF.Skill;
using FF;
using UnityEngine.AI;
using AI.Perception;
using Common;

namespace AI.FSM
{
    /// <summary>
    /// 状态机：有限状态机最核心的一个类
    /// </summary>
    [RequireComponent(typeof(CharacterState), typeof(NavMeshAgent))]
    public class FSMBase : MonoBehaviour
    {
        //所有的状态对象=状态集合=状态库
        /// <summary>
        /// 当前角色状态库
        /// </summary>
        private List<FSMState> states;
        /// <summary>
        /// 当前状态
        /// </summary>
        private FSMState currentState;
        /// <summary>
        /// 当前状态id
        /// </summary>
        public FSMStateID currentStateId;
        /// <summary>
        /// 默认状态
        /// </summary>
        private FSMState defaultState;
        /// <summary>
        /// 默认状态id
        /// </summary>
        public FSMStateID defaultStateId = FSMStateID.Idle;
        /// <summary>
        /// 动画控制
        /// </summary>
        [HideInInspector]
        public Animator anim;
        /// <summary>
        /// 当前角色状态
        /// </summary>
        [HideInInspector]
        public CharacterState chState;
        /// <summary>
        /// 状态文件StreamingAssets目录下 状态转换表
        /// </summary>
        public string aiConfigFile = "AI_01.txt";
        /// <summary>
        /// 移动速度
        /// </summary>
        public float moveSpeed = 5;
        /// <summary>
        /// 寻路组件
        /// </summary>
        private NavMeshAgent navAgent;
        /// <summary>
        /// 查找出的目标
        /// </summary>
        [HideInInspector]
        public Transform targetTF;
        /// <summary>
        /// 关注的目标tag
        /// </summary>
        /// <value></value>
        // public string[] targetTags = {"Player"};
        public EM.Tag[] targetTags = {
            EM.Tag.Player
        };
        /// <summary>
        /// 视野距离
        /// </summary>
        private float sightDistance = 10;
        /// <summary>
        /// 视野角度
        /// </summary>
        public int angleOfView = 360;
        /// <summary>
        /// 根据条件id改变状态
        /// </summary>
        /// <param name="triggerId">条件id</param>
        public void ChangeActiveState(FSMTriggerID triggerId)
        {
            //1 根据当前条件 确定 下一个状态是谁？待机》条件 》死亡
            var nextStateId = currentState.GetOutputState(triggerId);
            //如果是None返回
            if (nextStateId == FSMStateID.None) return;
            //如果不是None，继续判断是不是默认
            FSMState nextState = null;//状态对象
            if (nextStateId == FSMStateID.Default)
                nextState = defaultState;
            else
                nextState = states.Find(s => s.stateId == nextStateId);
            //2 退出当前状态
            currentState.ExitState(this);
            //更新当前状态对象和状态编号
            currentState = nextState;//更新当前状态对象
            currentStateId = currentState.stateId;
            //3 进入下一个状态
            currentState.EnterState(this);
        }

        //实时更新状态:实时检查条件的变化，条件一变-行为就变
        private void Update()
        {
            //判断当前状态条件
            currentState.Reason(this);
            //执行当前状态逻辑
            currentState.ActionState(this);
            //搜索目标
            // SearchTarget();
        }

        private void Awake()
        {
            ConfigFSM();
            _InitComponent();
        }

        //使用AI配置文件 确定 条件 状态的映射关系
        /// <summary>
        /// 配置状态机
        /// </summary>
        private void ConfigFSM()
        {
            states = new List<FSMState>();
            //var map = new AIConfigurationReader(aiConfigFile).map;
            //如:Dictionary<"待机",Dictionary<"HP=0","死亡">>
            Dictionary<string, Dictionary<string, string>> map = AIConfigurationReaderFactory.getMap(aiConfigFile);
            foreach (var state in map)
            {
                //state.Key 状态名称
                //state.Value 映射
                //1 创建状态对象
                var type = Type.GetType("AI.FSM." + state.Key + "State");
                FSMState stateObj = Activator.CreateInstance(type) as FSMState;
                //2 添加条件映射
                foreach (var dic in state.Value)
                {
                    //dic.Key 条件id
                    //dic.value 状态id
                    //string >对应 枚举
                    FSMTriggerID triggerId = (FSMTriggerID)(Enum.Parse(typeof(FSMTriggerID), dic.Key));
                    FSMStateID stateId = (FSMStateID)(Enum.Parse(typeof(FSMStateID), dic.Value));

                    stateObj.AddMap(triggerId, stateId);
                }
                //3 放入状态集合=状态库
                states.Add(stateObj);
            }
        }
        /// <summary>
        /// 指定默认状态
        /// </summary>
        private void InitDefaultState()
        {
            //根据属性窗口指定的 默认状态编号 为其它三个字段赋值=初始化
            defaultState = states.Find(s => s.stateId == defaultStateId);
            currentState = defaultState;
            currentState.EnterState(this);
            currentStateId = defaultStateId;
        }
        private void OnEnable()
        {
            InitDefaultState();//执行的时机 执行频率
            //临时调用 
            //InvokeRepeating("ResetTarget", 0, 0.2f);//3.0-1
        }

        public void Start()
        {
            if (sightSensor)
            {
                //如果有角色属性 使用熟悉的值  没有则用设置的值
                sightSensor.sightDistance = chState.stateData?.sightDistance ?? sightSensor.sightDistance;
                sightSensor.sightAngle = chState.stateData?.sightAngle ?? sightSensor.sightAngle;
                sightSensor.enabledAngle = chState.stateData?.sightAngle > 0?true:false;

                sightSensor.OnPerception += sightSensor_OnPerception;
                sightSensor.OnNonPerception += sightSensor_OnNonPerception;
            }

            sightDistance = chState.stateData.sightDistance;
        }
        /// <summary>
        /// 初始化组件
        /// </summary>
        private void _InitComponent()
        {
            chState = GetComponent<CharacterState>();
            anim = GetComponentInChildren<Animator>();
            navAgent = GetComponent<NavMeshAgent>();
            skillSystem = GetComponent<CharacterSkillSystem>();
            sightSensor = GetComponent<SightSensor>();
        }
        /// <summary>
        /// 寻路:朝目标移动
        /// </summary>
        /// <param name="position">目标当前的位置</param>
        /// <param name="speed">跑的速度</param>
        /// <param name="stopDistance">停止距离</param>
        public void MoveToTarget(Vector3 position, float speed, float stopDistance)
        {
            navAgent.speed = speed;
            navAgent.stoppingDistance = stopDistance;
            navAgent.SetDestination(position);
        }
        /// <summary>
        /// 寻路：停止移动
        /// </summary>
        public void StopMove()
        {
            navAgent.enabled = false;
            navAgent.enabled = true;
        }
        private void OnDisable()
        {
            InitDefaultState();//执行的时机 执行频率
            //放开 需求决定 HP=0》idle
            //删除              HP=0》dead
            //临时调用
            //CancelInvoke("ResetTarget");
            //因为使用了感应器，所以死亡状态 感应器失效=移除
            if (currentStateId != FSMStateID.Dead)
            {
                currentState.ExitState(this);
                currentState = states.Find(p => p.stateId == FSMStateID.Idle);
                currentStateId = currentState.stateId;
                //PlayAnimation(animParams.Idle);
            }
            else
            {
                AbstractSensor[] sensors = GetComponents<AbstractSensor>();
                foreach (AbstractSensor sensor in sensors)
                {
                    sensor.enabled = false;
                }
            }
        }
        /// <summary>
        /// 技能系统
        /// </summary>
        private CharacterSkillSystem skillSystem;
        /// <summary>
        /// 自动使用技能
        /// </summary>
        public void AutoUseSkill()
        {
            skillSystem.UseRandomSkill();
        }

        #region 定义寻路需要字段和方法
        [Header("patrol params")]
        [Tooltip("路点位置")]
        /// <summary>
        /// 路点位置
        /// </summary>
        public Transform[] wayPoints;
        [Tooltip("巡逻到达的距离")]
        /// <summary>
        /// 巡逻到达的距离
        /// </summary>
        public float patrolArrivalDistance = 1;
        [Tooltip("巡逻移动速度")]
        /// <summary>
        /// 巡逻速度
        /// </summary>
        public float patrolSpeed = 3;
        /// <summary>
        /// 是否完成巡逻
        /// </summary>
        [HideInInspector]
        public bool IsPatrolComplete = false;
        [Tooltip("巡逻方式")]
        /// <summary>
        /// 巡逻方式
        /// </summary>
        public PatrolMode patrolMode = PatrolMode.Once;
        #endregion


        #region 6.0 引入智能感应器
        private SightSensor sightSensor;//1 引入 2注册事件
        public void sightSensor_OnPerception(List<AbstractTrigger> triggerList)
        {
            targetTF = null;
            List<AbstractTrigger> targetTFList = triggerList.FindAll(t => Array.IndexOf(targetTags, t.tag.ToEnum<EM.Tag>()) >= 0);
            if (targetTFList.Count > 0)
            {
                targetTFList = targetTFList.FindAll(t => t.GetComponent<CharacterState>().stateData.HP > 0);
                if (targetTFList.Count > 0)
                {
                    //    targetTFList.ToArray().Min();
                    targetTF = ArrayHelper.Min(targetTFList.ToArray(),
                        t => Vector3.Distance(t.transform.position,
                            transform.position)).transform;
                }
            }
        }
        public void sightSensor_OnNonPerception()
        {
            targetTF = null;
        }

        #endregion
    }
}
