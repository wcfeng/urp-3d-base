using System;
using System.Collections.Generic;
using System.IO;
using Common;
using UnityEngine;
//给数据类型起别名
using myint=System.Int32; 

namespace AI.FSM
{
    /// <summary>
    /// 配置读取器
    /// </summary>
    public class AIConfigurationReader
    {
        /// <summary>
        /// 条件状态字典 例:Dictionary("待机",Dictionary("HP=0","死亡"))
        /// </summary>
        public Dictionary<string, Dictionary<string, string>> map;
        /// <summary>
        /// 读取配置文件到字典
        /// </summary>
        /// <param name="filename">配置文件名</param>
        public AIConfigurationReader(string filename)
        {
            map = new Dictionary<string, Dictionary<string, string>>();
            string configFile = ConfigurationReader.GetConfigFile(filename);
            ConfigurationReader.Reader(configFile, BuildMap);
        }

        private string mainKey;
        /// <summary>
        /// 读取每行
        /// </summary>
        /// <param name="line"></param>
        private void BuildMap(string line)
        {
            line = line.Trim();//去除空白行
            if (!string.IsNullOrEmpty(line))
            {   //取主键 如 [Idle] 》》Idle
                if (line.StartsWith("["))
                {
                    line = line.Substring(1, line.IndexOf("]") - 1);
                    map.Add(line, new Dictionary<string, string>());
                    mainKey = line;
                }//取子键以及值
                else
                {
                    var configValue = line.Split('>');
                    map[mainKey].Add(configValue[0].Trim(), configValue[1].Trim());
                }
            }
        }
    }
}


