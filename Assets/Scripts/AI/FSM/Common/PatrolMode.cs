﻿
namespace AI.FSM
{
    /// <summary>
    /// 巡逻模式
    /// </summary>
    public enum PatrolMode
    {
        /// <summary>
        /// 单次 123 巡逻完成
        /// </summary>
        Once,
        /// <summary>
        /// 循环 123123 从第一个路点出发完成，再从第一个开始
        /// </summary>
        Loop,
        /// <summary>
        /// 往返=来回 1232123
        /// </summary>
        PingPong,
    }
}
