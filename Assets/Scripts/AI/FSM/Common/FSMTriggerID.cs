using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.FSM
{
    /// <summary>
    /// 状态转换条件
    /// </summary>
    public enum FSMTriggerID
    {
        /// <summary>
        /// 生命为0
        /// </summary>	
        NoHealth,
        /// <summary>
        /// 发现目标
        /// </summary>	
        FindTarget,
        /// <summary>
        /// 目标进入攻击范围
        /// </summary>
        ReachTarget,
        /// <summary>
        /// 丢失目标
        /// </summary>
        LoseTarget,
        /// <summary>
        /// 完成巡逻
        /// </summary>
        CompletePatrol,
        /// <summary>
        /// 打死目标
        /// </summary>
        KillTarget,
        /// <summary>
        /// 目标不在攻击范围
        /// </summary>
        WithOutAttackRange,
    }
}
