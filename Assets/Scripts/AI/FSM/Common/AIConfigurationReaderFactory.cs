using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.FSM
{
    /// <summary>
    /// 配置读取器工厂
    /// </summary>
    public class AIConfigurationReaderFactory
    {
        /// <summary>
        /// 缓存配置
        /// </summary>
        private static Dictionary<string, AIConfigurationReader> cache;
        static AIConfigurationReaderFactory()
        {
            cache = new Dictionary<string, AIConfigurationReader>();
        }
        /// <summary>
        /// 获取缓存配置
        /// </summary>
        /// <param name="filename">文件名称</param>
        /// <returns></returns>
        public static Dictionary<string,Dictionary<string,string>> getMap(string filename)
        {
            if (!cache.ContainsKey(filename))
            {
                cache.Add(filename,new AIConfigurationReader(filename));
            }
            return cache[filename].map;
        }
    }
}
