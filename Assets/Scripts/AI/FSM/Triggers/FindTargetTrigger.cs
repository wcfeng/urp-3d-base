using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.FSM
{

    /// <summary>
    /// 发现目标
    /// </summary>
    public class FindTargetTrigger:FSMTrigger
    {
       public override void Init()
       {
           triggerId = FSMTriggerID.FindTarget;
       }
       public override bool HandleTrigger(FSMBase fsm)
       {
            return fsm.targetTF != null;
       }

    }
}
