using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace AI.FSM
{
    /// <summary>
    /// 死亡条件
    /// </summary>
    public class NoHealthTrigger:FSMTrigger
    {
        public override bool HandleTrigger(FSMBase fsm)
        {
            return fsm.chState.stateData.HP <= 0;
        }

        public override void Init()
        {
            triggerId = FSMTriggerID.NoHealth;
        }
    }
}
