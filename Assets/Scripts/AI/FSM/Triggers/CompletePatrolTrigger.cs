using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.FSM
{
    /// <summary>
    /// 完成巡逻条件
    /// </summary>
    public class CompletePatrolTrigger:FSMTrigger
    {
        public override void Init()
        {
            triggerId = FSMTriggerID.CompletePatrol;
        }
        public override bool HandleTrigger(FSMBase fsm)
        {
            return fsm.IsPatrolComplete;//true
        }
    }
}
