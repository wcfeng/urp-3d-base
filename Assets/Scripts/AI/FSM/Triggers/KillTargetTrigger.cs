using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FF;

namespace AI.FSM
{
    /// <summary>
    /// 打死目标
    /// </summary>
    public class KillTargetTrigger:FSMTrigger
    {
        public override void Init()
        {
            triggerId = FSMTriggerID.KillTarget;
        }
        public override bool HandleTrigger(FSMBase fsm)
        {
            //没找到目标
            if (fsm.targetTF == null) return false;
            //目标是否死亡
            bool b = fsm.targetTF.GetComponent<CharacterState>().stateData.HP <= 0;
            //死亡了 目标设置空
            if (b) fsm.targetTF = null;
            return b;
        }
    }
}
