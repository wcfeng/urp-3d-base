using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace AI.FSM
{
    /// <summary>
    /// 目标进入攻击范围
    /// </summary>
    class ReachTargetTrigger:FSMTrigger
    {
        public override void Init()
        {
            triggerId = FSMTriggerID.ReachTarget;
        }
        public override bool HandleTrigger(FSMBase fsm)
        {
            if(fsm.targetTF!=null)
            {
                //目标与自身距离<攻击距离
                bool b = Vector3.Distance(fsm.transform.position,fsm.targetTF.position) < fsm.chState.stateData.attackDistance;
                return b;
            }
            return false;
        }
    }
}
