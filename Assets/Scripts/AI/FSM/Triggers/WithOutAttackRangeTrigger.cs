using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace AI.FSM
{
    /// <summary>
    /// 目标离开攻击范围：视野距离内 攻击距离外
    /// </summary>
    public class WithOutAttackRangeTrigger:FSMTrigger
    {
        public override void Init()
        {
            triggerId = FSMTriggerID.WithOutAttackRange;
        }
        public override bool HandleTrigger(FSMBase fsm)
        {
            //1有目标 在视觉范围                    false 
            //2有目标 不在攻击范围但在视觉范围 true 
            //3没有目标                                 false
            if(fsm.GetComponent<FF.CharacterState>().isStop) return false;
            bool b = false;
            if (fsm.targetTF != null)
            {
                //目标与自身的距离
                var distance = Vector3.Distance(fsm.targetTF.position,fsm.transform.position);
                // 视野距离 < > 攻击距离
                b = distance > fsm.chState.stateData.attackDistance && distance < fsm.chState.stateData.sightDistance;
            }
            return b;//攻击目标不存在
        }
    }
}
