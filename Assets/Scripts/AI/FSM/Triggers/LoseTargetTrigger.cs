using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace AI.FSM
{
    /// <summary>
    /// 丢失目标
    /// </summary>
    public  class LoseTargetTrigger:FSMTrigger
    {
        public override void Init()
         {
            triggerId = FSMTriggerID.LoseTarget;
         }
        public override bool HandleTrigger(FSMBase fsm)
        {
            return fsm.targetTF == null;
        }
    }
}
