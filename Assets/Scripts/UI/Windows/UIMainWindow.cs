﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using UnityEngine.EventSystems;
using Common;
namespace UI
{
    public class UIMainWindow : UIWindow
    {
        private PlayableDirector director;

        private void Start()
        {
            //显示主窗口
            UIManager.Instance.GetWindow<UIMainWindow>().IsShow(true);

            //绑定事件
            GetUIEventListener("New Game").PointerClick += PlayTimeline;
            //GetUIEventListener("New Game").PointerClick += NewGameTest;
            GetUIEventListener("Continue").PointerClick += ContinueGame;
            GetUIEventListener("Set Button").PointerClick += SetClick;
            GetUIEventListener("Quit").PointerClick += QuitGame;

            //查找timeline
            director = DirectorManager.Instance.Find(Const.Director.GameStart);
            //注册播放完成事件
            director.stopped += NewGame;
        }

        private void PlayTimeline(PointerEventData eventData)
        {
            director.Play();
        }

        private void NewGame(PlayableDirector director)
        {
            PlayerPrefs.DeleteAll();
            SceneController.Instance.LoadScene(Const.Scene.Level1);
        }

        private void NewGameTest(PointerEventData eventData)
        {
            PlayerPrefs.DeleteAll();
            SceneController.Instance.LoadScene(Const.Scene.Level1);
        }

        private void ContinueGame(PointerEventData eventData)
        {
            SceneController.Instance.LoadToSaveScene();
        }

        private void SetClick(PointerEventData eventData)
        {
            UIManager.Instance.GetWindow<UISetWindow>().IsShow(true);
            Time.timeScale = 0;
        }

        private void QuitGame(PointerEventData eventData)
        {
            Application.Quit();
            Debug.Log("退出游戏");
        }

    }
}