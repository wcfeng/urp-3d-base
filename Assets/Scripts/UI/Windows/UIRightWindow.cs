using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    /// <summary>
    /// 游戏右窗口
    /// </summary>
    public class UIRightWindow : UIWindow
    {
        private TPSPlayerCharacter tPSPlayerCharacter;
        private void Start()
        {
            //transform.FindChildByName("ButtonGameStart").GetComponent<Button>().onClick.AddListener(OnGameStartButtonClick);
            //transform.FindChildByName("ButtonGameStart").GetComponent<UIEventListener>().PointerClick += OnPointerClick;
            //绑定此窗口下所有ui事件
            // GetUIEventListener("ButtonGameStart").PointerClick += OnPointerClick;

            //显示主窗口
            UIManager.Instance.GetWindow<UIRightWindow>().IsShow(true);

            GetUIEventListener("Handgun").PointerClick += OnSelectItem;
            GetUIEventListener("Grenade").PointerClick += OnSelectItem;
            GetUIEventListener("Trap").PointerClick += OnSelectItem;
            GetUIEventListener("Knife").PointerClick += OnSelectItem;
            print(GameObject.FindWithTag("Player"));
            tPSPlayerCharacter = GameObject.FindWithTag("Player").GetComponent<TPSPlayerCharacter>();
        }

        /// <summary>
        /// 选择武器
        /// </summary>
        /// <param name="eventData">点击的目标数据</param>
        private void OnSelectItem(PointerEventData eventData)
        {
            if (tPSPlayerCharacter.dead) return;
            string item = eventData.pointerPress.name;
            tPSPlayerCharacter.curItem = item;
            // 切换武器时可进行相关操作，陷阱的代码后面会用到
            switch (item)
            {
                case "Handgun": // 手枪
                    break;
                case "Grenade": // 投掷物
                    break;
                case "Trap": // 陷阱
                    {
                        //player.BeginTrap();
                    }
                    break;
                case "Knife":
                    break;
            }
        }

        private void OnPointerClick(PointerEventData eventData)
        {
            print(eventData.pointerPress);
            print(eventData.clickCount);
            //GameController.Instance.GameStart();
        }

        private void OnGameStartButtonClick()
        {
            // GameController.Instance.GameStart();
        }
    }
}
