﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using UnityEngine.EventSystems;
using Common;
using Config;
using System;

namespace UI
{
    public class UISetWindow : UIWindow
    {
        private Transform setPanel;

        private void Start()
        {
            setPanel = transform.FindChildByName("Set Panel");
            GetUIEventListener("Return Button").PointerClick += ReturnClick;
            GetUIEventListener("Audio Slider").GetComponent<Slider>().value = Set.Instance.soundValue;
            GetUIEventListener("Audio Slider").GetComponent<Slider>().onValueChanged.AddListener(onValueChanged);
        }

        private void onValueChanged(float arg0)
        {
            //把值保存起来
            //Set.Instance.soundValue = arg0;
            //SaveManager.Instance.Save("Set", Set.Instance);
            //设置音量
            SoundManager.Instance.masterMixer.SetFloat(Const.Sound.MixerVolume, arg0);
        }

        public void ReturnClick(PointerEventData eventData)
        {
            UIManager.Instance.GetWindow<UISetWindow>().IsShow(false);
            Time.timeScale = 1;
        }

        //private void AudioSliderClick()
        //{
        //    float value = eventData.pointerPress.gameObject.GetComponent<Slider>().value;
        //    SoundManager.Instance.masterMixer.SetFloat(Const.Sound.MixerVolume, value);
        //}

    }
}