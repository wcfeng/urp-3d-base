using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

namespace UI
{
    /// <summary>
    /// 游戏主窗口
    /// </summary>
    public class UIMainWindowSocketTest : UIWindow
    {
        public float progress;
        private WebRequest webRequest;
        private void Start()
        {
            //transform.FindChildByName("ButtonGameStart").GetComponent<Button>().onClick.AddListener(OnGameStartButtonClick);
            //transform.FindChildByName("ButtonGameStart").GetComponent<UIEventListener>().PointerClick += OnPointerClick;
            //绑定此窗口下所有ui事件
            // GetUIEventListener("ButtonGameStart").PointerClick += OnPointerClick;

            //显示主窗口
            UIManager.Instance.GetWindow<UIMainWindowTest>().IsShow(true);

            GetUIEventListener("submit").PointerClick += OnSubmit;
            GetUIEventListener("TcpConnect").PointerClick += OnConnect;
            GetUIEventListener("TcpSend").PointerClick += OnSend;
            webRequest = WebRequest.Instance;
        }

        private void OnSend(PointerEventData eventData)
        {
            TestSocket.Instance.Send();
        }

        private void OnConnect(PointerEventData eventData)
        {
            TestSocket.Instance.Connect();
        }

        /// <summary>
        /// 提交数据
        /// </summary>
        /// <param name="eventData"></param>
        private void OnSubmit(PointerEventData eventData)
        {
            string item = eventData.pointerPress.name;
            Transform usernameTF = transform.FindChildByName("username");
            Transform passwordTF = transform.FindChildByName("password");
            string username = usernameTF.GetComponent<TMP_InputField>().text;
            string password = passwordTF.GetComponent<TMP_InputField>().text;

            Data.User_SO userObj = ScriptableObject.CreateInstance<Data.User_SO>();
            userObj.username = username;
            userObj.password = password;

            //progress = webRequest.request?.downloadProgress??0;
            StartCoroutine(webRequest.PostRequest(userObj,getResponse));

        }

        /// <summary>
        /// 响应
        /// </summary>
        /// <param name="response"></param>
        private void getResponse(string response)
        {
            print(response);
        }
    }
}
