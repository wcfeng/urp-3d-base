﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using UnityEngine.EventSystems;
using Common;
using TMPro;
using System;

namespace UI
{
    public class UIPlayerWindow : UIWindow
    {
        private Image HPSlider;
        private Image expSlider;
        private Transform setPanel;
        private TextMeshProUGUI LevelText;

        private void Start()
        {
            //显示主窗口
            UIManager.Instance.GetWindow<UIPlayerWindow>().IsShow(true);
            //获取组件
            HPSlider = transform.FindChildByName("HP Slider").GetComponent<Image>();
            expSlider = transform.FindChildByName("Exp Slider").GetComponent<Image>();
            setPanel = transform.FindChildByName("Set Panel");
            //绑定事件
            GetUIEventListener("Set Button").PointerClick += SetClick;
            //设置按钮透明度
            _EasyTouchSetTransparent();

        }



        private void SetClick(PointerEventData eventData)
        {
            UIManager.Instance.GetWindow<UISetWindow>().IsShow(true);
            Time.timeScale = 0;
        }

        private void Update()
        {
            UpdateHP();
            UpdateExp();
        }

        private void UpdateHP()
        {
            float sliderPercent = (float)GameController.Instance.player.stateData.HP / GameController.Instance.player.stateData.maxHP;
            HPSlider.fillAmount = sliderPercent;
        }

        private void UpdateExp()
        {
            float sliderPercent = (float)GameController.Instance.player.stateData.currentExp / GameController.Instance.player.stateData.levelUpExp;
            expSlider.fillAmount = sliderPercent;
        }

        private void UpdateLevel()
        {
            LevelText.text = "Level " + GameController.Instance.player.stateData.currentLevel.ToString("00");
        }

        /// <summary>
        /// 如果在pc上 easytouch设置为透明
        /// </summary>
        private void _EasyTouchSetTransparent()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            Transform MainJoystick = transform.FindChildByName("MainJoystick");
            Transform SkillButton = transform.FindChildByName("SkillButton");
            if (MainJoystick)
                MainJoystick.GetComponent<CanvasGroup>().alpha = 0.2f;
            if (SkillButton)
                SkillButton.GetComponent<CanvasGroup>().alpha = 0.2f;
#endif
        }

    }
}