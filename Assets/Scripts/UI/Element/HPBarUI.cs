﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FF;
using Common;
/// <summary>
/// 世界空间画布下的血条组件
/// </summary>
[RequireComponent(typeof(CharacterState))]
public class HPBarUI : MonoBehaviour
{
    /// <summary>
    /// 血量显示组件UI
    /// </summary>
    public GameObject HPUIPrefab;
    /// <summary>
    /// 血条位置 不填默认为子物体 名称为（HPBarPoint）
    /// </summary>
    public Transform HPBarPoint;
    /// <summary>
    /// 是否总是显示
    /// </summary>
    public bool alwayVisible;
    /// <summary>
    /// 可视化时间
    /// </summary>
    public float visibleTime = 3;
    /// <summary>
    /// 剩余显示时间
    /// </summary>
    private float timeleft;
    /// <summary>
    /// 滑动条图片
    /// </summary>
    private Image HPSlider;
    /// <summary>
    /// 血条组件
    /// </summary>
    private Transform UIbar;
    /// <summary>
    /// 主相机
    /// </summary>
    private Transform MainCamera;
    /// <summary>
    /// 角色信息
    /// </summary>
    private CharacterState charaterState;

    private void Awake()
    {
        charaterState = GetComponent<CharacterState>();
        charaterState.UpdateHPOnDamage += UpdateHPBar;

        if (!HPBarPoint) HPBarPoint = transform.FindChildByName("HPBarPoint");
    }

    private void OnEnable()
    {
        MainCamera = Camera.main.transform;
        //找出所有世界坐标canvas
        foreach (Canvas canvas in FindObjectsOfType<Canvas>())
        {
            if (canvas.renderMode == RenderMode.WorldSpace)
            {
                //在canvas下创建血量ui组件
                UIbar = Instantiate(HPUIPrefab, canvas.transform).transform;
                //获取子物体（当前血量）
                HPSlider = UIbar.GetChild(0).GetComponent<Image>();
                //设置可见
                UIbar.gameObject.SetActive(alwayVisible);
            }
        }
    }
    /// <summary>
    /// 修改血量ui
    /// </summary>
    /// <param name="HP">当前血量</param>
    /// <param name="MaxHP">全部血量</param>
    private void UpdateHPBar(int HP, int MaxHP)
    {
        if (HP <= 0)
        {
            Destroy(UIbar.gameObject);
        }
        UIbar.gameObject.SetActive(true);
        //剩余显示时间赋值
        timeleft = visibleTime;
        //获取百分比血量
        float sliderPercent = (float)HP / MaxHP;
        //赋值到血量UI上
        HPSlider.fillAmount = sliderPercent;
    }

    private void LateUpdate()
    {
        if (UIbar != null)
        {
            //位置跟随
            UIbar.position = HPBarPoint.position;
            //方向跟随
            UIbar.forward = -MainCamera.forward;
            //设置显隐
            if (timeleft <= 0 && !alwayVisible)
            {
                UIbar.gameObject.SetActive(false);
            }
            else
            {
                timeleft -= Time.deltaTime;
            }
        }
    }
}
