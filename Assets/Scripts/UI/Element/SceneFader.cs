﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
/// <summary>
/// 淡入淡出控制
/// </summary>
[RequireComponent(typeof(DontDestroyOnLoad))]
public class SceneFader : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    /// <summary>
    /// 淡入时间
    /// </summary>
    public float fadeInDuration;
    /// <summary>
    /// 淡出时间
    /// </summary>
    public float fadeOutDuration;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    /// <summary>
    /// 淡入淡出
    /// </summary>
    /// <returns></returns>
    public IEnumerator FadeOutIn()
    {
        yield return FadeOut(fadeOutDuration);
        yield return FadeIn(fadeInDuration);
    }
    /// <summary>
    /// 淡出
    /// </summary>
    /// <param name="time">时间</param>
    /// <returns></returns>
    public IEnumerator FadeOut(float time)
    {
        while (canvasGroup.alpha < 1)
        {
            canvasGroup.alpha += Time.deltaTime / time;
            yield return null;
        }
    }
    /// <summary>
    /// 淡入
    /// </summary>
    /// <param name="time">时间</param>
    /// <returns></returns>
    public IEnumerator FadeIn(float time)
    {
        while (canvasGroup.alpha != 0)
        {
            canvasGroup.alpha -= Time.deltaTime / time;
            yield return null;
        }
        Destroy(gameObject);
    }
}
