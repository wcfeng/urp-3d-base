﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//此文件已弃用
public class PlayerHPUI : MonoBehaviour
{
    private TextMeshProUGUI LevelText;
    private Image HPSlider;
    private Image expSlider;

    private void Awake()
    {
        LevelText = transform.GetChild(2).GetComponent<TextMeshProUGUI>();
        HPSlider = transform.GetChild(0).GetChild(0).GetComponent<Image>();
        expSlider = transform.GetChild(1).GetChild(0).GetComponent<Image>();
    }

    private void Update()
    {
        LevelText.text = "Level " + GameController.Instance.player.stateData.currentLevel.ToString("00");
        UpdateHP();
        UpdateExp();
    }

    private void UpdateHP()
    {
        float sliderPercent = (float)GameController.Instance.player.stateData.HP / GameController.Instance.player.stateData.maxHP;
        HPSlider.fillAmount = sliderPercent;
    }

    private void UpdateExp()
    {
        float sliderPercent = (float)GameController.Instance.player.stateData.currentExp / GameController.Instance.player.stateData.levelUpExp;
        expSlider.fillAmount = sliderPercent;
    }
}
