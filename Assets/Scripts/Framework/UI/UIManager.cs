using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace UI
{
    /// <summary>
    /// UI管理器
    /// </summary>
    public class UIManager : MonoSingleton<UIManager>
    {
        /// <summary>
        /// 窗口字典 key 窗口名称 value 窗口引用
        /// </summary>
        private Dictionary<string, UIWindow> uiWindowDic;
        /// <summary>
        /// 初始化窗口 隐藏所有窗口 添加所有窗口到字典
        /// </summary>
        public override void Init()
        {
            base.Init();
            uiWindowDic = new Dictionary<string, UIWindow>();
            UIWindow[] uiWindowArr = FindObjectsOfType<UIWindow>();
            for (int i = 0; i < uiWindowArr.Length; i++)
            {
                //隐藏窗口
                uiWindowArr[i].IsShow(false);
                //记录窗口
                _AddWindow(uiWindowArr[i]);
            }
        }
        /// <summary>
        /// 添加窗口
        /// </summary>
        /// <param name="window">窗口</param>
        private void _AddWindow(UIWindow window)
        {
            //获取类名和实例添加到字典
            uiWindowDic.Add(window.GetType().Name, window);
        }
        /// <summary>
        /// 获取窗口
        /// </summary>
        /// <typeparam name="T">窗口</typeparam>
        public T GetWindow<T>() where T:class
        {
            string key = typeof(T).Name;
            if (!uiWindowDic.ContainsKey(key)) return null;
            return uiWindowDic[key] as T;
        }
    }
}
