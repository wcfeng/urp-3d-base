using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public delegate void PointerEventHandler(PointerEventData eventData);
    /// <summary>
    /// UI事件监听器
    /// </summary>
    public class UIEventListener : MonoBehaviour, IPointerClickHandler,IPointerDownHandler,IPointerUpHandler
    {
        //声明事件
        /// <summary>
        /// 点击事件
        /// </summary>
        public event PointerEventHandler PointerClick;
        /// <summary>
        /// 按下事件
        /// </summary>
        public event PointerEventHandler PointerDown;
        /// <summary>
        /// 抬起事件
        /// </summary>
        public event PointerEventHandler PointerUp;
        /// <summary>
        /// 获取变换事件监听器 没有则添加
        /// </summary>
        /// <param name="tf">变换组件</param>
        /// <returns></returns>
        public static UIEventListener GetListener(Transform tf)
        {
            UIEventListener uiEvent = tf.GetComponent<UIEventListener>();
            if (uiEvent == null) uiEvent = tf.gameObject.AddComponent<UIEventListener>();
            return uiEvent;

        }
        public void OnPointerClick(PointerEventData eventData)
        {
            if (PointerClick != null) PointerClick(eventData);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (PointerDown != null) PointerDown(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (PointerUp != null) PointerUp(eventData);
        }
    }
}
