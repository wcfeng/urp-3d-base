using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace UI
{
    /// <summary>
    /// UI窗口基类 提供窗口基本方法
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    public class UIWindow : MonoBehaviour
    {
        /// <summary>
        /// 画布组
        /// </summary>
        private CanvasGroup canvasGroup;
        /// <summary>
        /// ui事件字典 ui名称 -> ui事件监听
        /// </summary>
        private Dictionary<string, UIEventListener> uiEventDic;
        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            uiEventDic = new Dictionary<string, UIEventListener>();
        }
        /// <summary>
        /// 设置可见性
        /// </summary>
        /// <param name="state">是否显示</param>
        /// <param name="delay">延迟时间</param>
        public void IsShow(bool state, float delay = 0)
        {
            //画布组设置
            StartCoroutine(_IsShowDelay(state, delay));
        }
        /// <summary>
        /// 设置画布显隐
        /// </summary>
        /// <param name="state"></param>
        /// <param name="delay"></param>
        /// <returns></returns>
        private IEnumerator _IsShowDelay(bool state, float delay)
        {
            yield return new WaitForSeconds(delay);
            canvasGroup.alpha = state ? 1 : 0;
            canvasGroup.interactable = state;
            canvasGroup.blocksRaycasts = state;
        }

        /// <summary>
        /// 获取UI监听器
        /// </summary>
        /// <param name="name">ui名称</param>
        /// <returns></returns>
        public UIEventListener GetUIEventListener(string name)
        {
            if (!uiEventDic.ContainsKey(name))
            {
                Transform tf = transform.FindChildByName(name);
                UIEventListener uiEvent = UIEventListener.GetListener(tf);
                uiEventDic.Add(name, uiEvent);
            }
            return uiEventDic[name];
        }
    }
}
