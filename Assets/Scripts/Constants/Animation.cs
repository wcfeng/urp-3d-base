﻿namespace Const
{
    /// <summary>
    /// 动画参数
    /// </summary>
    public class Animation
    {
        /// <summary>
        /// 待机,走,跑(0,0.5,1)
        /// </summary>
        public const string Speed = "Speed";
        /// <summary>
        /// 攻击
        /// </summary>
        public const string Attack = "Attack";
        public const string Attack1 = "Attack1";

        /// <summary>
        /// 防御
        /// </summary>
        public const string Defense = "Defense";
        /// <summary>
        /// 移动
        /// </summary>
        public const string Move = "Move";
        /// <summary>
        /// 走
        /// </summary>
        public const string Walk = "Walk";
        /// <summary>
        /// 奔跑
        /// </summary>
        public const string Run = "Run";
        /// <summary>
        /// 跳跃
        /// </summary>
        public const string Jump = "Jump";
        /// <summary>
        /// 是否接触地面
        /// </summary>
        public const string Grounded = "Grounded";
        /// <summary>
        /// 是否自由落体
        /// </summary>
        public const string FreeFall = "FreeFall";
        /// <summary>
        /// 受击
        /// </summary>
        public const string GetHit = "GetHit";
        /// <summary>
        /// 死亡
        /// </summary>
        public const string Death = "Death";
        /// <summary>
        /// 眩晕
        /// </summary>
        public const string Dizzy = "Dizzy";
    }
}