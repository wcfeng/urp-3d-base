﻿namespace Const
{
    /// <summary>
    /// 玩家预设
    /// </summary>
    public class Player
    {
        /// <summary>
        /// 玩家相机关注点
        /// </summary>
        public const string CameraRoot = "PlayerCameraRoot";
    }
}