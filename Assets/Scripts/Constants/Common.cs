﻿namespace Const
{
    /// <summary>
    /// 公共常量
    /// </summary>
    public class Common
    {
        /// <summary>
        /// 预制件映射文件名
        /// </summary>
        public const string PrefabMapFile = "PrefabMap.txt";
        /// <summary>
        /// 渐变时间
        /// </summary>
        public const float FadeTime = 0.5f;
    }
}