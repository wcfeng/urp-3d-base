﻿namespace Const
{
    /// <summary>
    /// 场景
    /// </summary>
    public class Scene
    {
        /// <summary>
        /// 主页面
        /// </summary>
        public const string Main = "Main";
        /// <summary>
        /// 关卡1
        /// </summary>
        public const string Level1 = "Level1";
        /// <summary>
        /// 关卡2
        /// </summary>
        public const string Level2 = "Level2";
    }
}