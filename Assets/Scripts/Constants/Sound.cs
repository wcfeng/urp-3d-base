﻿namespace Const
{
    /// <summary>
    /// 声音
    /// </summary>
    public class Sound
    {
        /// <summary>
        /// 声音管道输出的变量名称
        /// </summary>
        public const string MixerVolume = "MixerVolume";
    }
}