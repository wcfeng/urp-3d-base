using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSBullet : MonoBehaviour
{
    public static float duration = 0.5f;
    public Vector3 dir;
    Rigidbody rigid;
    float speed = 10;
    float lifeTime = 0.5f;
    string targetTag;
    // 创建子弹时需要调用Init 函数
    public void Init(string tag, Vector3 _dir, float time = 0.5f, float _speed = 120)
    {
        targetTag = tag;
        dir = _dir;
        lifeTime = time;
        speed = _speed;
    }
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        StartCoroutine(Timeup());
        transform.LookAt(transform.position + dir);
        rigid.velocity = speed * dir.normalized;
    }
    IEnumerator Timeup()
    {
        yield return new WaitForSeconds(lifeTime);
        if (gameObject)
        {
            Destroy(gameObject);
        }
    }
    // 子弹碰到物体时的处理
    private void OnTriggerEnter(Collider other)
    {
        var go = other.gameObject;
        if (go.tag == targetTag) // 忽略开枪打中自己的情况
        {
            if (targetTag == "Enemy")
            {
                TPSEnemyCharacter enemy = go.GetComponent<TPSEnemyCharacter>();
                enemy.BeHit(1);
            }
            else if (targetTag == "Player")
            {
                TPSPlayerCharacter player = go.GetComponent<TPSPlayerCharacter>();
                player.BeHit(1);
            }
        }
        Destroy(gameObject);
    }
}
