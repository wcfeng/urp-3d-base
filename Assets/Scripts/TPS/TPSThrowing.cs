using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSThrowing : MonoBehaviour
{
    // 投掷时指定投掷的目标点
 public Vector3 throwTarget;
    // 与飞行有关的参数
    float time;
    float speedParam = 20;
    Vector3 startPos;
    float vx, vz, a, vy, T;
    // free 为 true 代表不再受脚本控制，变成普通刚体
    bool free;
    // 刚体和碰撞体组件初始为动力学刚体和触发器
    Rigidbody rigid;
    Collider coll;
    private void Start()
    {
        rigid = GetComponent<Rigidbody>();
        coll = GetComponent<Collider>();
        startPos = transform.position;
        // 目标向量
        Vector3 to = throwTarget - startPos;
        float d = to.magnitude;
        // 下面根据距离计算出合适的参数，包括抛物线高度、初始速度
        // 算出预计飞行时间 T
        T = d / speedParam;
        // 抛物线高度 h 与距离相关，距离远则抛得高一些，让效果更自然
        float h = d * 0.4f;
        // 加速度a
        a = 2.0f * h / (T * T / 4.0f);
        // Y 轴初速度
        vy = a * (T / 2);
        // X 轴初速度
        vx = to.x / T;
        // Z 轴初速度
        vz = to.z / T;
        free = false;
    }
    void Update()
    {
    if (free)
        {
            return;
        }
        // 在空中转起来比较好看
        transform.Rotate(0, 3, 0);
        // 根据时间改变位置，算法符合物理
        float dt = Time.deltaTime;
        time += dt;
        transform.position += new Vector3(vx, vy, vz) * dt;
        // Y 方向的速度受加速度影响
        vy -= a * dt;
    }
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            return;
        }
        if (free) return;
        // 当投掷物碰到地面或障碍时，就切换成普通的刚体碰撞体
        free = true;
        coll.isTrigger = false;
        rigid.isKinematic = false;
    }
    // 投掷物被敌人发现后，过一段时间消失。这个函数被其他逻辑调用
    public void DelayDestroy()
    {
        Destroy(gameObject, 10);
    }
}
