using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TPSEnemyCharacter : MonoBehaviour
{
    public enum AIState
    {
        Patrol, // 巡逻
        Attack, // 攻击
        Chase, // 追击
        Die, // 死亡
        Freeze, // 被冻结（如踩中了陷阱）
    }
    // 游戏中的角色属性
    float walkSpeed = 2.5f;
    float runSpeed = 8f;
    float hp = 3;
    // 各种距离配置，包括视野距离、射击距离、追击距离
    public float maxScanDist = 16f;
    public float maxAttackDist = 13f;
    public float maxChaseDist = 16f;
    // 被陷阱夹住的冻结时间
    float freezeTime;
    // 引用其他组件
    Animator anim;
    NavMeshAgent agent;
    CapsuleCollider coll;
    // 武器射击相关，与Player 角色类似
    Transform attackTarget;
    public Transform prefabBullet;
    public Transform weaponSlot;
    float nextShootTime = 0;
    public float shootInterval = 0.1f;


    void Start()
    {
        anim = GetComponent<Animator>();
        coll = GetComponent<CapsuleCollider>();
        // 动画变量
        anim.SetBool("Static _ b", true);
        anim.SetInteger("WeaponType _ int", 6);
        // 导航代理初始设置
        agent = GetComponent<NavMeshAgent>();
        if (agent.avoidancePriority == 0) // 设置避障优先级，多个敌人集体寻路时互相避让
        {
            agent.avoidancePriority = Random.Range(30, 61);
        }
        patrolIndex = 0;
        agent.isStopped = true;
        // AI 状态机初始为巡逻
        state = AIState.Patrol;
        // 渲染视野范围用的物体和组件
        viewIndicator = transform.Find("ViewIndicator");
        viewFilter = viewIndicator.GetComponent<MeshFilter>();
        viewRenderer = viewIndicator.GetComponent<MeshRenderer>();
    }
    // 更新动画，与主角类似
    void UpdateAnim()
    {
        if (state == AIState.Die)
        {
            anim.SetFloat("Speed _ f", 0);
            return;
        }
        if (state == AIState.Chase || state == AIState.Attack)
        {
            anim.SetBool("Shoot _ b", true);
            weaponSlot.localRotation = Quaternion.Euler(0, 0, 50);
        }
        else
        {
            anim.SetBool("Shoot _ b", false);
            weaponSlot.localRotation = Quaternion.Euler(0, 0, 0);
        }
        float speed = agent.velocity.magnitude / runSpeed;
        anim.SetFloat("Speed _ f", speed);
    }
    public void BeHit(float damage)
    {
        if (hp <= 0)
        {
            return;
        }
        hp -= damage;
        if (hp <= 0)
        {
            hp = 0;
            Die();
        }
    }
    // 踩了陷阱时调用此函数，被暂时冻结
    public void OnTrap(float time)
    {
        state = AIState.Freeze;
        freezeTime = Time.time + time;
        agent.isStopped = true;
    }
    // 死亡
    void Die()
    {
        state = AIState.Die;
        agent.isStopped = true;
        agent.enabled = false;
        transform.Rotate(90, 0, 0);
        Destroy(viewFilter);
        Destroy(viewRenderer);
        transform.Find("Corpse").gameObject.SetActive(true);
    }
    // 实现射击的函数，与玩家类似
    void Shoot(Vector3 target)
    {
        if (Time.time < nextShootTime)
        {
            return;
        }

        target.y = transform.position.y;
        transform.LookAt(target);
        Vector3 dir = target - transform.position;
        dir.y = 0;
        dir = dir.normalized;
        Vector3 startPos = new Vector3(transform.position.x, transform.position.y
       + coll.height / 2, transform.position.z);
        startPos += dir * (2.5f * coll.radius);
        Transform bullet = Instantiate(prefabBullet, startPos, Quaternion.identity);
        TPSBullet b = bullet.GetComponent<TPSBullet>();
        b.Init("Player", dir);
        nextShootTime = Time.time + shootInterval;
    }

    // 巡逻相关
    public List<Transform> patrolPoints; // 所有巡逻点
    int patrolIndex; // 当前巡逻点序号
    void UpdatePatrol()
    {
        if (patrolPoints.Count == 0)
        {
            return;
        }
        if (agent.isStopped)
        {
            agent.SetDestination(patrolPoints[patrolIndex].position);
            agent.isStopped = false;
            return;
        }
        // 判断到达当前目标点
        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            // 如果只有一个点，那么转向该点的朝向。因为一个点代表站岗状态，所以朝向很重要
            if (patrolPoints.Count == 1)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation,
                patrolPoints[0].rotation, 0.3f);
                return;
            }
            // 设置目标为下一个点
            patrolIndex++;
            patrolIndex = patrolIndex % patrolPoints.Count;
            agent.SetDestination(patrolPoints[patrolIndex].position);
        }
    }

    // 视觉感知相关
    Transform viewIndicator;
    MeshRenderer viewRenderer;
    MeshFilter viewFilter;
    Transform UpdateScan()
    {
        List<Vector3> points = new List<Vector3>();
        // 绘制视线范围开始
        System.Func<bool> _DrawRange = () =>
        {
            List<int> tris = new List<int>();
            for (int i = 2; i < points.Count; i++)
            {
                tris.Add(0);
                tris.Add(i - 1);
                tris.Add(i);
            }
            Mesh mesh = new Mesh();
            mesh.vertices = points.ToArray();
            mesh.triangles = tris.ToArray();
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            viewFilter.mesh = mesh;
            return true;
        };
        Vector3 offset = new Vector3(0, 1, 0); // 每个顶点都抬高一点
        points.Add(offset);
        for (int d = -60; d < 60; d += 4)
        {
            Vector3 v = Quaternion.Euler(0, d, 0) * transform.forward;
            Ray ray = new Ray(transform.position + offset, v);
            RaycastHit hitInfo;
            if (!Physics.Raycast(ray, out hitInfo, maxScanDist))
            {
                Vector3 localv = transform.InverseTransformVector(v);
                points.Add(offset + localv * maxScanDist);
                //Debug.DrawLine(transform.position, transform.position+v*maxScanDist, Color.red);
            }
            else
            {
                Vector3 local = transform.InverseTransformPoint(hitInfo.point);
                points.Add(offset + local);
                //Debug.DrawLine(transform.position, hitInfo.point, Color.red);
            }
        }
        _DrawRange();
        // 绘制视线范围结束
        // 检测视线内物体
        // 球形覆盖检测
        Collider[] colliders = Physics.OverlapSphere(transform.position, maxScanDist);
        //Debug.DrawLine(transform.position, transform.position + transform.forward * maxScanDist);
        List<Transform> targets = new List<Transform>();
        foreach (var c in colliders)
        {
            // 从当前角色到目标的向量
            Vector3 to = c.gameObject.transform.position - transform.position;
            // 判断角度
            if (Vector3.Angle(transform.forward, to) > 60)
            {
                continue;
            }
            // 发射射线以判断能否看到目标
            Ray ray = new Ray(transform.position + offset, to);
            RaycastHit hitInfo;
            if (!Physics.Raycast(ray, out hitInfo, maxScanDist))
            {
                continue;
            }
            if (hitInfo.collider != c)
            {
                continue;
            }
            Debug.DrawLine(transform.position + offset, hitInfo.point, Color.blue);
            // 下面通过几个 if 判断让 Player 物体优先级最高
            if (c.gameObject.tag == "Player")
            {
                return c.transform; // 最高优先级，直接返回
            }
            if (c.gameObject.tag == "Cigar")
            {
                targets.Add(c.transform);
            }
            if (c.gameObject.tag == "Corpse")
            {
                targets.Add(c.transform);
            }
        }
        if (targets.Count > 0)
        {
            return targets[0];
        }
        return null;
    }

    //追击
    void UpdateChase()
    {
        // 没有目标则回到巡逻状态
        if (attackTarget == null)
        {
            state = AIState.Patrol;
            return;
        }
        // 如果距离过远则回到巡逻状态
        if (Vector3.Distance(attackTarget.position, transform.position) > maxChaseDist)
        {
            state = AIState.Patrol;
            agent.isStopped = true;
            attackTarget = null;
            return;
        }
        // 追击过程中需要不断更新视野。例如追击诱饵时发现了玩家，就将目标改为玩家
        Transform target = UpdateScan();
        if (target == null)
        {
            return;
        }
        // 如果目标进入射程，转为攻击状态
        if (Vector3.Distance(target.position, transform.position) < maxAttackDist)
        {
            state = AIState.Attack;
            attackTarget = target;
            agent.isStopped = true;
            return;
        }
        if (Vector3.Distance(target.position, agent.destination) > 0.5f)
        {
            agent.isStopped = false;
            agent.SetDestination(target.position);
        }
    }

    //攻击
    void UpdateAttack()
    {
        if (attackTarget == null)
        {
            state = AIState.Patrol;
            return;
        }
        // 也要不断更新视野目标
        Transform target = UpdateScan();
        if (target == null)
        {
            state = AIState.Chase;
            agent.isStopped = false;
            return;
        }
        attackTarget = target;
        if (Vector3.Distance(attackTarget.position, transform.position) > maxAttackDist)
        {
            agent.isStopped = false;
            agent.SetDestination(attackTarget.position);
            return;
        }
        agent.isStopped = true;
        if (attackTarget.tag == "Player")
        {
            Shoot(attackTarget.position);
        }
        else if (attackTarget.tag == "Cigar") // 如果目标是投掷物，稍后销毁即可
        {
            attackTarget.GetComponent<TPSThrowing>().DelayDestroy();
        }
    }

    // AI 状态
    AIState _state;
    // 写成属性是为了方便加 Log 调试
    AIState state
    {
        get { return _state; }
        set
        {
            //Debug.Log("state change to " + value.ToString());
            _state = value;
        }
    }
    void Update()
    {
        UpdateAI();
        UpdateAnim();
    }
    void UpdateAI()
    {
        switch (state)
        {
            case AIState.Patrol:
                {
                    agent.speed = walkSpeed;
                    UpdatePatrol();
                    Transform target = UpdateScan();
                    if (target != null)
                    {
                        state = AIState.Chase;
                        attackTarget = target;
                    }
                }
                break;
            case AIState.Chase:
                {
                    agent.speed = runSpeed;
                    UpdateChase();
                }
                break;
            case AIState.Attack:
                {
                    UpdateAttack();
                }
                break;
            case AIState.Die:
                break;
            case AIState.Freeze:
                if (Time.time > freezeTime)
                {
                    state = AIState.Patrol;
                }
                break;
        }
    }
}
