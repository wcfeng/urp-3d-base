using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

public class TPSWeapon : MonoBehaviour
{
    public Transform weaponSlot; // 武器槽位
    public Transform gunfirePos; // 定位枪口闪光
    public ParticleSystem prefabGunFire; // 枪口火光预制体
    public Transform prefabBullet; // 子弹预制体
    [HideInInspector]
    public float nextShootTime; // 下次可以开火的时间
    public float shootInterval = 0.3f; // 射击间隔

    private CharacterController cc;

    void Start()
    {
        // ......
        cc = GetComponent<CharacterController>();
        if (!weaponSlot) weaponSlot = transform.FindChildByName("WeaponSlot");
        weaponSlot.gameObject.SetActive(false);
        gunfirePos = weaponSlot.Find("GunfirePos");
        // ......
    }

    public void Shoot(Vector3 target)
    {
        if (Time.time < nextShootTime)
        {
            return;
        }
        Vector3 dir = target - transform.position;
        dir.y = 0;
        dir = dir.normalized;
        Vector3 startPos = new Vector3(transform.position.x, transform.position.y + cc.height / 2, transform.position.z);
        startPos += dir * (2.5f * cc.radius); // 发射点稍微往前挪一点
        Transform bullet = Instantiate(prefabBullet, startPos, Quaternion.identity);
        TPSBullet b = bullet.GetComponent<TPSBullet>();
        // 设置子弹 Tag 为 Enemy，用于后续判断，防止子弹打到自己
        b.Init("Enemy", dir, 0.2f);
        // 可加音效（可省略）
        // PlaySound(handgunSound);
        // 枪口粒子（可省略）
        if (prefabGunFire)
        {
            //生成枪口特效
            var particle = Instantiate(prefabGunFire, gunfirePos.position, Quaternion.identity);
            //将特效作为枪口的子集
            particle.transform.SetParent(gunfirePos);
            //闪烁0.2秒移除
            Destroy(particle, 0.2f);
        }
        // 枪口光源
        var light = gunfirePos.GetComponent<Light>();
        if(light)
            light.enabled = true;
        // 枪口光源闪烁一次
        StartCoroutine(_Flash(light));
        // 控制开枪的时间间隔
        nextShootTime = Time.time + shootInterval;
    }
    // 实现枪口闪光的协程
    IEnumerator _Flash(Light light)
    {
        if (light == null) { yield break; }
        light.enabled = true;
        yield return new WaitForSeconds(0.1f);
        light.enabled = false;
    }
}
