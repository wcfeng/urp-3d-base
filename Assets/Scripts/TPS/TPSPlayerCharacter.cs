using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSPlayerCharacter : MonoBehaviour
{
    // 游戏中的角色属性
    public float moveSpeed;
    public float hp = 9999;
    [HideInInspector]
    public bool dead = false;
    [HideInInspector]
    public Vector3 curInput; // 当前输入方向
    // 引用其他组件
    CharacterController cc;
    TPSPlayerInput playerInput;
    TPSWeapon tPSWeapon;
    Animator anim;
    // 当前道具
    public string curItem;
    void Start()
    {
        cc = GetComponent<CharacterController>();
        playerInput = GetComponent<TPSPlayerInput>();
        anim = GetComponentInChildren<Animator>();
        tPSWeapon = GetComponent<TPSWeapon>();
        anim.SetBool("Static_b", true);
        curItem = "none";
    }
    public void UpdateMove()
    {
        if (dead) return;
        float curSpeed = moveSpeed;
        switch (curItem)
        {
            case "none":
                break;
            case "Handgun": // 手枪
                {
                    curSpeed = moveSpeed / 2.0f;
                }
                break;
            case "Grenade": // 手雷
                {
                    curSpeed = moveSpeed / 2.0f;
                }
                break;
            case "Trap": // 陷阱
                {
                    curSpeed = 0;
                }
                break;
            case "Knife": // 匕首
                curSpeed = moveSpeed / 2.0f;
                break;
        }
        // 重力下落
        Vector3 v = curInput;
        if (!cc.isGrounded)
        {
            v.y = -0.5f;
        }
        cc.Move(v * curSpeed * Time.deltaTime);
    }
    public void UpdateAnim(Vector3 curInputPos)
    {
        if (dead)
        {
            anim.SetFloat("Speed_f", 0);
            return;
        }
        anim.SetFloat("Speed_f", cc.velocity.magnitude / moveSpeed);
        if (cc.velocity.magnitude > moveSpeed / 1.9f)
        {
            //播放走路声音
            //PlayFootstepSound();
        }
        //收起武器
        tPSWeapon.weaponSlot.gameObject.SetActive(false);

        if (curItem == "Handgun")
        {
            anim.SetLayerWeight(anim.GetLayerIndex("Weapons"), 1);
            // 显示武器
            tPSWeapon.weaponSlot.gameObject.SetActive(true);
            tPSWeapon.weaponSlot.Find("Weapon_Pistol").gameObject.SetActive(true);
            tPSWeapon.weaponSlot.Find("Weapon_Knife").gameObject.SetActive(false);
            tPSWeapon.weaponSlot.Find("Weapon_Grenade").gameObject.SetActive(false);
            // 调整枪口朝向
            Vector3 v = new Vector3(curInputPos.x, transform.position.y, curInputPos.z);
            transform.LookAt(v);
        }
        else if (curItem == "Grenade")
        {
            anim.SetLayerWeight(anim.GetLayerIndex("Weapons"), 1);
            tPSWeapon.weaponSlot.gameObject.SetActive(true);
            tPSWeapon.weaponSlot.Find("Weapon_Pistol").gameObject.SetActive(false);
            tPSWeapon.weaponSlot.Find("Weapon_Grenade").gameObject.SetActive(true);
            Vector3 v = new Vector3(curInputPos.x, transform.position.y, curInputPos.z);
            transform.LookAt(v);
        }
        else if (curItem == "Trap")
        {
            anim.SetFloat("Speed_f", 0);
            anim.SetInteger("WeaponType_int", 9);
        }
        else if (curItem == "Knife")
        {
            anim.SetFloat("Speed_f", 0);
            anim.SetInteger("WeaponType_int", 4);
            transform.Rotate(0, 1, 0);
        }
        else
        {
            anim.SetInteger("WeaponType_int", 0);
            anim.SetLayerWeight(anim.GetLayerIndex("Weapons"), 0);
            if (curInput.magnitude > 0.01f)
            {
                transform.rotation = Quaternion.LookRotation(curInput);
            }
        }
        // 如果有输入，转向输入方向
        //if (curInput.magnitude > 0.01f)
        //{
        //    transform.rotation = Quaternion.LookRotation(curInput);
        //}
    }

    // 投掷物相关字段
    [HideInInspector]
    public Vector3 throwTarget; // 投掷目标点
    public TPSThrowing prefabCigar; // 投掷物预制体
    void ThrowItem(TPSThrowing item)
    {
        // 发射位置
        Vector3 startPos = new Vector3(transform.position.x, transform.position.y
       + cc.height / 2, transform.position.z);
        startPos += transform.forward * (2.0f * cc.radius); // 发射位置往前移一些
                                                            // 创建投掷物
        TPSThrowing obj = Instantiate(item, startPos, Quaternion.identity);
        obj.throwTarget = throwTarget;
    }

    public void BeHit(float damage)
    {
        if (hp <= 0)
        {
            return;
        }
        hp -= damage;
        if (hp <= 0)
        {
            hp = 0;
            Die();
        }
    }

    void Die()
    {
        transform.Rotate(90, 0, 0);
        dead = true;
        //GameMode.Instance.GameOver();
    }

    // 陷阱相关逻辑
    [HideInInspector]
    public float settingTrapTime; // 陷阱设置完毕的时刻
    [HideInInspector]
    public float settingTrapStart = -100; // 开始设置陷阱的时刻
    public float settingTrapInterval = 4.0f; // 陷阱道具 CD
    public Transform prefabTrap; // 陷阱预制体
    public void BeginTrap()
    {
        if (settingTrapStart + settingTrapInterval > Time.time)
        {
            curItem = "none";
            return;
        }
        settingTrapTime = Time.time + 2;
    }
    void PlaceTrap()
    {
        Instantiate(prefabTrap, transform.position, Quaternion.identity);
        settingTrapStart = Time.time;
    }

    public void UpdateAction(Vector3 curInputPos, bool fire)
    {
        if (dead) return;
        switch (curItem)
        {
            case "none":
                break;
            case "Handgun": // 手枪
                {
                    anim.SetInteger("WeaponType_int", 6);
                    if (fire)
                    {
                        tPSWeapon.Shoot(curInputPos);
                        anim.SetBool("Shoot_b", true);
                        //BigNoise();
                    }
                    else
                    {
                        anim.SetBool("Shoot_b", false);
                    }
                }
                break;
            case "Grenade": // 投掷物
                {
                    throwTarget = curInputPos;
                    if (fire)
                    {
                        curItem = "none";
                        ThrowItem(prefabCigar);
                    }
                }
                break;
            case "Trap": // 陷阱
                if (Time.time > settingTrapTime)
                {
                    curItem = "none";
                    PlaceTrap();
                }
                break;
            case "Knife":
                anim.SetInteger("WeaponType_int", 101);
                if (fire)
                {
                //Stab();
                    anim.SetBool("Shoot_b", true);
                }
                else
                {
                    anim.SetBool("Shoot_b", false);
                }
                break;
        }
    }

}
