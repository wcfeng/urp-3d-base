using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSPlayerInput : MonoBehaviour
{
    TPSPlayerCharacter player;
    LineRenderer throwLine;
    [HideInInspector]
    public Vector3 curGroundPoint;
    void Start()
    {
        player = GetComponent<TPSPlayerCharacter>();
        throwLine = GetComponentInChildren<LineRenderer>();
    }
    void Update()
    {
        Vector3 input;
        input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        if (input.magnitude > 1.0f)
        {
            input = input.normalized;
        }
        // 设置当前输入向量
        player.curInput = input;
        bool fire = false;
        fire = Input.GetKeyDown(KeyCode.J);
        bool touch = TouchGroundPos(); // 函数内部会记录鼠标指针当前指向的点curGroundPoint
        // 由 PlayerInput 脚本调用 PlayerCharacter 脚本
        player.UpdateMove();
        player.UpdateAction(curGroundPoint, fire); // 之后会用到
        player.UpdateAnim(curGroundPoint);

        if (player.curItem == "Grenade")
        {
            DrawThrowLine(curGroundPoint);
        }
        else
        {
            HideThrowLine();
        }
    }
    bool TouchGroundPos()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        // 仅检测 Ground Layer。注意将地板设置为 Ground 层
        if (!Physics.Raycast(ray, out hitInfo, 1000, LayerMask.GetMask("Ground")))
        {
            return false;
        }
        curGroundPoint = hitInfo.point;
        return true;
    }

    public void OnSelectItem(string item)
    {
        if (player.dead) return;
        player.curItem = item;
        // 切换武器时可进行相关操作，陷阱的代码后面会用到
        switch (item)
        {
            case "Handgun": // 手枪
                break;
            case "Grenade": // 投掷物
                break;
            case "Trap": // 陷阱
                {
                    //player.BeginTrap();
                }
                break;
            case "Knife":
                break;
        }
    }

    // 隐藏抛物线
    void HideThrowLine()
    {
        throwLine.positionCount = 0;
    }

    // 显示抛物线
    public void DrawThrowLine(Vector3 _target)
    {
        Debug.Log(_target);
        Vector3 target = transform.InverseTransformPoint(_target);
        float d = target.magnitude;
        float h = d / 3;
        // 算法与抛物线飞行类似，用 20 条直线段模拟曲线
        int T = 20; // 抛物线分成 T 段
        float T_half = T / 2.0f;
        float a = 2 * h / (T_half *T_half);
        float vx = target.x / T;
        float vz = target.z / T;
        List<Vector3> points = new List<Vector3>();
        // 下面的点坐标都是局部坐标系的
        //points.Add(Vector3.zero);
        points.Add(transform.position);
        //Vector3 p = Vector3.zero;
        Vector3 p = transform.position;
        float vy = T_half *a;
        for (int i = 0; i < T; i++)
        {
            p.x += vx;
            p.z += vz;
            p.y += vy - a / 2.0f;
            vy -= a;
            points.Add(p);
        }
        // 将 points 列表赋给 Line Renderer 组件，就会显示曲线了
        throwLine.positionCount = points.Count;
        throwLine.SetPositions(points.ToArray());
    }
}
