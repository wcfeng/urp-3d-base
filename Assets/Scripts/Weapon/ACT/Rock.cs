﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using FF;

namespace FF.Weapon
{
    public class Rock : MonoBehaviour
    {
        //石头状态
        public enum RockState
        {
            HitPlayer,
            HitEnemy,
            HitNothing,
        }
        /// <summary>
        /// 石头当前状态
        /// </summary>
        public RockState rockState;
        private Rigidbody rb;
        /// <summary>
        /// 力
        /// </summary>
        [Header("Basic Settings")]
        public float force;
        /// <summary>
        /// 攻击目标
        /// </summary>
        public GameObject target;
        /// <summary>
        /// 攻击方向
        /// </summary>
        private Vector3 direction;
        /// <summary>
        /// 破碎效果预制件
        /// </summary>
        public GameObject breakEffect;
        /// <summary>
        /// 伤害
        /// </summary>
        public int damage = 10;

        private void Start()
        {
            rb = GetComponent<Rigidbody>();
            rb.velocity = Vector3.one;
            rockState = RockState.HitPlayer;
            FlyToTarget();
        }

        private void FixedUpdate()
        {
            if (rb.velocity.sqrMagnitude < 1)
            {
                rockState = RockState.HitNothing;
            }
        }

        private void FlyToTarget()
        {
            //获取攻击目标
            if (target == null)
                target = FindObjectOfType<PlayerState>().gameObject;
            //设置攻击方向 添加一个向上的方向
            direction = (target.transform.position - transform.position + Vector3.up).normalized;
            //添加冲击力
            rb.AddForce(direction * force, ForceMode.Impulse);
        }

        private void OnCollisionEnter(Collision other)
        {
            switch (rockState)
            {
                case RockState.HitPlayer:
                    //攻击到玩家
                    if (other.gameObject.CompareTag(EM.Tag.Player.ToString()))
                    {
                        //停止行动 添加击退的力 设置眩晕状态 造成伤害 石头转为闲置状态
                        other.gameObject.GetComponent<NavMeshAgent>().isStopped = true;
                        other.gameObject.GetComponent<NavMeshAgent>().velocity = direction * force;
                        other.gameObject.GetComponent<Animator>().SetTrigger("Dizzy");
                        other.gameObject.GetComponent<CharacterState>().Damage(damage);

                        rockState = RockState.HitNothing;
                    }
                    break;
                case RockState.HitEnemy:
                    //攻击到敌人
                    if (other.gameObject.GetComponent<EnemyState>())
                    {
                        //造成伤害 创建爆炸特效 销毁石头
                        EnemyState otherState = other.gameObject.GetComponent<EnemyState>();
                        otherState.Damage(damage);
                        Instantiate(breakEffect, transform.position, Quaternion.identity);
                        Destroy(gameObject);
                    }
                    break;
                case RockState.HitNothing:
                    break;
                default:
                    break;
            }
        }
    }
}
