using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageAnimTest : MonoBehaviour
{
    Image image;
    // 可以在编辑器里指定另一张图片
    public Sprite otherSprite;
    float fillAmount = 0;
    void Start()
    {
        // 获取 Image 组件
        image = GetComponent<Image>();

        // 直接将图片换为另一张图片
        if (otherSprite != null)
        {
            image.sprite = otherSprite;
        }
        // 将图片类型改为 Filled，360°填充，方便制作旋转动画
        image.type = Image.Type.Filled;
        image.fillMethod = Image.FillMethod.Radial360;
    }
    void Update()
    {
        // 制作一个旋转显示的动画效果，直线效果也是类似的
        // 取值为 0~1
        image.fillAmount = fillAmount;
        fillAmount += Time.deltaTime;
        if (fillAmount > 1)
        {
            fillAmount = 0;
        }
    }
}
