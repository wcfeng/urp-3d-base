using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FF.Weapon
{
    /// <summary>
    /// 自动步枪
    /// </summary>
    public class Rifle : Gun
    {
        /// <summary>
        /// 开火函数，由角色脚本调用
        /// </summary>
        /// <param name="keyDown">按下开火键</param>
        /// <param name="keyPressed">开火键正在持续按下</param>
        public override void Fire(bool keyDown, bool keyPressed)
        {
            if (keyPressed)
            {
                base.Fire(keyDown, keyPressed);
                if (lastFireTime + CD > Time.time)
                {
                    return;
                }
                lastFireTime = Time.time;
                GameObject bullet = Instantiate(prefabBullet, null);
                bullet.GetComponent<Bullet>().owner = gameObject;
                bullet.transform.position = transform.position + transform.forward * 1.0f;
                bullet.transform.forward = transform.forward;
            }
        }
    }
}
