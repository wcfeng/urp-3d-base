using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FF.Weapon
{
    /// <summary>
    /// 枪 抽象类
    /// </summary>
    public abstract class Gun : MonoBehaviour
    {
        /// <summary>
        /// 子弹预制件
        /// </summary>
        public GameObject prefabBullet;
        /// <summary>
        /// 发射cd时间
        /// </summary>
        public float CD = 0.2f;
        /// <summary>
        /// 枪类型
        /// </summary>
        public EM.GunType gunType;
        /// <summary>
        /// 枪的拥有者
        /// </summary>
        [HideInInspector]
        public GameObject owner;
        /// <summary>
        /// 开火
        /// </summary>
        public virtual void Fire(bool keyDown, bool keyPressed)
        {

        }
        // 上次开火时间
        public float lastFireTime;
    }
}
