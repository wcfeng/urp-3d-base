using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FF.Weapon
{
    /// <summary>
    /// 散弹枪
    /// </summary>
    public class Shotgun : Gun
    {
        /// <summary>
        /// 开火函数，由角色脚本调用
        /// </summary>
        /// <param name="keyDown">按下开火键</param>
        /// <param name="keyPressed">开火键正在持续按下</param>
        public override void Fire(bool keyDown, bool keyPressed)
        {
            if (keyDown)
            {
                base.Fire(keyDown, keyPressed);
                if (lastFireTime + CD > Time.time)
                {
                    return;
                }
                lastFireTime = Time.time;
                // 创建 5 颗子弹，相互间隔 10°，分布于前方扇形区域
                for (int i = -2; i <= 2; i++)
                {
                    GameObject bullet = Instantiate(prefabBullet, null);
                    bullet.GetComponent<Bullet>().owner = gameObject;
                    Vector3 dir = Quaternion.Euler(0, i * 10, 0) * transform.forward;
                    bullet.transform.position = transform.position + dir * 1.0f;
                    bullet.transform.forward = dir;
                    // 霰弹枪的子弹射击距离很短，因此修改子弹的生命周期
                    Bullet b = bullet.GetComponent<Bullet>();
                    b.lifeTime = 0.3f;
                }
            }
        }
    }
}
