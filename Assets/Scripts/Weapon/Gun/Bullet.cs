using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FF.Weapon
{
    /// <summary>
    /// 子弹
    /// </summary>
    public class Bullet : MonoBehaviour
    {
        // 子弹飞行速度
        public float speed = 10.0f;
        // 子弹生命期（几秒之后消失）
        public float lifeTime = 2;
        public EM.Tag effectTag;
        // 子弹出生的时间
        private float startTime;
        /// <summary>
        /// 子弹的拥有者
        /// </summary>
        [HideInInspector]
        public GameObject owner;
        private void Start()
        {
            startTime = Time.time;
        }
        private void Update()
        {
            // 子弹移动
            transform.position += speed * transform.forward * Time.deltaTime;
            // 超过一定时间销毁自身
            if (startTime + lifeTime < Time.time)
            {
                Destroy(gameObject);
            }
        }
        // 当子弹碰到其他物体时触发
        private void OnTriggerEnter(Collider other)
        {
            if (CompareTag(other.tag))
            {
                return;
            }
            Destroy(gameObject);
        }
    }
}
