using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FF.Weapon
{
    /// <summary>
    /// 敌人
    /// </summary>
    public class GunEnemy : GunCharacter
    {
        // 用于制作死亡效果的预制体，暂时不用
        public GameObject prefabBoomEffect;
        public float speed = 2;
        public float fireTime = 0.1f;
        public float maxHp = 1;
        public float stopDistance = 5;
        Transform player;

        protected override void Start()
        {
            base.Start();
            // 初始确保满血状态
            hp = maxHp;
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
        void Update()
        {
            Move();
            Fire();
        }
        void Move()
        {
            // 玩家的 input 是从键盘输入而来，而敌人的 input 则是始终指向玩家的方向
            input = player.position - transform.position;

            input = input.normalized;
            if (input.magnitude > stopDistance)
            {
                transform.position += input * speed * Time.deltaTime;
            }
            
            if (input.magnitude > 0.1f)
            {
                transform.forward = input;
            }
        }
        void Fire()
        {
            // 一直开枪，开枪的频率可以通过武器启动控制
            gunManager.Fire(true, true);
        }
        private void OnTriggerEnter(Collider other)
        {
            Bullet bullet = other.GetComponent<Bullet>();

            if (bullet && bullet.owner.tag != tag)
            {
                Destroy(other.gameObject);
                hp--;
                if (hp <= 0)
                {
                    dead = true;
                    // 这里可以添加死亡特效
                    Instantiate(prefabBoomEffect, transform.position, transform.rotation);
                    Destroy(gameObject);
                }
            }
        }

    }
}
