using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FF.Weapon
{
    /// <summary>
    /// 枪管理器
    /// </summary>
    public class GunManager : MonoBehaviour
    {
        /// <summary>
        /// 枪列表
        /// </summary>
        public Gun[] guns;
        private Gun currentGun;
        private int currentGunIndex;

        private void Start()
        {
            currentGun = guns[0];
            currentGun.owner = gameObject;
        }
        /// <summary>
        /// 切换枪
        /// </summary>
        public void ChangeGun()
        {

            currentGunIndex += 1;
            if (currentGunIndex == guns.Length)
            {
                currentGunIndex = 0;
            }
            currentGun = guns[currentGunIndex];
            currentGun.owner = gameObject;
        }
        
        public void Fire(bool keyDown, bool keyPressed)
        {
            currentGun.Fire(keyDown, keyPressed);
        }
    }
}
