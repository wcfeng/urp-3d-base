using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FF.Weapon
{
    /// <summary>
    /// 角色
    /// </summary>
    public class GunCharacter : MonoBehaviour
    {
        // 变量，输入方向用
        protected Vector3 input;
        protected GunManager gunManager;
        // 是否死亡
        protected bool dead;
        // 当前血量
        protected float hp;
        protected virtual void Start()
        {
            gunManager = GetComponent<GunManager>();
        }
    }
}
