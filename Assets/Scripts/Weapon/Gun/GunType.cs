﻿namespace EM
{
    /// <summary>
    /// 枪类型
    /// </summary>
    public enum GunType
    {
        /// <summary>
        /// 手枪
        /// </summary>
        Pistol = 1,
        /// <summary>
        /// 散弹枪
        /// </summary>
        Shotgun = 2,
        /// <summary>
        /// 自动步枪
        /// </summary>
        Rifle = 4,
    }
}