using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FF.Weapon
{
    /// <summary>
    /// 玩家
    /// </summary>
    public class GunPlayer : GunCharacter
    {
        // 移动速度
        public float speed = 3;
        // 最大血量
        public float maxHp = 20;
        private Rigidbody rigid;
        // 是否死亡
        protected override void Start()
        {
            base.Start();
            // 初始确保满血状态
            hp = maxHp;
            rigid = GetComponent<Rigidbody>();

        }
        private void Update()
        {
            // 将键盘的横向、纵向输入，保存在 input 变量中
            input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            // 未死亡则执行移动逻辑
            if (!dead)
            {
                Move();
                Fire();
                Jump();

                if (Input.GetKeyDown(KeyCode.R))
                {
                    rigid.centerOfMass = new Vector3(0, -1, 0);
                    rigid.angularVelocity = new Vector3(0, 3.14f, 0);
                }
            }
        }
        private void Move()
        {
            // 先归一化输入向量，让输入更直接，同时避免斜向移动时速度超过最大速度
            input = input.normalized;
            transform.position += input * speed * Time.deltaTime;
            // 令角色前方与移动方向一致
            if (input.magnitude > 0.1f)
            {
                transform.forward = input;
            }
            // 以上移动方式没有考虑阻挡，因此使用下面的代码限制移动范围
            Vector3 temp = transform.position;
            const float BORDER = 20;
            if (temp.z > BORDER) { temp.z = BORDER; }
            if (temp.z < -BORDER) { temp.z = -BORDER; }
            if (temp.x > BORDER) { temp.x = BORDER; }
            if (temp.x < -BORDER) { temp.x = -BORDER; }
            transform.position = temp;
        }

        private void Jump()
        {
            if (Input.GetButtonDown("Jump"))
            {
                rigid.velocity = new Vector3(rigid.velocity.x, 0, rigid.velocity.z);
                rigid.AddForce(new Vector3(0, 100, 0));
            }
        }

        private void Fire()
        {
            bool fireKeyDown = Input.GetKeyDown(KeyCode.J);
            bool fireKeyPressed = Input.GetKey(KeyCode.J);
            bool changeWeapon = Input.GetKeyDown(KeyCode.Q);

            gunManager.Fire(fireKeyDown, fireKeyPressed);
            if (changeWeapon)
            {
                gunManager.ChangeGun();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            Bullet bullet = other.GetComponent<Bullet>();
            if (bullet && bullet.owner.tag != tag)
            {
                hp--;
                if (hp <= 0) { dead = true; }
            }
        }
    }
}
