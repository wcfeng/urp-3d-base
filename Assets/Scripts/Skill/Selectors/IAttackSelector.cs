using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FF.Skill
{
    /// <summary>
    /// 攻击选区接口
    /// </summary>
    public interface IAttackSelector
    {
        /// <summary>
        /// 查找目标
        /// </summary>
        /// <param name="skillData">技能</param>
        /// <param name="skillTF">技能位置</param>
        /// <returns></returns>
        Transform[] SelectTarget(SkillData_SO skillData, Transform skillTF);
    }
}
