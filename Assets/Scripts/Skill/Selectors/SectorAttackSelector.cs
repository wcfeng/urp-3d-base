using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace FF.Skill
{
    /// <summary>
    /// 扇形/圆形选区
    /// </summary>
    public class SectorAttackSelector : IAttackSelector
    {
        /// <summary>
        /// 查找目标
        /// </summary>
        /// <param name="skillData">技能</param>
        /// <param name="skillTF">技能位置</param>
        /// <returns></returns>
        public Transform[] SelectTarget(SkillData_SO skillData, Transform skillTF)
        {
            //选择目标
            List<Transform> targets = new List<Transform>();
            for (int i = 0; i < skillData.attackTargetTags.Length; i++)
            {
                GameObject[] targetGoArray = GameObject.FindGameObjectsWithTag(skillData.attackTargetTags[i].ToString());

                targets.AddRange(targetGoArray.Select(g => g.transform));
            }
            //判断攻击范围
            targets = targets.FindAll(t => 
            Vector3.Distance(t.position, skillTF.position) <= skillData.attackDistance + t.GetComponent<CharacterState>().stateData.modelRadius &&
            Vector3.Angle(skillTF.forward,t.position - skillTF.position) <= skillData.attackAngle /2
            );
            //找出活的角色
            foreach (var v in targets)
            {
                Debug.Log(v.GetComponent<CharacterState>());
                Debug.Log(v.GetComponent<CharacterState>().stateData);
                Debug.Log(v.GetComponent<CharacterState>().stateData.HP);
            }
            targets = targets.FindAll(t => t.GetComponent<CharacterState>().stateData.HP > 0);
            //返回目标 单攻/群攻
            Transform[] result = targets.ToArray();
            if (skillData.attackType == SkillAttackType.Group || result.Length == 0)
                return result;
            Transform min = result.Min(t => Vector3.Distance(t.position, skillTF.position));
            return new Transform[] { min };
        }
    }
}
