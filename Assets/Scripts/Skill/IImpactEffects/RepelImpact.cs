using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace FF.Skill
{
    /// <summary>
    /// 击退效果
    /// </summary>
    public class RepelImpact : IImpactEffect
    {
        //击退力度
        public float kickForce = 10;
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="deployer">技能释放器</param>
        public void Execute(SkillDeployer deployer)
        {
            //skillData = deployer.SkillData;
            RepelTarget(deployer);
        }

        private void RepelTarget(SkillDeployer deployer)
        {
            for (int i = 0; i < deployer.SkillData.attackTargets.Length; i++)
            {
                //攻击目标
                CharacterState characterState = deployer.SkillData.attackTargets[i].GetComponent<CharacterState>();
                //如果 技能的力 > 敌人质量  击退敌人
                if (deployer.SkillData.force - characterState.stateData.mass > 0)
                {
                    //获取击退方向
                    Vector3 direction = deployer.SkillData.attackTargets[i].transform.position - deployer.SkillData.owner.transform.position;
                    direction.Normalize();
                    //受到力影响
                    characterState.getForce(direction * kickForce);
                }
            }
        }
    }
}
