using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace FF.Skill
{
    /// <summary>
    /// 眩晕
    /// </summary>
    public class DizzyImpact : IImpactEffect
    {
        //眩晕时间
        public float timeout = 2;
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="deployer">技能释放器</param>
        public void Execute(SkillDeployer deployer)
        {
            foreach (Transform item in deployer.SkillData.attackTargets)
            {
                item.GetComponent<CharacterState>().Dizzy(timeout);
            }
            //skillData = deployer.SkillData;
            //Dizzy(deployer);
            //deployer.StartCoroutine(StopDizzy(deployer));
        }

        private void Dizzy(SkillDeployer deployer)
        {
            //进入眩晕
            for (int i = 0; i < deployer.SkillData.attackTargets.Length; i++)
            {
                //受到力影响
                deployer.SkillData.attackTargets[i].GetComponentInChildren<Animator>().SetBool(Const.Animation.Dizzy,true);
            }
        }

        private IEnumerator StopDizzy(SkillDeployer deployer)
        {

            //总伤害时间
            float atkTime = 0;
            do
            {
                yield return new WaitForSeconds(timeout);
                //退出眩晕
                for (int i = 0; i < deployer.SkillData.attackTargets.Length; i++)
                {
                    deployer.SkillData.attackTargets[i].GetComponentInChildren<Animator>().SetBool(Const.Animation.Dizzy,false);
                }
                atkTime += timeout;
            } while (atkTime < timeout);
        }
    }
}
