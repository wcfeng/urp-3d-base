using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FF.Skill
{
    /// <summary>
    /// 影响效果
    /// </summary>
    public interface IImpactEffect
    {
        /// <summary>
        /// 执行技能效果
        /// </summary>
        /// <param name="deployer">技能释放器</param>
        void Execute(SkillDeployer deployer);
    }
}
