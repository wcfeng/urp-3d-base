using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FF.Skill
{
    /// <summary>
    /// 消耗魔法
    /// </summary>
    public class CostMPImpact : IImpactEffect
    {
        public void Execute(SkillDeployer deployer)
        {
            CharacterState status = deployer.SkillData.owner.GetComponent<CharacterState>();
            status.stateData.MP -= deployer.SkillData.costMP;
        }
    }
}
