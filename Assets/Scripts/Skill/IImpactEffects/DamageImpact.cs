using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FF.Skill
{
    /// <summary>
    /// 伤害
    /// </summary>
    public class DamageImpact : IImpactEffect
    {
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="deployer">技能释放器</param>
        public void Execute(SkillDeployer deployer)
        {
            //skillData = deployer.SkillData;
            deployer.StartCoroutine(RepeatDamage(deployer));
        }
        /// <summary>
        /// 重复伤害
        /// </summary>
        /// <param name="deployer">技能释放器</param>
        /// <returns></returns>
        private IEnumerator RepeatDamage(SkillDeployer deployer)
        {
            //总伤害时间
            float atkTime = 0;
            do
            {
                //伤害
                OnceDamage(deployer.SkillData);
                yield return new WaitForSeconds(deployer.SkillData.atkInterval);
                atkTime += deployer.SkillData.atkInterval;
                deployer.CalculateTargets();//重新计算
            } while (atkTime < deployer.SkillData.durationTime);
        }
        /// <summary>
        /// 单次伤害
        /// </summary>
        /// <param name="skillData">技能数据</param>
        private void OnceDamage(SkillData_SO skillData)
        {
            float atk = skillData.atkRatio * skillData.owner.GetComponent<CharacterState>().stateData.attack;
            for (int i = 0; i < skillData.attackTargets.Length; i++)
            {
                CharacterState status = skillData.attackTargets[i].GetComponent<CharacterState>();
                status.Damage(atk);
            }
        }
    }
}
