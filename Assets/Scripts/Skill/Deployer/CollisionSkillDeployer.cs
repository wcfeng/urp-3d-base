using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FF.Skill
{
    /// <summary>
    /// 碰撞技能
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class CollisionSkillDeployer : SkillDeployer
    {
        /// <summary>
        /// 力
        /// </summary>
        [Tooltip("飞向目标时的力")]
        public float force = 10;
        public GameObject breakEffect;
        /// <summary>
        /// 向上的力 避免物体直接落到地上
        /// </summary>
        [Tooltip("向上的力 可以瞄准目标头部 避免物体直接落到地上")]
        public Vector3 upForce = new Vector3(0, 2, 0);
        private Rigidbody rb;
        private Vector3 direction;
        /// <summary>
        /// 是否处于闲置状态
        /// 不是闲置状态就是攻击状态 
        /// 例如:飞行途中的石头对碰撞到角色会造成伤害，闲置状态的石头喷到目标不会造成伤害
        /// </summary>
        private bool isIdle;
        /// <summary>
        /// 是否处于闲置状态
        /// 不是闲置状态就是攻击状态 
        /// 飞行途中的石头对碰撞到角色会造成伤害，闲置状态的石头喷到目标不会造成伤害
        /// </summary>
        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            rb.velocity = Vector3.one;
        }
        private void FixedUpdate()
        {
            if (rb.velocity.sqrMagnitude < 1)
            {
                isIdle = true;
            }
        }
        //private void FlyToTarget()
        //{
        //    if (skillData == null)
        //        target = FindObjectOfType<Player>().gameObject;
        //    direction = (target.transform.position - transform.position + Vector3.up).normalized;
        //    rb.AddForce(direction * force, ForceMode.Impulse);
        //}
        public override void DeploySkill()
        {
            //执行选区
            CalculateTargets();
            //飞向目标
            FlyToTarget();
            //执行影响 碰撞事件
            //ImpactTargets();
        }

        /// <summary>
        /// 飞向目标
        /// </summary>
        public void FlyToTarget()
        {
            if (skillData.attackTargets.Length == 0) return;
            //获取方向 添加一个向上的力 以免掉落目标直接在地上
            direction = (skillData.attackTargets[0].transform.position - transform.position + upForce).normalized;
            //添加冲击力
            rb.AddForce(direction * force, ForceMode.Impulse);
        }

        private void OnCollisionEnter(Collision other)
        {
            //不是闲置状态
            if (!isIdle)
            {
                //碰撞到玩家
                if (other.gameObject.CompareTag(EM.Tag.Player.ToString()))
                {
                    //影响
                    ImpactTargets();
                    Instantiate(breakEffect, transform.position, Quaternion.identity);
                    //攻击目标变成敌人
                }
            }
            //switch (rockState)
            //{
            //    case RockState.HitPlayer:
            //        if (other.gameObject.CompareTag("Player"))
            //        {
            //            other.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().isStopped = true;
            //            other.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>().velocity = direction * force;
            //            other.gameObject.GetComponent<Animator>().SetTrigger("Dizzy");
            //            other.gameObject.GetComponent<CharaterState>().TakeDamage(damage, other.gameObject.GetComponent<CharaterState>());

            //            rockState = RockState.HitNothing;
            //        }
            //        break;
            //    case RockState.HitEnemy:
            //        if (other.gameObject.GetComponent<Golem>())
            //        {
            //            CharaterState otherState = other.gameObject.GetComponent<CharaterState>();
            //            otherState.TakeDamage(damage, otherState);
            //            Instantiate(breakEffect, transform.position, Quaternion.identity);
            //            Destroy(gameObject);
            //        }
            //        break;
            //    case RockState.HitNothing:
            //        break;
            //    default:
            //        break;
            //}
        }
    }
}
