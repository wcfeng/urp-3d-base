using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FF.Skill
{
    /// <summary>
    /// 近战技能
    /// </summary>
    public class MeleeSkillDeployer : SkillDeployer
    {
        public override void DeploySkill()
        {
            //执行选区
            CalculateTargets();
            //执行影响
            ImpactTargets();
        }
    }
}
