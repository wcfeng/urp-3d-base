using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FF.Skill
{
    /// <summary>
    /// 技能释放器
    /// </summary>
    public abstract class SkillDeployer : MonoBehaviour
    {
        /// <summary>
        /// 技能数据
        /// </summary>
        protected SkillData_SO skillData;
        /// <summary>
        /// 技能攻击选区
        /// </summary>
        private IAttackSelector selector;
        /// <summary>
        /// 技能影响
        /// </summary>
        private IImpactEffect[] impactArray;

        /// <summary>
        /// 初始化技能 创建选区与影响
        /// </summary>
        /// <value></value>
        public SkillData_SO SkillData
        {
            get
            {
                return skillData;
            }
            set
            {
                skillData = value;
                InitDeployer();
            }
        }
        /// <summary>
        /// 初始化释放器
        /// </summary>
        private void InitDeployer()
        {
            selector = DeployerConfigFactory.CreateAttackSelector(skillData);
            impactArray = DeployerConfigFactory.CreateImpactEffect(skillData);
        }

        /// <summary>
        /// 选区
        /// </summary>
        public void CalculateTargets()
        {
            skillData.attackTargets = selector.SelectTarget(skillData, transform);
            //foreach (var item in skillData.attackTargets)
            //{
            //    print(item);
            //}
        }

        /// <summary>
        /// 技能影响
        /// </summary>
        public void ImpactTargets()
        {
            for (int i = 0; i < impactArray.Length; i++)
            {
                impactArray[i].Execute(this);
            }
        }

        /// <summary>
        /// 释放技能
        /// </summary>
        public abstract void DeploySkill();
    }
}
