using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FF.Skill
{
    /// <summary>
    /// 释放器工厂
    /// </summary>
    public class DeployerConfigFactory
    {
        /// <summary>
        /// 反射（选区、影响）类缓存
        /// </summary>
        private static Dictionary<string,object> cache;
        static DeployerConfigFactory(){
            cache = new Dictionary<string, object>();
        }
        /// <summary>
        /// 创建技能选区
        /// </summary>
        /// <param name="skillData">技能数据</param>
        /// <returns></returns>
        public static IAttackSelector CreateAttackSelector(SkillData_SO skillData)
        {
            //反射
            //选区
            //通过技能类型创建类型对象
            string className = string.Format("FF.Skill.{0}AttackSelector", skillData.selectorType);
            return CreateObject<IAttackSelector>(className);
        }
        /// <summary>
        /// 创建技能影响
        /// </summary>
        /// <param name="skillData"></param>
        /// <returns></returns>
        public static IImpactEffect[] CreateImpactEffect(SkillData_SO skillData)
        {
            IImpactEffect[] impacts = new IImpactEffect[skillData.impactType.Length];
            //影响
            for (int i = 0; i < skillData.impactType.Length; i++)
            {
                string classNameImpact = string.Format("FF.Skill.{0}Impact", skillData.impactType[i].ToString());
                impacts[i] = CreateObject<IImpactEffect>(classNameImpact);
            }
            return impacts;
        }

        /// <summary>
        /// 通过名称创建数据类型（反射）
        /// </summary>
        /// <typeparam name="T">类名</typeparam>
        private static T CreateObject<T>(string className) where T : class
        {
            if(!cache.ContainsKey(className))
            {
                Type type = Type.GetType(className);
                object instance = Activator.CreateInstance(type);
                cache.Add(className,instance);
            }
            return cache[className] as T;
        }
    }
}
