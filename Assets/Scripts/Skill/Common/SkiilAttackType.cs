using System;

namespace FF.Skill
{
    /// <summary>
    /// 攻击类型
    /// </summary>
    public enum SkillAttackType
    {
        /// <summary>
        /// 单体攻击
        /// </summary>
        Single,
        /// <summary>
        /// 群体攻击
        /// </summary>
        Group,
    }
}
