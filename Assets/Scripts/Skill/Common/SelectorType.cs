using System;

namespace FF.Skill
{
    /// <summary>
    /// 攻击选区类型
    /// </summary>
    public enum SelectorType
    {
        /// <summary>
        /// 扇形、圆形攻击
        /// </summary>
        Sector,
        /// <summary>
        /// 矩形攻击
        /// </summary>
        Rectangle,
    }
}
