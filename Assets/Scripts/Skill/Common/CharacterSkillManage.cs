using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace FF.Skill
{
    /// <summary>
    /// 技能管理器
    /// </summary>
    public class CharacterSkillManage : MonoBehaviour
    {
        public SkillData_SO[] skillsTemplate;
        /// <summary>
        /// 技能列表
        /// </summary>
        [HideInInspector]
        public SkillData_SO[] skills;

        private void Start()
        {
            //初始化数量
            skills = new SkillData_SO[skillsTemplate.Length];
            //复制原数据
            for (int i = 0; i < skillsTemplate.Length; i++)
            {
                skills[i] = Instantiate(skillsTemplate[i]);
                //TODO:有时候开始游戏技能会有冷却时间
                skills[i].coolRemain = 0;
            }
            
            for (int i = 0; i < skills.Length; i++)
            {
                InitSkill(skills[i]);
            }
        }

        /// <summary>
        /// 初始化技能
        /// </summary>
        /// <param name="skillData">技能数据</param>
        private void InitSkill(SkillData_SO skillData)
        {
            //skillData.skillPrefab = Resources.Load<GameObject>("Skill/" + skillData.prefabName);
            //skillData.skillPrefab = ResourceManager.Instance.Load<GameObject>(skillData.prefabName);
            skillData.skillPrefab = ResourceManager.Load<GameObject>(skillData.prefabName);
            skillData.owner = gameObject;
        }

        /// <summary>
        /// 准备技能
        /// </summary>
        /// <param name="id">技能id</param>
        /// <returns></returns>
        public SkillData_SO PrepareSkill(int id)
        {
            SkillData_SO skillData = skills.Find(s => s.id == id);
            float mp = GetComponent<CharacterState>().stateData.MP;
            
            if (skillData != null && skillData.coolRemain <= 0 && skillData.costMP <= mp)
            {
                return skillData;
            }
            else
                return null;
        }
        /// <summary>
        /// 释放技能
        /// </summary>
        /// <param name="skillData">技能数据</param>
        public void GenerateSkill(SkillData_SO skillData)
        {
            //获取生成技能位置
            Vector3? position = transform.FindChildByName(skillData.createPos)?.position;
            //创建技能游戏对象
            GameObject skillGo;
            if (skillData.useObjectPool)
            {
                skillGo = GameObjectPool.Instance.CreateObject(skillData.prefabName, skillData.skillPrefab, position ?? transform.position, transform.rotation);
            }
            else
            {
                skillGo = Instantiate(skillData.skillPrefab,position??transform.position,transform.rotation);
            }

            SkillDeployer deployer = skillGo.GetComponent<SkillDeployer>();
            deployer.SkillData = skillData;
            //释放技能
            deployer.DeploySkill();
            //回收技能
            if (skillData.useObjectPool)
            {
                GameObjectPool.Instance.CollectObject(skillGo, skillData.durationTime);
            }
            else
            {
                Destroy(skillGo, skillData.durationTime);
            }
            // //设置技能冷却
            // StartCoroutine(CoolTimeDown(skillData));
        }

        /// <summary>
        /// 设置技能冷却时间
        /// </summary>
        /// <param name="skillData">技能</param>
        /// <returns></returns>
        public IEnumerator CoolTimeDown(SkillData_SO skillData)
        {
            skillData.coolRemain = skillData.coolTime;
            while(skillData.coolRemain > 0)
            {
                yield return new WaitForSeconds(1);
                skillData.coolRemain--;
            }
        }
    }
}
