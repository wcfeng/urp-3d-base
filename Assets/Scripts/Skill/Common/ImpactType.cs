using System;

namespace FF.Skill
{
    /// <summary>
    /// 影响类型
    /// </summary>
    public enum ImpactType
    {
        /// <summary>
        /// 消耗MP
        /// </summary>
        CostMP,
        /// <summary>
        /// 造成伤害
        /// </summary>
        Damage,
        /// <summary>
        /// 击退
        /// </summary>
        Repel,
        /// <summary>
        /// 眩晕
        /// </summary>
        Dizzy
    }
}
