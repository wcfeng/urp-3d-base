using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
using System;

namespace FF.Skill
{
    /// <summary>
    /// 角色技能系统
    /// </summary>
    [RequireComponent(typeof(CharacterSkillManage))]
    public class CharacterSkillSystem : MonoBehaviour
    {
        /// <summary>
        /// 技能管理器
        /// </summary>
        private CharacterSkillManage skillManage;
        /// <summary>
        /// 动画控制
        /// </summary>
        private Animator anim;
        //动作队列？
        /// <summary>
        /// 技能
        /// </summary>
        private SkillData_SO skill;
        /// <summary>
        /// 选择目标
        /// </summary>
        private Transform selectedTarget;

        public event Action<SkillData_SO> OnSkillStart;
        private void Start()
        {
            skillManage = GetComponent<CharacterSkillManage>();
            anim = GetComponentInChildren<Animator>();
            // GetComponentInChildren<AnimationEventBehaviour>().attackHandler += DeploySkill;
            GetComponentInChildren<AnimatorEventController>().attackHandler += DeploySkill;
        }
        /// <summary>
        /// 释放技能
        /// </summary>
        private void DeploySkill()
        {
            skillManage.GenerateSkill(skill);
        }

        /// <summary>
        /// 使用技能攻击
        /// </summary>
        public void AttackUseSkill(int id, bool isBatter = false)
        {
            //连击
            if (skill != null && isBatter)
                id = skill.nextBatterId;

            //准备技能
            skill = skillManage.PrepareSkill(id);
            if (skill == null) return;
            OnSkillStart?.Invoke(skill);
            //释放技能  生成技能
            anim.SetBool(skill.animationName, true);
            //设置技能冷却
            StartCoroutine(skillManage.CoolTimeDown(skill));
            //StartCoroutine(skillManage.CoolTimeDown(skillManage.skills.Find(s => skill)));
            //如果单攻
            if (skill.attackType != SkillAttackType.Single) return;
            //寻找攻击目标
            Transform targetTF = SelectTarget();
            //没找目标
            if (!targetTF) return;
            //转向目标
            transform.LookAt(targetTF);
            //打印对方血量
            int hp = targetTF.GetComponent<CharacterState>().stateData.HP;
            //选中目标
            SetSelectedActiveFx(false);
            selectedTarget = targetTF;
        }
        /// <summary>
        /// 选择目标
        /// </summary>
        /// <returns>目标变换</returns>
        private Transform SelectTarget()
        {
            Transform[] target = new SectorAttackSelector().SelectTarget(skill, transform);
            return target.Length != 0 ? target[0] : null;
        }
        /// <summary>
        /// 设置选择目标激活状态（底下红圈）
        /// </summary>
        /// <param name="state"></param>
        private void SetSelectedActiveFx(bool state)
        {
            if (selectedTarget == null) return;
            CharacterSelected selected = selectedTarget.GetComponent<CharacterSelected>();
            if (selected) selected.SetSelectedActive(state);
        }
        /// <summary>
        /// 使用随机技能（为NPC提供）
        /// </summary>
        public void UseRandomSkill()
        {
            //TODO 技能重复使用的问题
            //从管理器 中挑选出随机技能
            SkillData_SO[] skills = skillManage.skills.FindAll(s => skillManage.PrepareSkill(s.id) != null);
            if (skills.Length == 0) return;
            int index = UnityEngine.Random.Range(0, skills.Length);
            AttackUseSkill(skills[index].id);
        }
    }
}
