﻿using UnityEditor;
using UnityEngine;

namespace EM
{
    /// <summary>
    /// 目的地标记
    /// </summary>
    public enum DestinationTag
    {
        /// <summary>
        /// 场景起点
        /// </summary>
        Start,
        A,
        B,
        C
    }
}