﻿using UnityEditor;
using UnityEngine;

namespace EM
{
    /// <summary>
    /// 转换类型（传送类型）
    /// </summary>
    public enum TransitionType
    {
        /// <summary>
        /// 同场景
        /// </summary>
        SameScene,
        /// <summary>
        /// 不同场景
        /// </summary>
        DifferentScene
    }
}