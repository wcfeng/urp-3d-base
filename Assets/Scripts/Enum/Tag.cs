﻿namespace EM
{
	public enum Tag
	{
		Untagged,
		Respawn,
		Finish,
		EditorOnly,
		MainCamera,
		Player,
		GameController,
		Enemy,
		Portal,
		CinemachineTarget,
		Ground,
		Attackable,
}
	}
