using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
namespace Common
{
    /// <summary>
    /// 动画事件管理
    /// </summary>
    public class AnimatorEventController : MonoBehaviour
    {
        private Animator anim;
        [Tooltip("是否自动绑定攻击")]
        public bool isAuto = true;
        /// <summary>
        /// 攻击事件
        /// </summary>
        public event Action attackHandler;
        private void Start()
        {
            anim = GetComponent<Animator>();
            //自动添加动画事件 不需要可注释
            if (isAuto)
            {
                AddAnimationEvent();
            }
        }

        /// <summary>
        /// 自动添加动画事件
        /// </summary>
        private void AddAnimationEvent()
        {
            //得到所有动画
            AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;

            for (int i = 0; i < clips.Length; i++)
            {
                //根据动画名字  找到你要添加的动画
                //普通攻击
                if (string.Equals(clips[i].name, "Attack1"))
                {
                    //添加动画事件开始
                    AnimationEvent events = new AnimationEvent();
                    events.functionName = "OnStartAttack1";
                    events.stringParameter = "Attack1";
                    events.time = 0.1f;
                    clips[i].AddEvent(events);

                    AnimationEvent eventsEnd = new AnimationEvent();
                    //添加动画事件结束
                    eventsEnd.functionName = "OnEndAnim";
                    eventsEnd.stringParameter = "Attack1";
                    eventsEnd.time = clips[i].length;
                    clips[i].AddEvent(eventsEnd);
                }
                //多段攻击
                if (string.Equals(clips[i].name, "Attack2"))
                {
                    //添加动画击晕事件
                    AnimationEvent events = new AnimationEvent();
                    // events.functionName = "OnStartRepelAttack2";
                    // events.stringParameter = "Attack2";
                    // events.time = clips[i].length * 0.5f;
                    // clips[i].AddEvent(events);

                    //添加动画攻击
                    events = new AnimationEvent();
                    events.functionName = "OnStartAttack2";
                    events.stringParameter = "Attack2";
                    events.time = clips[i].length * 0.8f;
                    clips[i].AddEvent(events);

                    AnimationEvent eventsEnd = new AnimationEvent();
                    //添加动画事件结束
                    eventsEnd.functionName = "OnEndAnim";
                    eventsEnd.stringParameter = "Attack2";
                    eventsEnd.time = clips[i].length;
                    clips[i].AddEvent(eventsEnd);
                }
                if (string.Equals(clips[i].name, "Attack3"))
                {
                    //添加动画击晕事件
                    AnimationEvent events = new AnimationEvent();
                    // events.functionName = "OnStartRepelAttack2";
                    // events.stringParameter = "Attack2";
                    // events.time = clips[i].length * 0.5f;
                    // clips[i].AddEvent(events);

                    //添加动画攻击
                    events = new AnimationEvent();
                    events.functionName = "OnStartAttack3";
                    events.stringParameter = "Attack3";
                    events.time = clips[i].length * 0.8f;
                    clips[i].AddEvent(events);

                    AnimationEvent eventsEnd = new AnimationEvent();
                    //添加动画事件结束
                    eventsEnd.functionName = "OnEndAnim";
                    eventsEnd.stringParameter = "Attack3";
                    eventsEnd.time = clips[i].length;
                    clips[i].AddEvent(eventsEnd);
                }
            }

            anim.Rebind();
        }

        /// <summary>
        /// 攻击开始
        /// </summary>
        /// <param name="animParam"></param>
        private void OnStartAttack1(string animParam)
        {
            if (attackHandler != null)
            {
                attackHandler();
            }
        }
        /// <summary>
        /// 攻击开始
        /// </summary>
        /// <param name="animParam"></param>
        private void OnStartAttack2(string animParam)
        {
            if (attackHandler != null)
            {
                attackHandler();
            }
        }
        /// <summary>
        /// 攻击开始
        /// </summary>
        /// <param name="animParam"></param>
        private void OnStartAttack3(string animParam)
        {
            if (attackHandler != null)
            {
                attackHandler();
            }
        }

        /// <summary>
        /// 攻击结束
        /// </summary>
        /// <param name="animParam"></param>
        private void OnEndAnim(string animParam)
        {
            anim.SetBool(animParam, false);
        }
    }
}