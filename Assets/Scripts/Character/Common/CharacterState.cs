using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace FF
{
    /// <summary>
    /// 角色状态
    /// </summary>
    public class CharacterState : MonoBehaviour
    {
        /// <summary>
        /// 状态数据模板
        /// </summary>
        public StateData_SO stateDataTemplate;
        /// <summary>
        /// 当前角色状态数据
        /// </summary>
        [HideInInspector]
        public StateData_SO stateData;
        /// <summary>
        /// 动画控制
        /// </summary>
        private Animator anim;
        [HideInInspector]
        public bool isStop;
        public string desc;
        /// <summary>
        /// aa
        /// </summary>
        public event Action<int, int> UpdateHPOnDamage;
        private void Awake()
        {
            //复制原数据
            if (stateDataTemplate != null)
            {
                stateData = Instantiate(stateDataTemplate);
            }

            anim = GetComponentInChildren<Animator>();

        }
        protected void Start()
        { }
        /// <summary>
        /// 受伤
        /// </summary>
        /// <param name="damage">伤害</param>
        public void Damage(float damage)
        {
            //所有受到伤害是共性的表现 HP减少！
            //受击者 有防御能力
            damage -= stateData.DEF;
            damage = Mathf.Max(damage, 1);
            stateData.HP -= (int)damage;
            SoundManager.Instance.HurtAudio();
            //受击 发送到网络
            TestNet.Instance.SendHit(this, damage);
            print("伤害：" + damage);
            //受伤事件
            UpdateHPOnDamage?.Invoke(stateData.HP, stateData.maxHP);
            //可能死亡
            if (stateData.HP <= 0)
                Dead();
            else
                //受击动画
                anim.SetTrigger(Const.Animation.GetHit);
            //子类可以再加上个性的表现
        }
        /// <summary>
        /// 死亡
        /// </summary>
        protected virtual void Dead()
        {
            //死亡 发送到网络
            TestNet.Instance.SendDie(this);
            isStop = true;
            anim.SetBool(Const.Animation.Death, true);
        }
        //受击 同时播放受击 特效：需要找到 受击特效挂载点

        /// <summary>
        /// 修改经验值
        /// </summary>
        /// <param name="exp">经验值</param>
        public void UpdateExp(int exp)
        {
            stateData.currentExp += exp;
            if (stateData.currentExp >= stateData.levelUpExp)
                LevelUp();
        }
        /// <summary>
        /// 受到外力
        /// </summary>
        /// <param name="force">力</param>
        public void getForce(Vector3 force)
        {
            //导航受力
            NavMeshAgent NavMeshAgent = GetComponent<NavMeshAgent>();
            if (NavMeshAgent)
            {
                NavMeshAgent.isStopped = true;
                NavMeshAgent.velocity = force;
            }

            //角色控制器受力
            CharacterController characterController = GetComponent<CharacterController>();
            if (characterController)
            {
                //characterController.SimpleMove(force);
            }

            //刚体受力
        }
        public void Dizzy(float timeout)
        {
            anim.SetBool(Const.Animation.Dizzy, true);
            StartCoroutine(StopDizzy(timeout));
        }
        private IEnumerator StopDizzy(float timeout)
        {
            //总伤害时间
            float atkTime = 0;
            do
            {
                yield return new WaitForSeconds(timeout);
                //退出眩晕
                anim.SetBool(Const.Animation.Dizzy, false);
                atkTime += timeout;
            } while (atkTime < timeout);
        }
        /// <summary>
        /// 升级
        /// </summary>
        private void LevelUp()
        {
            //获取升级后的等级 最高等级限制
            stateData.currentLevel = Mathf.Clamp(stateData.currentLevel + 1, 0, stateData.maxLevel);
            //获取下一级所需经验
            stateData.levelUpExp += (int)(stateData.levelUpExp * stateData.LevelMultiplier);
            //最大生命值增加
            stateData.maxHP = (int)(stateData.maxHP * stateData.LevelMultiplier);
            //当前生命值恢复未最大生命值
            stateData.HP = stateData.maxHP;
            //TODO:升级后续
            Debug.Log("LEVEL UP");
        }
    }
}
