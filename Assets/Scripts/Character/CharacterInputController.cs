using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;
using FF.Skill;

namespace FF
{
    /// <summary>
    /// 角色输入控制器
    /// </summary>
    [RequireComponent(typeof(CharacterMotor), typeof(PlayerState))]
    public class CharacterInputController : MonoBehaviour
    {
        /// <summary>
        /// 滑杆
        /// </summary>
        private ETCJoystick joystick;
        /// <summary>
        /// 角色马达
        /// </summary>
        private CharacterMotor chMotor;
        /// <summary>
        /// 动画控制
        /// </summary>
        private Animator anim;
        /// <summary>
        /// 角色控制器
        /// </summary>
        private CharacterController _controller;
        /// <summary>
        /// 玩家状态
        /// </summary>
        private PlayerState playerState;
        /// <summary>
        /// 按钮
        /// </summary>
        private ETCButton[] skillButtons;
        /// <summary>
        /// 技能系统
        /// </summary>
        private CharacterSkillSystem skillSystem;
        /// <summary>
        /// 最后按下时间
        /// </summary>
        private float lastPressTime;
        // private Vector3 cameraOffset;
        // private Transform PlayerCameraRoot;

        #region 跳跃与重力相关参数
        /// <summary>
        /// 再次跳跃时间 设置为0f，立即再次跳跃
        /// </summary>
        public float JumpTimeout = 0.5f;
        /// <summary>
        /// 进入下落状态所需的时间，对走下楼梯很有用
        /// </summary>
        public float FallTimeout = 0.15f;
        //跳跃时间
        private float _jumpTimeoutDelta;
        //落地时间
        private float _fallTimeoutDelta;
        //达到期望高度所需的速度
        private float _verticalVelocity;
        //自由落体 最大速度
        private float _terminalVelocity = 53.0f;
        //跳跃高度
        public float JumpHeight = 1.2f;
        //角色重力值 引擎默认9.81
        public float Gravity = -15.0f;
        /// <summary>
        /// 是否跳跃
        /// </summary>
        private bool _isJump;
        #endregion

        /// <summary>
        /// 获取各个组件 按钮等
        /// </summary>
        private void Awake()
        {
            joystick = FindObjectOfType<ETCJoystick>();
            chMotor = GetComponent<CharacterMotor>();
            anim = GetComponentInChildren<Animator>();
            playerState = GetComponent<PlayerState>();
            skillButtons = FindObjectsOfType<ETCButton>();
            _controller = GetComponent<CharacterController>();
            // PlayerCameraRoot = transform.FindChildByName("PlayerCameraRoot");

            skillSystem = GetComponent<CharacterSkillSystem>();
        }
        private void Start()
        {
            // this.cameraOffset = Camera.main.transform.position - this.transform.position;

            _jumpTimeoutDelta = JumpTimeout;
            _fallTimeoutDelta = FallTimeout;
        }

        /// <summary>
        /// 脚本启用 注册各个事件
        /// </summary>
        private void OnEnable()
        {
            joystick.onMove.AddListener(OnJoystickMove);
            joystick.onMoveStart.AddListener(OnJoystickMoveStart);
            joystick.onMoveEnd.AddListener(OnJoystickMoveEnd);

            for (int i = 0; i < skillButtons.Length; i++)
            {
                switch (skillButtons[i].name)
                {
                    case "BaseButton":
                        skillButtons[i].onPressed.AddListener(OnSkillButtonPressed);
                        break;
                    case "JumpButton":
                        skillButtons[i].onPressed.AddListener(OnJumpButtonDown);
                        break;
                    default:
                        skillButtons[i].onDown.AddListener(OnSkillButtonDown);
                        break;
                }
                //if (skillButtons[i].name == "BaseButton")
                //    skillButtons[i].onPressed.AddListener(OnSkillButtonPressed);
                //else
                //    skillButtons[i].onDown.AddListener(OnSkillButtonDown);
            }
        }
        private void OnJumpButtonDown()
        {
            if (Incapacitated()) return;
            _isJump = true;
            SoundManager.Instance.JumpAudio();
        }

        /// <summary>
        /// 跳跃与重力
        /// </summary>
        private void JumpAndGravity()
        {
            if (Grounded)
            {
                // 重置掉落时间
                _fallTimeoutDelta = FallTimeout;

                // 停止跳跃和落下参数
                anim.SetBool(Const.Animation.Jump, false);
                anim.SetBool(Const.Animation.FreeFall, false);

                // 如果 掉落速度 < 0
                if (_verticalVelocity < 0)
                {
                    //掉落速度 = -2
                    _verticalVelocity = -2f;
                }

                // 按下跳跃 并且 跳跃时间<=0
                if (_isJump && _jumpTimeoutDelta <= 0.0f)
                {
                    // H * -2 * G的平方根=达到期望高度所需的速度
                    _verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);

                    // 更新跳跃动画
                    anim.SetBool(Const.Animation.Jump, true);
                }

                // 跳跃 倒计时
                if (_jumpTimeoutDelta >= 0.0f)
                {
                    _jumpTimeoutDelta -= Time.deltaTime;
                }
            }
            else
            {
                //不在地面时
                // 重置跳跃时间
                _jumpTimeoutDelta = JumpTimeout;

                // 落下 倒计时
                if (_fallTimeoutDelta >= 0.0f)
                {
                    _fallTimeoutDelta -= Time.deltaTime;
                }
                else
                {
                    //播放落下动画
                    anim.SetBool(Const.Animation.FreeFall, true);
                }

                // 没有落地前 不再跳跃
                _isJump = false;
            }

            // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
            //如果在终端下，在一段时间内应用重力(乘以2倍时间线性加速)  
            if (_verticalVelocity < _terminalVelocity)
            {
                _verticalVelocity += Gravity * Time.deltaTime;
            }
        }

        private float _speed;
        //加速度
        public float SpeedChangeRate = 10.0f;
        private float _animationBlend;
        private void Move()
        {
            float targetSpeed = playerState.stateData.moveSpeed;
            // 没有输入是 速度设置为0
            if (targetDirection == Vector3.zero) targetSpeed = 0.0f;
            // 玩家当前 相对 水平速度
            float currentHorizontalSpeed = new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;

            float speedOffset = 0.1f;
            float inputMagnitude = targetDirection != Vector3.zero ? targetDirection.magnitude : 1f;

            // 加速或减速到目标速度
            if (currentHorizontalSpeed < targetSpeed - speedOffset || currentHorizontalSpeed > targetSpeed + speedOffset)
            {
                //创建弯曲的结果，而不是线性的一个给予更有机的速度变化  
                //注意:Lerp中的T是夹紧的，所以我们不需要夹紧我们的速度  
                _speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude, Time.deltaTime * SpeedChangeRate);

                // 将速度四舍五入到小数点后3位
                _speed = Mathf.Round(_speed * 1000f) / 1000f;
            }
            else
            {
                _speed = targetSpeed;
            }
            _animationBlend = Mathf.Lerp(_animationBlend, targetSpeed, Time.deltaTime * SpeedChangeRate);

            // normalise input direction
            Vector3 inputDirection = targetDirection;

            //旋转

            //移动
            // _controller.Move(targetDirection.normalized * (_speed * Time.deltaTime) + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);
            //只模拟重力
            _controller.Move(new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);

            // update animator if using character
            // anim.SetFloat(_animIDSpeed, _animationBlend);
            // anim.SetFloat(_animIDMotionSpeed, inputMagnitude);
        }

        private void Update()
        {
            GroundedCheck();
            JumpAndGravity();
            Move();
        }
        //崎岖路面
        public float GroundedOffset = -0.14f;
        //地面半径
        public float GroundedRadius = 0.28f;
        public LayerMask GroundLayers;
        //是否落地
        public bool Grounded = true;
        /// <summary>
        /// 检测是否落地
        /// </summary>
        private void GroundedCheck()
        {
            // 设置球体位置，比当前角色位置略低
            Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z);
            // 判断球体的范围内是个有地面（层） 判断是否站在地面上
            Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);
            //设置地面参数
            anim.SetBool(Const.Animation.Grounded, Grounded);
        }

        /// <summary>
        /// 连击
        /// </summary>
        private void OnSkillButtonPressed()
        {
            
            if (Incapacitated()) return;
            //单次攻击
            //skillSystem.AttackUseSkill(1);
            //连击
            //时间间隔：当前按下时间 - 最后按下时间
            float interval = Time.time - lastPressTime;
            if (interval < 0.5f) return;
            //是否连击
            bool isBatter = interval <= 2;
            skillSystem.AttackUseSkill(1001, isBatter);
            print(anim.GetCurrentAnimatorStateInfo(1).normalizedTime);
            lastPressTime = Time.time;
        }

        //技能按下
        /// <summary>
        /// 技能按下事件
        /// </summary>
        /// <param name="name">按键名称</param>
        private void OnSkillButtonDown(string name)
        {
            if (Incapacitated()) return;

            int id = 0;
            switch (name)
            {
                case "BaseButton":
                    id = 1;
                    break;
                //case "SkillButton01":
                //    id = 2;
                //    break;
                case "SkillButton02":
                    id = 3;
                    break;
                default:
                    break;
            }
            skillSystem.AttackUseSkill(id);
            //CharacterSkillManage skillManager = GetComponent<CharacterSkillManage>();
            //SkillData skilldata = skillManager.PrepareSkill(id);
            //if (skilldata != null)
            //{
            //    skillManager.GenerateSkill(skilldata);
            //    print("打死你");
            //}
        }
        /// <summary>
        /// 开始移动 设置移动动画
        /// </summary>
        private void OnJoystickMoveStart()
        {
            // anim.SetBool(Const.Animation.Run, true);
            //anim.SetFloat("Speed", 1);
        }
        /// <summary>
        /// 停止移动 停止动画
        /// </summary>
        private void OnJoystickMoveEnd()
        {
            anim.SetFloat(Const.Animation.Speed, 0);
            //移动 发送到网络
            TestNet.Instance.SendMove(playerState);
        }
        /// <summary>
        /// 脚本禁用 移除启用的事件
        /// </summary>
        private void OnDisable()
        {
            joystick.onMove.RemoveListener(OnJoystickMove);
            joystick.onMoveStart.RemoveListener(OnJoystickMoveStart);
            joystick.onMoveEnd.RemoveListener(OnJoystickMoveEnd);

            for (int i = 0; i < skillButtons.Length; i++)
            {
                if (skillButtons[i] == null) continue;
                if (skillButtons[i].name == "BaseButton")
                    skillButtons[i].onPressed.RemoveListener(OnSkillButtonPressed);
                else
                    skillButtons[i].onDown.RemoveListener(OnSkillButtonDown);
            }
        }

        private Vector3 targetDirection;
        /// <summary>
        /// 滑杆移动
        /// </summary>
        /// <param name="dir">移动位置</param>
        private void OnJoystickMove(Vector2 dir)
        {
            if (Incapacitated()) return;

            Vector3 moveDir = new Vector3(dir.x, 0, dir.y);

            //移动新方法
            moveDir = dir.magnitude > 0.1f ? moveDir : Vector3.zero;
            targetDirection = moveDir;

            // return ;
            if (!_controller.isGrounded) moveDir = Vector3.zero;
            //调用角色马达移动方法
            float speed = dir.magnitude > 0.1f ? 1 : 0;
            anim.SetFloat(Const.Animation.Speed, speed);
            if (!SoundManager.Instance.audioSource.isPlaying)
                SoundManager.Instance.MoveAudio();
            chMotor.Movement(moveDir, playerState.stateData.moveSpeed);
            //移动 发送到网络
            TestNet.Instance.SendMove(playerState);
        }
        /// <summary>
        /// 是否在攻击中
        /// </summary>
        /// <returns></returns>
        private bool IsAttacking()
        {
            return false;//做了动画后删除
            // return anim.GetBool(Const.Animation.Attack) || anim.GetBool(Const.Animation.Attack1);
        }
        /// <summary>
        /// 是否无法行动
        /// </summary>
        /// <returns></returns>
        private bool Incapacitated()
        {
            if (GameController.Instance.player.isStop) return true;
            string[] amintions = new string[]{
                Const.Animation.Attack1,
            };
            foreach (string amintion in amintions)
            {
                if (anim.GetBool(amintion)) return true;
            }
            return false;
        }
        private bool IsMove()
        {
            return anim.GetBool(Const.Animation.Run);
        }
    }
}
