using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace FF
{
    [RequireComponent(typeof(CharacterController))]
    /// <summary>
    /// 角色马达
    /// </summary>
    public class CharacterMotor : MonoBehaviour
    {
        /// <summary>
        /// 角色控制器
        /// </summary>
        private CharacterController chController;

        /// <summary>
        /// 相机平滑旋转时间
        /// </summary>
        private float _rotationSmoothTime = 0.12f;
        /// <summary>
        /// 相机旋转速度
        /// </summary>
        private float _rotationVelocity;

        private void Start()
        {
            chController = GetComponent<CharacterController>();
        }
        /// <summary>
        /// 转向
        /// </summary>
        /// <param name="direction">方向</param>
        public void LookAtTarget(Vector3 direction)
        {
            if (direction == Vector3.zero) return;

            //固定视角旋转
            // transform.Rotate(0, direction.x * Time.deltaTime * rotationSpeed, 0);
            // Quaternion lookDir = Quaternion.LookRotation(direction);
            // transform.rotation = Quaternion.Lerp(transform.rotation, lookDir, rotationSpeed);

            //移动视角旋转
            //获取目标角度 -180~180
            float _targetDegree = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            //获取相机y轴旋转角度 0~360
            float _cameraRotateY = Camera.main.transform.eulerAngles.y;
            float _targetRotation = _targetDegree + _cameraRotateY;
            
            float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity, _rotationSmoothTime);
            // 旋转到相对于相机位置的面输入方向
            Quaternion targetDir = Quaternion.Euler(0, rotation, 0);

            transform.rotation = Quaternion.Lerp(transform.rotation,targetDir,1);
        }
        /// <summary>
        /// 移动
        /// </summary>
        /// <param name="direction">方向</param>
        public void Movement(Vector3 direction, float moveSpeed)
        {
            LookAtTarget(direction);
            Vector3 forward = transform.forward;
            // forward.y = -1;
            // chController.Move(forward * Time.deltaTime * moveSpeed);
            // chController.Move(forward * direction.z * Time.deltaTime * moveSpeed);
            //固定视角第三人称
            chController.SimpleMove(forward * moveSpeed);
        }

        /// <summary>
        /// 跳跃
        /// </summary>
        public void Jump()
        {
            Vector3 forward = Vector3.up;
            forward.y = 5;
            chController.Move(forward * 5);
        }
    }
}
