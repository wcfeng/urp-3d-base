using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace FF
{
    /// <summary>
    /// 同步角色状态
    /// </summary>
    public class SyncState : CharacterState
    {
        private NavMeshAgent navAgent;
        private Animator animator;
        private new void Start()
        {
            base.Start();
            navAgent = GetComponent<NavMeshAgent>();
            animator = GetComponentInChildren<Animator>();
        }

        public void Move(Vector3 position,float moveSpeed)
        {
            animator.SetFloat(Const.Animation.Speed, moveSpeed);
            navAgent.SetDestination(position);
        }

        public void Attack()
        {
            animator.SetBool(Const.Animation.Attack1, true);
        }

        protected override void Dead()
        {
            base.Dead();
            //TODO 死亡处理
            print("死亡");
            GameController.Instance.NotifyObservers();
        }

    }
}
