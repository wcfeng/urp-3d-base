using Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FF
{
    /// <summary>
    /// 玩家状态
    /// </summary>
    public class PlayerState : CharacterState
    {

        //受击 同时播放受击 特效：需要找到 受击特效挂载点
        public Transform HitFxPos;
        /// <summary>
        /// 相机观察点
        /// </summary>
        [HideInInspector]
        public Transform playerCameraRoot;
        private new void Start()
        {
            base.Start();
            //HitFxPos = TransformHelper.FindChild(transform, "HitFxPos");
            GetComponent<FF.Skill.CharacterSkillSystem>().OnSkillStart += SkillStart;
            playerCameraRoot = transform.FindChildByName("PlayerCameraRoot");
        }

        private void SkillStart(SkillData_SO obj)
        {
            //攻击 发送到网络
            TestNet.Instance.SendAttack(this);
        }

        protected override void Dead()
        {
            base.Dead();
            //TODO 死亡处理
            print("死亡");
            GameController.Instance.NotifyObservers();
        }

        private void OnEnable() {
            GameController.Instance.RigisterPlayer(this);
            CameraManager.Instance.RigisterPlayer(this);
        }

    }
}
