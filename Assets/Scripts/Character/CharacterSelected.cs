using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FF
{
    /// <summary>
    /// 角色选择器
    /// </summary>
    public class CharacterSelected : MonoBehaviour
    {
        /// <summary>
        /// 激活游戏物体
        /// </summary>
        private GameObject selectedGo;
        /// <summary>
        /// 激活游戏物体名称
        /// </summary>
        public string selectedName = "selected";
        private float hideTime;
        /// <summary>
        /// 激活时间
        /// </summary>
        public float displayTime = 3;

        private void Start()
        {
            selectedGo = transform.Find(selectedName).gameObject;
        }
        /// <summary>
        /// 设置激活状态
        /// </summary>
        /// <param name="state">是否激活</param>
        public void SetSelectedActive(bool state)
        {
            //设置选择器物体激活状态
            selectedGo.SetActive(state);
            //设置当前脚本激活状态 影响update
            this.enabled = state;
            if (state)
                hideTime = Time.time + displayTime;
        }

        private void Update()
        {
            if(hideTime <= Time.time)
            {
                SetSelectedActive(false);
            }
        }
    }
}
