using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace FF
{
    /// <summary>
    /// 敌人状态
    /// </summary>
    public class EnemyState : CharacterState,IEndGameObserver
    {

        //受击 同时播放受击 特效：需要找到 受击特效挂载点
        public Transform HitFxPos;
        private new void Start()
        {
            base.Start();
            //HitFxPos = TransformHelper.FindChild(transform, "HitFxPos");
            // GameManager.Instance.AddObserver(this);
        }

        protected override void Dead()
        {
            base.Dead();
            Destroy(gameObject, stateData.destroyTime);
        }

        private void OnEnable()
        {
            GameController.Instance.AddObserver(this);
        }
        private void OnDisable()
        {
            //FIXME 这里会报错
            GameController.Instance.RemoveObserver(this);
        }
        public void EndGameNotify()
        {
            //获胜
            //停止移动
            //停止Agent
            // anim.SetBool("Win", true);
            // playerDead = true;
            print("end game");
        }

    }
}
