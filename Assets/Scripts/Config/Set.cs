using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Config
{
    /// <summary>
    /// 用户设置
    /// </summary>
    public class Set : MonoSingleton<Set>
    {
        public float soundValue = -50;
    }
}
